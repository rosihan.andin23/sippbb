-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2020 at 09:04 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pbb`
--

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `conf_code` varchar(100) NOT NULL,
  `conf_groups` varchar(150) DEFAULT NULL,
  `conf_name` varchar(100) NOT NULL,
  `conf_value` text NOT NULL,
  `conf_default` text DEFAULT NULL,
  `conf_type` enum('text','number','html') NOT NULL DEFAULT 'text',
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_by` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`conf_code`, `conf_groups`, `conf_name`, `conf_value`, `conf_default`, `conf_type`, `updated_at`, `updated_by`) VALUES
('DENDA_ALL', '[ALL]', 'Besaran Denda Default (% per bulan)', '2', '2', 'number', '2020-03-20 06:56:08', 'admin'),
('KANTOR_EMAIL', '[KANTOR][SURAT]', 'Alamat Email', 'badankeuangan@ngawikab.go.id', 'badankeuangan@ngawikab.go.id', 'text', '2020-03-19 15:29:06', NULL),
('KANTOR_KODE_POS', '[KANTOR][SURAT]', 'Kode Pos Kantor', '63211', '63211', 'text', '2020-03-19 15:29:06', NULL),
('KANTOR_WEBSITE', '[KANTOR][SURAT]', 'Alamat Website', 'http//www.ngawikab.go.id', 'http//www.ngawikab.go.id', 'text', '2020-03-19 15:29:06', NULL),
('KD_DATI2', '[ALL]', 'ID Kode Dati 2 (Kota/Kab)', '21', '21', 'number', '2020-03-19 15:29:06', NULL),
('KD_PROPINSI', '[ALL]', 'ID Kode Provinsi', '35', '35', 'number', '2020-03-19 15:29:06', NULL),
('START_TAHUN', '[ALL]', 'Tahun Mulai PBB Berjalan', '2014', '2014', 'number', '2020-03-29 09:17:06', NULL),
('SURAT_HIMBAUAN_ISI', '[SURAT]', 'Isi Surat Himbauan', '<p>Dengan ini diberitahukan bahwa menurut administrasi pembukuan yang tercatat di Badan Keuangan Kabupaten Ngawi, Pajak Bumi dan Bangunan Perdesaan dan Perkotaan (PBB P2) yang menjadi kewajiban saudara adalah sebagai berikut :</p>\r\n\r\n{DATA}\r\n\r\n<p>Diminta segera melunasi PBB terhutang sebagaimana tersebut diatas. Apabila saudara tidak melunasi PBB terhutang setelah Jatuh Tempo maka akan dikenakan denda administrasi sebesar {DENDA_ALL}% perbulan sejak tanggal jatuh tempo.</p>\r\n<p>Demikian atas perhatiannya disampaikan terima kasih.</p>', '<p>Dengan ini diberitahukan bahwa menurut administrasi pembukuan yang tercatat di Badan Keuangan Kabupaten Ngawi, Pajak Bumi dan Bangunan Perdesaan dan Perkotaan (PBB P2) yang menjadi kewajiban saudara adalah sebagai berikut :</p>\r\n\r\n{DATA}\r\n\r\n<p>Diminta segera melunasi PBB terhutang sebagaimana tersebut diatas. Apabila saudara tidak melunasi PBB terhutang setelah Jatuh Tempo maka akan dikenakan denda administrasi sebesar {DENDA_ALL}% perbulan sejak tanggal jatuh tempo.</p>\r\n<p>Demikian atas perhatiannya disampaikan terima kasih.</p>', 'html', '2020-03-19 15:29:06', NULL),
('SURAT_HIMBAUAN_NO', '[SURAT]', 'Format Nomor Surat Himbauan', '973 /{KOSONG}/404.108 /{TAHUN}', '973 /{KOSONG}/404.108 /{TAHUN}', 'text', '2020-03-19 15:29:06', NULL),
('SURAT_HIMBAUAN_TTD', '[SURAT]', 'TTD Surat Himbauan', '<p><b>An. KEPALA BADAN KEUANGAN </b></p>\r\n<p><b>KABUPATEN NGAWI</b></p>\r\n<p><b>KEPALA BIDANG PENGELOLAAN PENDAPATAN</b></p>\r\n<br>\r\n<br>\r\n<br>\r\n<p><b><u>AKHMAD ARWAN ARIFIYANTO, SE</u></b></p>\r\n<p>Penata Tk. I</p>\r\n<p>NIP. 19740614 200312 1 004</p>', '<p><b>An. KEPALA BADAN KEUANGAN </b></p>\r\n<p><b>KABUPATEN NGAWI</b></p>\r\n<p><b>KEPALA BIDANG PENGELOLAAN PENDAPATAN</b></p>\r\n<br>\r\n<br>\r\n<br>\r\n<p><b><u>AGUS SETO BUDI, SE</u></b></p>\r\n<p>Pembina</p>\r\n<p>NIP. 19620421 199003 1 013</p>', 'html', '2020-09-05 02:28:25', 'tanu'),
('SURAT_TAGIHAN_ISI', '[SURAT]', 'Isi Surat Tagihan Bagian', '<p>Dengan ini diberitahukan bahwa menurut administrasi pembukuan yang tercatat di Badan Keuangan Kabupaten Ngawi, Pajak Bumi dan Bangunan Perdesaan dan Perkotaan (PBB P2) yang menjadi kewajiban saudara adalah sebagai berikut :</p>\r\n\r\n{DATA}\r\n\r\n<p>Selanjutnya diminta dengan hormat untuk segera melaksanakan pembayaran tagihan PBB P2 dimaksud sebelum tanggal <b><i>{JATUH_TEMPO_SURAT}</i></b>, di Bank Jatim.</p>\r\n\r\n<p>Demikian atas perhatiannya di sampaikan terimakasih.</p>', '<p>Dengan ini diberitahukan bahwa menurut administrasi pembukuan yang tercatat di Badan Keuangan Kabupaten Ngawi, Pajak Bumi dan Bangunan Perdesaan dan Perkotaan (PBB P2) yang menjadi kewajiban saudara adalah sebagai berikut :</p>\r\n\r\n{DATA}\r\n\r\n<p>Selanjutnya diminta dengan hormat untuk segera melaksanakan pembayaran tagihan PBB P2 dimaksud sebelum tanggal <b><i>{JATUH_TEMPO_SURAT}</i></b>, di Bank Jatim.</p>\r\n\r\n<p>Demikian atas perhatiannya di sampaikan terimakasih.</p>', 'html', '2020-03-19 15:29:06', NULL),
('SURAT_TAGIHAN_NO', '[SURAT]', 'Format Nomor Surat Tagihan', '973 /{KOSONG}/404.108 /{TAHUN}', '973 /{KOSONG}/404.108 /{TAHUN}', 'text', '2020-03-19 15:29:06', NULL),
('SURAT_TAGIHAN_TTD', '[SURAT]', 'TTD Surat Tagihan', '<p><b>An. KEPALA BADAN KEUANGAN </b></p>\r\n<p><b>KABUPATEN NGAWI</b></p>\r\n<p><b>KEPALA BIDANG PENGELOLAAN PENDAPATAN</b></p>\r\n<br>\r\n<br>\r\n<br>\r\n<p><b><u>AKHMAD ARWAN ARIFIYANTO, SE</u></b></p>\r\n<p>Penata Tk. I</p>\r\n<p>NIP. 19740614 200312 1 004</p>', '<p><b>An. KEPALA BADAN KEUANGAN </b></p>\r\n<p><b>KABUPATEN NGAWI</b></p>\r\n<p><b>KEPALA BIDANG PENGELOLAAN PENDAPATAN</b></p>\r\n<br>\r\n<br>\r\n<br>\r\n<p><b><u>AGUS SETO BUDI, SE</u></b></p>\r\n<p>Pembina</p>\r\n<p>NIP. 19620421 199003 1 013</p>', 'html', '2020-09-05 02:30:24', 'tanu');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(160) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(160) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(160) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `nip` varchar(25) CHARACTER SET utf8 DEFAULT NULL,
  `telepon` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` enum('YES','NO') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'YES',
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `nama_lengkap`, `email`, `password`, `remember_token`, `role`, `nip`, `telepon`, `is_active`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'admin', 'Admin', 'tes@mail.com', '$2y$10$npovbhEz2WDAJiiNL5L8budNxWgYWC8I3qDJV2n8npWj/Z9af7KU6', 'OvsPT5tSI8FIxBInQQZalpHHaKHzaqAKMIyTlKWl2rB0fLka9k03F8psWeVw', 'admin', '123321', '081212121212', 'YES', NULL, NULL, '2020-02-13 19:15:09', NULL),
(2, 'karangjati', 'User Karangjati', 'keckarangjati1', '$2y$10$rzQffwfUZCbztb9X.SQ61.thZga6JNL2mTnw7zJOok9rE.s9rqrsC', NULL, 'user', '123', NULL, 'YES', '2020-03-23 07:29:39', 'admin', '2020-07-23 01:45:47', 'admin'),
(3, 'tanu', 'tanuwijaya', 'joe.tanuwijaya@gmail.com', '$2y$10$uUF03hGz7WekwEudXWA5l.Vo4faWtkVkcLGU7Oib//Nr2CKyar3Ie', 'wWwfB3bLBfGzhb4CV3bUj6IxzHHxYHAQV2rXM3tDVnRox4YVztpj7OA0wez1', 'admin', '1223456789123`', NULL, 'YES', '2020-07-01 01:02:13', 'admin', '2020-07-19 19:33:53', 'admin'),
(4, 'bringin', 'User Bringin', 'kecbringin@ngawikab.go.id', '$2y$10$HvvKZMM2F4KDLXmfv7pUp.ldzz.Ukg76F.fCeQChtTsPrwK4whKKa', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:44:14', 'admin', '2020-07-23 01:44:14', NULL),
(5, 'geneng', 'User Geneng', 'kecgeneng@ngawikab.go.id', '$2y$10$dbvyxuW7LwQ4igJvZtSZtuV/Hq5igvHuxYb.uHcc.vweuLRwuDNnG', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:46:41', 'admin', '2020-07-23 01:46:41', NULL),
(6, 'gerih', 'User Gerih', 'kecgerih@ngawikab.go.id', '$2y$10$Il2JbSG6jFL7Svv23AQ8g.rHtb8gYKWhui.UoTbF7Zz3DV3IayX9G', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:47:55', 'admin', '2020-07-23 01:47:55', NULL),
(7, 'jogorogo', 'User Jogorogo', 'kecjogorogo@ngawikab.go.id', '$2y$10$BOgBwBEMZnkvohSx4T7NXOg6KloAfrnW/e2TSyQkp6LLcb4.B14Py', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:49:17', 'admin', '2020-07-23 01:49:17', NULL),
(8, 'karanganyar', 'User Karanganyar', 'keckaranganyar@ngawikab.go.id', '$2y$10$DfzIBiFD3fNwhOc1hPu3A.TZ/hP7.6UXyBqE59zRIGK9Biy/.eUt.', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:50:05', 'admin', '2020-07-23 01:50:05', NULL),
(9, 'kasreman', 'User Kasreman', 'keckasreman@ngawikab.go.id', '$2y$10$vc9JVwh8ZVWjTCzNw1jQyu1YCXygL5UOLsNn6M.ZdOhxIA04ctlSy', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:51:05', 'admin', '2020-07-23 01:51:05', NULL),
(10, 'kedunggalar', 'User Kedunggalar', 'keckedunggalar@ngawikab.go.id', '$2y$10$M1c87HT5amaN5RY5D7.NxeXJJ4RDVDxWmbLRFXup05Uh0LiEm00TG', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:51:49', 'admin', '2020-07-23 01:51:49', NULL),
(11, 'kendal', 'User Kendal', 'keckendal@ngawikab.go.id', '$2y$10$EKkyy5MCE0cEKHl78xyBOeyfsbMMOrOrjp4JAeUdGJAC9psyzCJoO', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:52:31', 'admin', '2020-07-23 01:52:31', NULL),
(12, 'kwadungan', 'User Kwadungan', 'keckwadungan@ngawikab.go.id', '$2y$10$KGIKFPwGTL5OXDiYgG1jmu.ephF./QOKL837TawMiPOsBpJbSY8wW', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:53:06', 'admin', '2020-07-23 01:53:06', NULL),
(13, 'mantingan', 'User Mantingan', 'kecmantingan@ngawikab.go.id', '$2y$10$2SwwpNg0wat7xUmT1Kz/suwYcz4tN1mZpAJ3vpiDxNcr/1ygrDXde', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:53:52', 'admin', '2020-07-23 01:53:52', NULL),
(14, 'ngawi', 'User Ngawi', 'kecngawi@ngawikab.go.id', '$2y$10$eWzuL72KZktVZqZYvKJKse4pGpPEstIjpVpBIV7mZuCln13VuzsHS', '4yiH4Wuv03iotZubGWR8EYQEmql40YNNxmepyEeb9p4DXGjo2Sex49JueS6y', 'user', NULL, NULL, 'YES', '2020-07-23 01:54:43', 'admin', '2020-07-23 01:54:43', 'ngawi'),
(15, 'ngrambe', 'User Ngrambe', 'kecngrambe@ngawikab.go.id', '$2y$10$jUaAk1jaxiHQW8ulLf2L4eQWHHvfPSULRomDHQaU1CzFO2vgQL5UK', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:55:58', 'admin', '2020-07-23 01:55:58', NULL),
(16, 'padas', 'User Padas', 'kecpadas@ngawikab.go.id', '$2y$10$Yi2Rs6IruMZlavzkZILv2.SUrq7zKesB2M3KoOWlpITOzsQwCg6iG', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:56:40', 'admin', '2020-07-23 01:56:40', NULL),
(17, 'pangkur', 'User Pangkur', 'kecpangkur@ngawikab.go.id', '$2y$10$nDEVCC95uTYn1b5ukwKwD.0oiRCIbZZhIXKgGBaNFdsm..P51dGeC', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:57:24', 'admin', '2020-07-23 01:57:24', NULL),
(18, 'paron', 'User Paron', 'kecparon@ngawikab.go.id', '$2y$10$bV/as1X5dCc5pElhruz.HuACfcWn4209umB44PrpwN40PKXuoQIFm', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:58:08', 'admin', '2020-07-23 01:58:08', NULL),
(19, 'pitu', 'User Pitu', 'kecpitu@ngawikab.go.id', '$2y$10$pPPShFnx6HyuJnkPEwlj8.MVslTPQyZGY8EG8D0Bh43/82x.9X6cO', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 01:58:47', 'admin', '2020-07-23 01:58:47', NULL),
(20, 'sine', 'User Sine', 'kecsine@ngawikab.go.id', '$2y$10$1V.pG3xIP1utFLrIjrxEWeNNneMhPplRIVC/MfZ7AIMC5zoc8nK6K', 'zfmDnx2OL8WCeBCs2CyaKJDhqhGdjcfTiXkdwPtCT4AGKfndAMW1014oAcuM', 'user', NULL, NULL, 'YES', '2020-07-23 01:59:28', 'admin', '2020-07-23 01:59:28', 'sine'),
(21, 'widodaren', 'User Widodaren', 'kecwidodaren@ngawikab.go.id', '$2y$10$b8PgL3R4uvayskwjpNpNLOFZ7XIAnu3/KxeaxXZDe5BVUzTiPD6Hm', NULL, 'user', NULL, NULL, 'YES', '2020-07-23 02:00:07', 'admin', '2020-07-23 02:00:07', NULL),
(22, 'tagihan', 'tuhu hariyana', 'tuhu@ngawikab.go.id', '$2y$10$uXySdHs/5iKoWqx5C3WvZ.CbGvtgpueAxqJjMVo3obTXT2wKpB1EO', 'BV5eLiUXjYMBMcyeKJhRLCXXodVhNVyhcb5sxYFPFmn72lu3jmwNDUqkLNbU', 'admin', NULL, NULL, 'YES', '2020-09-03 07:43:55', 'tanu', '2020-09-03 07:44:28', 'tanu');

-- --------------------------------------------------------

--
-- Table structure for table `users_kecamatan`
--

CREATE TABLE `users_kecamatan` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `kd_kecamatan` char(7) NOT NULL COMMENT 'kd_propinsi + kd_dati2 + kd_kecamatan',
  `is_primary` enum('YES','NO') NOT NULL DEFAULT 'NO',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(150) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_kecamatan`
--

INSERT INTO `users_kecamatan` (`user_id`, `kd_kecamatan`, `is_primary`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '3521161', 'NO', '0000-00-00 00:00:00', NULL, NULL, NULL),
(1, '3521162', 'NO', '0000-00-00 00:00:00', NULL, NULL, NULL),
(2, '3521080', 'NO', '2020-03-23 14:29:39', 'admin', '2020-03-23', NULL),
(4, '3521090', 'NO', '2020-07-23 08:44:14', 'admin', '2020-07-23', NULL),
(5, '3521050', 'NO', '2020-07-23 08:46:41', 'admin', '2020-07-23', NULL),
(6, '3521162', 'NO', '2020-07-23 08:47:55', 'admin', '2020-07-23', NULL),
(7, '3521030', 'NO', '2020-07-23 08:49:17', 'admin', '2020-07-23', NULL),
(8, '3521161', 'NO', '2020-07-23 08:50:05', 'admin', '2020-07-23', NULL),
(9, '3521163', 'NO', '2020-07-23 08:51:05', 'admin', '2020-07-23', NULL),
(10, '3521130', 'NO', '2020-07-23 08:51:49', 'admin', '2020-07-23', NULL),
(11, '3521040', 'NO', '2020-07-23 08:52:31', 'admin', '2020-07-23', NULL),
(12, '3521060', 'NO', '2020-07-23 08:53:06', 'admin', '2020-07-23', NULL),
(13, '3521160', 'NO', '2020-07-23 08:53:52', 'admin', '2020-07-23', NULL),
(14, '3521110', 'NO', '2020-07-23 08:54:43', 'admin', '2020-07-23', NULL),
(15, '3521020', 'NO', '2020-07-23 08:55:58', 'admin', '2020-07-23', NULL),
(16, '3521100', 'NO', '2020-07-23 08:56:40', 'admin', '2020-07-23', NULL),
(17, '3521070', 'NO', '2020-07-23 08:57:24', 'admin', '2020-07-23', NULL),
(18, '3521120', 'NO', '2020-07-23 08:58:08', 'admin', '2020-07-23', NULL),
(19, '3521140', 'NO', '2020-07-23 08:58:47', 'admin', '2020-07-23', NULL),
(20, '3521010', 'NO', '2020-07-23 08:59:28', 'admin', '2020-07-23', NULL),
(21, '3521150', 'NO', '2020-07-23 09:00:07', 'admin', '2020-07-23', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`conf_code`),
  ADD KEY `conf_groups` (`conf_groups`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Indexes for table `users_kecamatan`
--
ALTER TABLE `users_kecamatan`
  ADD PRIMARY KEY (`user_id`,`kd_kecamatan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_kecamatan`
--
ALTER TABLE `users_kecamatan`
  ADD CONSTRAINT `users_kecamatan_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
