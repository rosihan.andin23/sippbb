<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarif extends Model
{
	protected $connection = 'oracle';

	protected $table = 'tarif';
}
