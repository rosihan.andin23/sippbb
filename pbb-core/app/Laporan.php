<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Config;

class Laporan extends Model
{
	protected $connection = 'oracle';

	protected $table = 'sppt';
	
	public function getLaporanPenerimaan($filter)
	{
		$configs = Config::getValueByGroup('ALL');
		$buku = Buku::whereIn('KD_BUKU', $filter['buku'])
			->where('THN_AWAL', '<=', $filter['tahun'])
			->where('THN_AKHIR', '>=', $filter['tahun'])
			->get();
		
		$where_tagih = [];
		$where_bayar = [];
		$where_buku = [];
		
		if($buku) foreach($buku as $row)
		{
			$where_buku[] = '(t.PBB_YG_HARUS_DIBAYAR_SPPT BETWEEN '.$row->nilai_min_buku.' AND '.$row->nilai_max_buku.')';
		}
		if($where_buku) $where_tagih[] = '('.implode(' OR ', $where_buku).')';
		
		//check auth kecamatan
		if(Auth::user()->role != 'admin') 
		{
			$kodeKec = Auth::user()->userKecamatan->pluck('kd_kecamatan')->toArray();
			$kodeKec = '\''.implode('\',\'', $kodeKec).'\'';
			$where_bayar[] = 'KD_PROPINSI||KD_DATI2||KD_KECAMATAN IN ('.$kodeKec.')';
			$where_tagih[] = 't.KD_PROPINSI||t.KD_DATI2||t.KD_KECAMATAN IN ('.$kodeKec.')';
		}
		
		$where_tagih = $where_tagih ? ' AND '.implode(" AND ", $where_tagih) : '';
		$where_bayar = $where_bayar ? ' AND '.implode(" AND ", $where_bayar) : '';
		
		$query = "SELECT
			kc.NM_KECAMATAN,
			kl.NM_KELURAHAN,
			LAP.*
		FROM
		(
			SELECT 
				t.KD_PROPINSI||t.KD_DATI2||t.KD_KECAMATAN as kode_kec, 
				t.KD_PROPINSI||t.KD_DATI2||t.KD_KECAMATAN||t.KD_KELURAHAN as kode_kel, 
				COUNT(t.KD_PROPINSI) sppt_tagih,
				SUM(t.PBB_YG_HARUS_DIBAYAR_SPPT) as tot_tagih,
				COUNT(p.kd_sppt) sppt_bayar,
				
				SUM(CASE WHEN 
					t.STATUS_PEMBAYARAN_SPPT = 0 AND t.TGL_JATUH_TEMPO_SPPT < SYSDATE
					THEN
						(t.PBB_YG_HARUS_DIBAYAR_SPPT * (:denda/100)) * FLOOR(MONTHS_BETWEEN(SYSDATE, t.TGL_JATUH_TEMPO_SPPT))
					ELSE 0 
				END) tot_denda_tagih,
				
				SUM(tot_bayar) as tot_bayar,
				SUM(tot_denda_bayar) as tot_denda_bayar
			FROM 
				SPPT t
			LEFT JOIN
			(
				SELECT 
					KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP||THN_PAJAK_SPPT AS kd_sppt,
					KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN AS kode_kel, 
					SUM (JML_SPPT_YG_DIBAYAR) AS tot_bayar,
					SUM (DENDA_SPPT) AS tot_denda_bayar
				FROM PEMBAYARAN_SPPT
				WHERE 
					THN_PAJAK_SPPT = :tahun
					AND KD_PROPINSI = :kd_propinsi
					AND KD_DATI2 = :kd_dati2
					$where_bayar
					AND TGL_PEMBAYARAN_SPPT >= TO_DATE(:tgl_awal,'DD/MM/YYYY') 
					AND TGL_PEMBAYARAN_SPPT <= TO_DATE(:tgl_akhir,'DD/MM/YYYY')
				GROUP BY KD_PROPINSI, KD_DATI2, KD_KECAMATAN, KD_KELURAHAN, KD_BLOK, NO_URUT, KD_JNS_OP, THN_PAJAK_SPPT
			) p ON p.kd_sppt = t.KD_PROPINSI||t.KD_DATI2||t.KD_KECAMATAN||t.KD_KELURAHAN||t.KD_BLOK||t.NO_URUT||t.KD_JNS_OP||t.THN_PAJAK_SPPT
			WHERE 
				t.THN_PAJAK_SPPT = :tahun
				AND t.KD_PROPINSI = :kd_propinsi
				AND t.KD_DATI2 = :kd_dati2
				$where_tagih
			GROUP BY 
				t.KD_PROPINSI, t.KD_DATI2, t.KD_KECAMATAN, t.KD_KELURAHAN
		) lap
		JOIN
			REF_KECAMATAN kc ON kc.KD_PROPINSI||kc.KD_DATI2||kc.KD_KECAMATAN = LAP.KODE_KEC
		JOIN
			REF_KELURAHAN kl ON kl.KD_PROPINSI||kl.KD_DATI2||kl.KD_KECAMATAN||kl.KD_KELURAHAN = LAP.KODE_KEL
		ORDER BY
			LAP.KODE_KEC ASC, LAP.KODE_KEL";
		
		$param = [
			'kd_propinsi' => $configs['KD_PROPINSI'],
			'kd_dati2' => $configs['KD_DATI2'],
			'tahun' => $filter['tahun'],
			'tgl_awal' => $filter['tgl_awal'],
			'tgl_akhir' => $filter['tgl_akhir'],
			'denda' => $configs['DENDA_ALL'],
		];		
		$results = DB::connection($this->connection)->select($query, $param);
		return $results;		
	}
	
	public function getLaporanTunggakan($filter, $kodeOp=false, $configs=false)
	{
		if(!$configs) $configs = Config::getValueByGroup('KODE');
		
		$selKodeOp = 's.KD_PROPINSI||s.KD_DATI2||s.KD_KECAMATAN||s.KD_KELURAHAN||s.KD_BLOK||s.NO_URUT||s.KD_JNS_OP';
		$selNOP = 's.KD_PROPINSI||\'.\'||s.KD_DATI2||\'.\'||s.KD_KECAMATAN||\'.\'||s.KD_KELURAHAN||\'.\'||s.KD_BLOK||\'.\'||s.NO_URUT||\'.\'||s.KD_JNS_OP';
		$selTelat = 'FLOOR(MONTHS_BETWEEN(SYSDATE, s.TGL_JATUH_TEMPO_SPPT))';
		
		$isKelurahan = isset($filter['kelurahan']);
		$kodeBreakdown = explode('.',$filter[$isKelurahan ? 'kelurahan' : 'kecamatan']);
		
		//check auth kecamatan
		if(Auth::user()->role != 'admin') 
		{
			$allowedKodeKec = Auth::user()->userKecamatan->pluck('kd_kecamatan')->toArray();
			$filterKecValid = $kodeBreakdown[0].$kodeBreakdown[1].$kodeBreakdown[2];
			if(!in_array($filterKecValid, $allowedKodeKec)) return false;
		}

		$param = [
			'kd_propinsi' => $configs['KD_PROPINSI'],
			'kd_dati2' => $configs['KD_DATI2'],
			'kd_kecamatan' => $kodeBreakdown[2],
			'thn_start' => $filter['thn_start'], 
			'thn_end' => $filter['thn_end'], 
		];

		$where = [
			's.STATUS_PEMBAYARAN_SPPT = 0',
			's.KD_PROPINSI = :kd_propinsi',
			's.KD_DATI2 = :kd_dati2',
			's.KD_KECAMATAN = :kd_kecamatan',
			's.THN_PAJAK_SPPT BETWEEN :thn_start AND :thn_end',
		];
		if($isKelurahan) 
		{
			$where[] = 's.KD_KELURAHAN = :kd_kelurahan';
			$param['kd_kelurahan'] = $kodeBreakdown[3];
		}
		if(isset($filter['print-tgl']) && $filter['print-tgl'])
		{
			$param['print_tgl'] = $filter['print-tgl'];
			$selTelat = 'FLOOR(MONTHS_BETWEEN(TO_DATE(:print_tgl,\'DD/MM/YYYY\') , s.TGL_JATUH_TEMPO_SPPT))';
		}
		if(isset($filter['keyword'])) 
		{
			$where[] = '('.$selKodeOp.' LIKE :keyword OR s.NM_WP_SPPT LIKE :keyword OR s.JLN_WP_SPPT LIKE :keyword OR o.JALAN_OP LIKE :keyword)';
			$param['keyword'] = '%'.$filter['keyword'].'%';
		}
		if(isset($filter['buku']) && $filter['buku'])
		{
			$where_buku = [];
			$buku = Buku::whereIn('KD_BUKU', $filter['buku'])
				->where('THN_AWAL', '<=', $filter['thn_start'])
				->where('THN_AKHIR', '>=', $filter['thn_end'])
				->get();
				
			if($buku) 
			{
				foreach($buku as $row) $where_buku[] = '(s.PBB_YG_HARUS_DIBAYAR_SPPT BETWEEN '.$row->nilai_min_buku.' AND '.$row->nilai_max_buku.')';
				$where[] = '('.implode(' OR ', $where_buku).')';
			}
		}
		if($kodeOp && is_array($kodeOp))
		{
			$in = '';
			foreach ($kodeOp as $i => $item)
			{
				$key = "kode_op_$i";
				$in .= ":$key,";
				$param[$key] = $item;
			}
			$in = rtrim($in,","); 
			$where[] = $selKodeOp.' IN ('.$in.')';
		}
		
		$where = implode(' AND ', $where);
		
		
		$query = "SELECT
			$selNOP AS NOP,
			$selKodeOp AS KODE_OP,
			s.THN_PAJAK_SPPT,
			
			s.NM_WP_SPPT, 
			s.JLN_WP_SPPT,
			s.BLOK_KAV_NO_WP_SPPT,
			s.RW_WP_SPPT,
			s.RT_WP_SPPT,
			s.KELURAHAN_WP_SPPT,
			s.KOTA_WP_SPPT,

			o.JALAN_OP, 
			o.BLOK_KAV_NO_OP,
			o.RW_OP,
			o.RT_OP,
			o.KD_JNS_OP,
			o.SUBJEK_PAJAK_ID,
			
			kc.NM_KECAMATAN,
			kl.NM_KELURAHAN,
			
			s.PBB_TERHUTANG_SPPT,
			s.PBB_YG_HARUS_DIBAYAR_SPPT, 
			s.TGL_JATUH_TEMPO_SPPT,
			$selTelat AS BULAN_TELAT,
			s.STATUS_PEMBAYARAN_SPPT
		FROM
			SPPT s	
		LEFT JOIN
			DAT_OBJEK_PAJAK o ON o.KD_PROPINSI = s.KD_PROPINSI
				AND o.KD_DATI2 = s.KD_DATI2
				AND o.KD_KECAMATAN = s.KD_KECAMATAN
				AND o.KD_KELURAHAN = s.KD_KELURAHAN
				AND o.KD_BLOK = s.KD_BLOK
				AND o.NO_URUT = s.NO_URUT
				AND o.KD_JNS_OP = s.KD_JNS_OP
		JOIN
			REF_KECAMATAN kc ON kc.KD_PROPINSI = o.KD_PROPINSI AND kc.KD_DATI2=o.KD_DATI2 AND kc.KD_KECAMATAN = o.KD_KECAMATAN
		JOIN
			REF_KELURAHAN kl ON kl.KD_PROPINSI=o.KD_PROPINSI AND kl.KD_DATI2=o.KD_DATI2 AND kl.KD_KECAMATAN=o.KD_KECAMATAN AND kl.KD_KELURAHAN = o.KD_KELURAHAN

		WHERE
			$where
		ORDER BY
			KODE_OP ASC, s.THN_PAJAK_SPPT ASC";
		
		
		$results = DB::connection($this->connection)->select($query, $param);
		return $results;		
	}
	
	public function getKantor($configs = false)
	{
		if(!$configs) $configs = Config::getValueByGroup('SURAT');
		
		$data = DB::connection($this->connection)
			->table('REF_ADM_KPPBB RAK')
			->join('REF_KPPBB RK', 'RAK.KD_KPPBB', '=', 'RK.KD_KPPBB')
			->where('KD_PROPINSI', $configs['KD_PROPINSI'])
			->where('KD_DATI2', $configs['KD_DATI2'])
			->select('RK.*')
			->first();
			
		if($data)
		{
			$data->kode_pos = $configs['KANTOR_KODE_POS'];
			$data->email = $configs['KANTOR_EMAIL'];
			$data->website = $configs['KANTOR_WEBSITE'];
		}
		return $data;		
	}
	
}
