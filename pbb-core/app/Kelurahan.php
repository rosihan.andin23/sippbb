<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Kelurahan extends Model
{
	protected $connection = 'oracle';

	protected $table = 'ref_kelurahan';
	protected $primaryKey = 'kd_kelurahan';
	protected $keyType = 'string';
	protected $appends = array('nm_kecamatan');
	
	public function kecamatan()
	{
		return $this
			->belongsTo('App\Kecamatan', 'kd_kecamatan', 'kd_kecamatan')
			->where('kd_propinsi', $this->kd_propinsi)
			->where('kd_dati2', $this->kd_dati2);
	}
	
	public function scopeKodeWithSeparatorEqual($query, $kode=false)
	{
		return $query->where(DB::raw('KD_PROPINSI||\'.\'||KD_DATI2||\'.\'||KD_KECAMATAN||\'.\'||KD_KELURAHAN'),  $kode);
	}
	
	public function getKodeWithSeparatorAttribute($value)
	{
		return $this->kd_propinsi.'.'.$this->kd_dati2.'.'.$this->kd_kecamatan.'.'.$this->kd_kelurahan;
	}
	
	public function getNmKecamatanAttribute($value)
	{
		return $this->kecamatan->nm_kecamatan;
	}
	
}

