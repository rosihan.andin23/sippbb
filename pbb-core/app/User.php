<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
	use Notifiable;
	
	public $HOME = '/dashboard_kec';
	protected $appends = array('kecamatan_str');

	protected $fillable = [
		'username', 'nama_lengkap', 'email', 'password', 'nip', 'telepon', 'is_active', 'role'
	];

	protected $hidden = [
		'password', 'remember_token',
	];

	protected $casts = [
		'email_verified_at' => 'datetime',
	];
	
	public function userKecamatan()
	{
		return $this->hasMany('App\UserKecamatan', 'user_id');
	}
	
	public function getKecamatanAttribute($value)
	{
		$data = $this->userKecamatan->pluck('kd_kecamatan')->all();
		return Kecamatan::kodeIn($data)->get();
	}
	
	public function getKecamatanStrAttribute($value)
	{
		return $this->kecamatan->implode('nm_kecamatan', ', ');
	}
	
	public function getNameAttribute($value)
	{
		return $this->nama_lengkap;
	}
	
	public static function boot() 
	{
		parent::boot();

		static::updating(function($table)  {
			if( Auth::user()) $table->updated_by = Auth::user()->username;
		});
	
		static::creating(function($table)  {
			if( Auth::user()) $table->created_by = Auth::user()->username;
		});
	}
}
