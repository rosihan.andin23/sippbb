<?php

namespace App\Libraries\LetterTemplate;

use App\Libraries\fpdf\FPDF;

class DefaultPdfTemplate extends FPDF
{
	protected $rowSpan = [];
	
	function SetColsWidth($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}

	function SetColsAlign($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}

	function Row($data, $lineHeight = 4.2)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=$lineHeight*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();

			//Draw the border & detect rowspan
			if(isset($this->rowSpan[$i]))
			{
				$this->rowSpan[$i]->h += $h;
				if($this->rowSpan[$i]->y > $y) $this->rowSpan[$i]->y = $y;
				
				if($this->rowSpan[$i]->status == 'end' )
				{
					$this->Rect($x, $this->rowSpan[$i]->y, $w, $this->rowSpan[$i]->h);
					unset($this->rowSpan[$i]);
				} 
			}
			else $this->Rect($x,$y,$w,$h);
			
			//Print the text
			$this->MultiCell($w,$lineHeight,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	
	function StartRowSpan($colIndexs)
	{
		if(!is_array($colIndexs)) $colIndexs = [$colIndexs];
		foreach($colIndexs as $i) 
		{
			$this->rowSpan[$i] = (object)[
				'status' => 'start',
				'y' => $this->GetY(),
				'h' => 0,
			];
		}		
	}
	
	function EndRowSpan($colIndexs)
	{
		if(!is_array($colIndexs)) $colIndexs = [$colIndexs];
		foreach($colIndexs as $i) if(isset($this->rowSpan[$i])) $this->rowSpan[$i]->status = 'end';
	}
	
	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger) 
		{
			// draw unfinished rowspan
			if(is_array($this->widths))foreach($this->widths as $col => $w)
			{
				$x = $this->GetX();
				$y = $this->GetY();
				if(isset($this->rowSpan[$col]))
				{
					$this->Rect($x, $this->rowSpan[$col]->y, $w, $this->rowSpan[$col]->h);
					$this->rowSpan[$col]->h = 0;
				}
				$this->SetXY($x+$w, $y);
			}
			
			$this->AddPage($this->CurOrientation);
		}
	}

	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
	
	function Footer()
	{
		// Position at 1.5 cm from bottom
		$this->SetY(-15);
		// Arial italic 8
		$this->SetFont('Arial','I',8);
		// Page number
		$this->Cell(0,10,'Halaman '.$this->PageNo().' dari {nb}',0,0,'R');
	}
}
?>
