<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
	protected $connection = 'oracle';

	protected $table = 'ref_buku';
	protected $primaryKey = 'kd_buku';
	protected $keyType = 'string';
	public $incrementing = false;

}
