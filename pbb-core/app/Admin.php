<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
	use Notifiable;
	
	public $HOME = '/dashboard_adm';

	protected $connection = 'oracle';

	protected $table = 'dat_login';
	protected $primaryKey = 'nm_login';
	protected $keyType = 'string';
	public $incrementing = false;

	protected $fillable = [];
	protected $hidden = ['password'];
	
	public function getNameAttribute($value)
	{
		return $this->nm_login;
	}
	
	public function getIdAttribute($value)
	{
		return $this->nm_login;
	}

	
}
