<?php

namespace App;

use App\Model\UserStamp;

class UserKecamatan extends UserStamp
{
	protected $table = 'users_kecamatan';
	
	protected $fillable = [
		'user_id', 'kd_kecamatan', 'is_primary'
	];
	
	public function getKecamatanAttribute($value)
	{
		return $this->kd_kecamatan;
	}
	
	public function scopeCurrent($query)
	{
		return $query->where('user_id', $this->user_id)->where('kd_kecamatan',$this->kd_kecamatan);
	}

	
}
