<?php

namespace App\Http\Controllers\Kecamatan;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Kecamatan;
use App\StatisticKecamatan;
use App\Config;


class DashboardController extends Controller
{
	
	public function view(Request $request, $kodeKecamatan = false, $tahun=false)
	{
		$listKecamatan = Kecamatan::myKecamatan()->get();
		$pack = [
			'tahun' => date('Y')-1,
			'minTahun' => Config::getValue('START_TAHUN', 1990),
			'listKecamatan' => $listKecamatan,
		];
		return view('kecamatan.dashboard.index', $pack);
	}
	
	public function load(Request $request, $kodeKecamatan, $tahun=false)
	{
		$start = microtime(true);
		if(!$kodeKecamatan)
		{
			$kecamatan = Kecamatan::myKecamatan()->first();
			if(!$kecamatan) return response()->jsonFail('Kecamatan tidak ditemukan');
			$kodeKecamatan = $kecamatan->kode;
		}
		
		$tahun = !$tahun ? date('Y')-1 : $tahun;
		$stat = new StatisticKecamatan($kodeKecamatan);
		$stat->getCollections();
		
		$pack = [
			'tahun' => $tahun,
			'kodeKecamatan' => $kodeKecamatan,
			'namaKecamatan' => $stat->kecamatan->nm_kecamatan,
			
			'dataPerTahun' => $stat->getGroupByTahun(),
			'dataPerWilayah' => $stat->getGroupByKelurahan($tahun),
			'dataPerBuku' => $stat->getGroupByBuku($tahun),
			'publishDate' => date('d/m/Y', strtotime($stat->publishDate)),
			'execTime' => microtime(true) - $start,
		];
		return response()->jsonSuccess($pack);
	}
	
	public function reload()
	{
		$stat = new StatisticKecamatan();
		$stat->clearCache();
		return redirect()->route('kecamatan.dashboard');
	}
}
