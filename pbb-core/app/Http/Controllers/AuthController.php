<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;

use App\Admin;
use App\User;

class AuthController extends Controller
{
	use RedirectsUsers, ThrottlesLogins;
	
	protected $guardName = null;
	
	public function __construct()
	{
		$this->middleware('guest')->except('logout');
	}
	
	public function view()
	{
		return view('auth.main');
	}
	
	public function username()
	{
		return 'username';
	}
	
	public function login(Request $request)
	{
		$request->validate([
			$this->username() => 'required|string',
			'password' => 'required|string',
		]);

		if (method_exists($this, 'hasTooManyLoginAttempts') &&  $this->hasTooManyLoginAttempts($request)) 
		{
			$this->fireLockoutEvent($request);
			return $this->sendLockoutResponse($request);
		}

		if ($this->attemptLogin($request)) 
		{
			$request->session()->regenerate();
			$this->clearLoginAttempts($request);
			if($this->guard()->check()) return redirect()->intended($this->guard()->user()->HOME);
		}

		$this->incrementLoginAttempts($request);

		throw ValidationException::withMessages([
			$this->username() => [trans('auth.failed')],
		]);
	}

	protected function attemptLogin(Request $request)
	{
		$credentials = array_merge(
			$request->only($this->username(), 'password'),
			['is_active' => 'YES', 'role' => 'user']
		);
		
		$res = $this->guard()->attempt($credentials, $request->filled('remember'));
		if($res) return $res;
		
		// jika gagal login kecamatan maka login admin
		$this->changeGuard('admin');
		$credentials['role'] = 'admin';
		$res = $this->guard()->attempt($credentials, $request->filled('remember'));
		if($res) return $res;
		
		// jika gagal login dengan table admin di oracle
		/* $admin = Admin::where('password', $request->input('password'))->find($request->input('username'));
		if(!$admin) return false;		
		return ($this->guard()->loginUsingId($admin->id)); */
	}

	public function logout(Request $request)
	{
		$this->guard()->logout();
		$this->changeGuard('admin');
		$this->guard()->logout();
		$request->session()->invalidate();
		$request->session()->regenerateToken();
		return redirect('/');
	}
	
	protected function changeGuard($name=null)
	{
		$this->guardName = $name;
	}

	protected function guard()
	{
		return Auth::guard($this->guardName);
	}
}
