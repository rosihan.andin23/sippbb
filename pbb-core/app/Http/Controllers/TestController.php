<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Admin;
use App\User;

class TestController extends Controller
{
	
	public function oracle()
	{
		//Auth::guard()->logout();
		//Auth::guard('admin')->logout();
		
		
		$admin = Admin::find('GIH1');
		$user = User::find(1);
		
		Auth::guard('admin')->login($admin);
		dd(Auth::guard('admin')->user());
		
	}
	
	public function oracleManual()
	{
		echo '<h1>oracle</h1>';
		$conn = oci_connect('MYPBB', 'asas123', 'localhost/LOCAL');
		if (!$conn) {
			$e = oci_error();
			trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
		}

		$stid = oci_parse($conn, 'SELECT * FROM REF_KECAMATAN');
		oci_execute($stid);

		echo "<table border='1'>\n";
		while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) 
		{
			echo "<tr>\n";
			foreach ($row as $item) {
				echo "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
			}
			echo "</tr>\n";
		}
		echo "</table>\n";
	}
	
	
}
