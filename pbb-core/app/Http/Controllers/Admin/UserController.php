<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\User;
use App\Kecamatan;

class UserController extends Controller
{
	
	public function index()
	{
		return view('admin.user.index');
	}
	
	public function datatable()
	{
		$data = User::select('id', 'username', 'nama_lengkap','telepon','nip','email','role','is_active')->get()->toArray();
		return response()->json(['data'=>$data]);		
	}
	
	public function form(Request $request, $id=false)
	{
		$pack = [
			'id' => $id,
			'data' => $id ? User::find($id) : '',
			'kecamatan' => Kecamatan::all(),
		];
		return view('admin.user.form', $pack);
	}
	
	public function store(Request $request, $id=false)
	{
		$validator = Validator::make($request->all(), [
			'username' => 'required|unique:users,username,'.$id,
			'nama_lengkap' => 'required',
			'role' => 'required',
			'email' => 'nullable',
			'nip' => 'nullable',
			'telepon' => 'nullable',
			'kecamatan.*' => ['nullable',  Rule::in(Kecamatan::all()->pluck('kode')->toArray())],
			'password' => $id ? 'nullable|min:4' : 'required|min:4',
    ]);
		
		if ($validator->fails()) 
			return response()->jsonFail($validator->errors()->all());
		
		$result = DB::transaction(function() use ($id, $request)
		{
			$model = $id ? User::find($id) : new User;
			foreach($model->getFillable() as $field) if($request->has($field))
			{
				$value = $request->input($field);
				if($field == 'password') 
				{
					if(!$value) continue;
					$value = Hash::make($value);
				}
				$model->$field = $value;
			}
			$model->save();
			
			if($model->userKecamatan) foreach($model->userKecamatan as $userKec) 
				if(!in_array($userKec->kd_kecamatan, $request->input('kecamatan')))
					$userKec->current()->delete();
				
			if(is_array($request->input('kecamatan')))foreach($request->input('kecamatan') as $kdKec)
				$model->userKecamatan()->updateOrCreate(['kd_kecamatan'=>$kdKec]);
			
			return $model;
		});

		return $result ? response()->jsonSuccess($result) : response()->jsonFail('Data gagal disimpan');
	}
	
	
	public function profile(Request $request)
	{
		$pack = [
			'data' => Auth::user(),
			'kecamatan' => Kecamatan::all(),
		];
		return view('admin.user.profile', $pack);
	}
	
	public function profileSubmit(Request $request, $id=false)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'nullable',
			'telepon' => 'nullable',
			'password_lama' => 'nullable|min:4',
			'password_baru' => 'nullable|min:4',
			'ulangi_password' => 'nullable|min:4',
    ]);
		
		if ($validator->fails()) 
			return response()->jsonFail($validator->errors()->all());
		
		$user = Auth::user();
		$user->email = $request->input('email');
		$user->telepon = $request->input('telepon');
		
		if($request->input('password_lama') || $request->input('password_baru') || $request->input('ulangi_password'))
		{
			$validator = Validator::make($request->all(), [
				'password_lama' => 'required|min:4',
				'password_baru' => 'required|min:4',
				'ulangi_password' => 'required|min:4|same:password_baru',
			]);
			
			if ($validator->fails()) 
				return response()->jsonFail($validator->errors()->all());
			
			if (!(Hash::check($request->get('password_lama'), Auth::user()->password))) 
				return response()->jsonFail('Password lama anda tidak sesuai');
			
			$user->password = Hash::make($request->get('password_baru'));
		}
		
		$user->save();
		
		return response()->jsonSuccess('Data berhasil disimpan');
	}
	
	
	
	public function activate(Request $request, $id=false, $state=false)
	{
		if(!$id || !in_array($state, ['YES','NO'])) return response()->jsonFail('Data tidak ditemukan');
		
		$model = User::find($id);
		if (!$model) return response()->jsonFail('Data tidak ditemukan');
		$model->is_active = $state;
		$model->save();		
		return response()->jsonSuccess($model);
	}
	
	
	public function delete(Request $request, $id=false)
	{
		if(!$id) return response()->jsonFail('Data tidak ditemukan');
		
		$model = User::find($id);
		if (!$model) return response()->jsonFail('Data tidak ditemukan');
		
		$model->delete();		
		return response()->jsonSuccess($model);
	}
	
}
