<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Buku;

class BukuController extends Controller
{
	
	public function index()
	{
		return view('admin.buku.index');
	}
	
	public function datatable()
	{
		$data = Buku::all()->toArray();
		return response()->json(['data'=>$data]);		
	}
		
}
