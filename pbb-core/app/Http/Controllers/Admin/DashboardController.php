<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Kecamatan;
use App\StatisticDati2;
use App\Config;

class DashboardController extends Controller
{
	
	public function view(Request $request, $kodeKecamatan=false)
	{
		$listKecamatan = Kecamatan::myKecamatan()->get();
		$pack = [
			'tahun' => date('Y')-1,
			'kodeKecamatan' => $kodeKecamatan,
			'minTahun' => Config::getValue('START_TAHUN', 1990),
			'listKecamatan' => $listKecamatan,
		];
		return $kodeKecamatan ? view('kecamatan.dashboard.index', $pack) : view('admin.dashboard.index', $pack);
	}
	
	public function load(Request $request, $kodeKecamatan=false, $tahun=false)
	{
		$start = microtime(true);
		$tahun = !$tahun ? date('Y')-1 : $tahun;
		
		$stat = new StatisticDati2();
		$stat->getCollections();
		
		$pack = [
			'tahun' => $tahun,
			'kodeKecamatan' => 'Semua Kec',
			
			'dataPerTahun' => $stat->getGroupByTahun(),
			'dataPerWilayah' => $stat->getGroupByKecamatan($tahun),
			'dataPerBuku' => $stat->getGroupByBuku($tahun),
			'publishDate' => date('d/m/Y', strtotime($stat->publishDate)),
			'execTime' => microtime(true) - $start,
		];
		return response()->jsonSuccess($pack);
	}
	
	public function reload()
	{
		$stat = new StatisticKecamatan();
		$stat->clearCache();
		return redirect()->route('admin.dashboard');
	}
}
