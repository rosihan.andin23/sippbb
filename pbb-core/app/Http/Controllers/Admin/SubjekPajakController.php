<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\SubjekPajak;
use App\Kecamatan;
use App\Config;

class SubjekPajakController extends Controller
{
	public function index(Request $request)
	{
		$filter = $request->validate([
			'kecamatan' => 'nullable',
			'kelurahan' => 'nullable',
			'keyword' => 'nullable',
    ]);
		
		$pack = [
			'filter' => $filter,
			'kecamatan' => Kecamatan::myKecamatan()->get(),
			'encodedFilter' => $filter ? encrypt(json_encode($filter)) : false,
		];
		return view('admin.subjek_pajak.index', $pack);
	}
	
	public function datatable(Request $request, $encodedFilter)
	{
		$filter = json_decode(decrypt($encodedFilter), true);
		$model = new SubjekPajak();
		$data = $model->getData($filter);
		return response()->json(['data'=>$data]);		
	}
	
	public function detail(Request $request, $id)
	{
		$model = new SubjekPajak();
		$pack = [
			'id' => $id,
			'denda' => Config::getValue('DENDA_ALL'),
			'data' => $model->getDetail($id),
		];
		return view('admin.subjek_pajak.detail', $pack);
	}
	


}
