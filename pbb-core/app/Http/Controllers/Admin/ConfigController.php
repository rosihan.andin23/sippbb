<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

use App\Config;

class ConfigController extends Controller
{
	
	public function index()
	{
		return view('admin.config.index');
	}
	
	public function datatable()
	{
		$data = Config::select('conf_code', 'conf_type', 'conf_name','conf_value')->get()->toArray();
		return response()->json(['data'=>$data]);		
	}
	
	public function form(Request $request, $id=false)
	{
		$pack = [
			'id' => $id,
			'data' => $id ? Config::find($id) : '',
		];
		return view('admin.config.form', $pack);
	}
	
	public function store(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'conf_value' => 'nullable',
    ]);
		
		if ($validator->fails()) 
			return response()->jsonFail($validator->errors()->all());
		
		$result = DB::transaction(function() use ($id, $request)
		{
			$model = Config::find($id);
			foreach($model->getFillable() as $field) if($request->has($field))
			{
				$value = $request->input($field);
				if($field == 'password') 
				{
					if(!$value) continue;
					$value = Hash::make($value);
				}
				$model->$field = $value;
			}
			$model->save();
			
			return $model;
		});

		return $result ? response()->jsonSuccess($result) : response()->jsonFail('Data gagal disimpan');
	}

	
}
