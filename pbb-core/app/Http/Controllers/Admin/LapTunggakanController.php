<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Buku;
use App\Kecamatan;
use App\Laporan;
use App\Config;

class LapTunggakanController extends Controller
{
	
	public function index()
	{
		$pack = [
			'buku' => Buku::all(),
			'kecamatan' => Kecamatan::myKecamatan()->get(),
		];
		return view('admin.lap_tunggakan.index', $pack);
	}
	
	public function filter(Request $request)
	{
		$filter = $request->validate([
			'thn_start' => 'required',
			'thn_end' => 'required',
			'buku' => 'nullable',
			'kecamatan' => 'required',
			'kelurahan' => 'nullable',
			'keyword' => 'nullable',
    ]);
		
		$laporan = new Laporan;
		$pack = [
			'denda' => Config::getValue('DENDA_ALL'),
			'buku' => Buku::all(),
			'filter' => $filter,
			'kecamatan' => Kecamatan::myKecamatan()->get(),
			'data' => collect($laporan->getLaporanTunggakan($filter))->groupBy('kode_op'),
		];
		return view('admin.lap_tunggakan.index', $pack);
	}
	
	public function exportPdf(Request $request)
	{
		$filter = $request->validate([
			'thn_start' => 'required',
			'thn_end' => 'required',
			'buku' => 'nullable',
			'kecamatan' => 'required',
			'kelurahan' => 'nullable',
			'keyword' => 'nullable',
    ]);
		
		$laporan = new Laporan;
		$pack = [
			'pdf' => new \App\Libraries\LetterTemplate\DefaultPdfTemplate(),
			'denda' => Config::getValue('DENDA_ALL'),
			'buku' => Buku::all(),
			'filter' => $filter,
			'kecamatan' => Kecamatan::myKecamatan()->kodeWithSeparatorEqual($filter['kecamatan'])->first(),
			'data' => collect($laporan->getLaporanTunggakan($filter))->groupBy('kode_op'),
		];
		return view('admin.lap_tunggakan.pdf', $pack);
	}
}
