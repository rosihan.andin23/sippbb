<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Njoptkp;

class NjoptkpController extends Controller
{
	
	public function index()
	{
		return view('admin.njoptkp.index');
	}
	
	public function datatable()
	{
		$data = Njoptkp::all()->toArray();
		return response()->json(['data'=>$data]);		
	}
		
}
