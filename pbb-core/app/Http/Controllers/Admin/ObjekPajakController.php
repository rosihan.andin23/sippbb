<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\ObjekPajak;
use App\Kecamatan;
use App\Config;

class ObjekPajakController extends Controller
{
	public function index(Request $request)
	{
		$filter = $request->validate([
			'kecamatan' => 'nullable',
			'kelurahan' => 'nullable',
			'keyword' => 'nullable',
    ]);
		
		$pack = [
			'filter' => $filter,
			'kecamatan' => Kecamatan::myKecamatan()->get(),
			'encodedFilter' => $filter ? encrypt(json_encode($filter)) : false,
		];
		return view('admin.objek_pajak.index', $pack);
	}
	
	public function datatable(Request $request, $encodedFilter)
	{
		$filter = json_decode(decrypt($encodedFilter), true);
		$model = new ObjekPajak();
		$data = $model->getData($filter);
		return response()->json(['data'=>$data]);		
	}
	
	public function detail(Request $request, $id)
	{
		$model = new ObjekPajak();
		$pack = [
			'id' => $id,
			'denda' => Config::getValue('DENDA_ALL'),
			'data' => $model->getDetail($id),
		];
		//dd($pack);
		return view('admin.objek_pajak.detail', $pack);
	}
	


}
