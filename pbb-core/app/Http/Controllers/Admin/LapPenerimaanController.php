<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Buku;
use App\Laporan;

class LapPenerimaanController extends Controller
{
	
	public function index()
	{
		$pack = [
			'buku' => Buku::all(),
		];
		return view('admin.lap_penerimaan.index', $pack);
	}
	
	public function filter(Request $request)
	{
		$filter = $request->validate([
			'tgl_awal' => 'required',
			'tgl_akhir' => 'required',
			'tahun' => 'required',
			'buku' => 'nullable|array',			
    ]);
		
		$laporan = new Laporan;
		$pack = [
			'buku' => Buku::all(),
			'filter' => $filter,
			'data' => collect($laporan->getLaporanPenerimaan($filter))->groupBy('kode_kec'),
		];
		return view('admin.lap_penerimaan.index', $pack);
	}
	
	public function exportPdf(Request $request)
	{
		$filter = $request->validate([
			'tgl_awal' => 'required',
			'tgl_akhir' => 'required',
			'tahun' => 'required',
			'buku' => 'nullable|array',			
    ]);
		
		$laporan = new Laporan;
		$pack = [
			'pdf' => new \App\Libraries\LetterTemplate\DefaultPdfTemplate(),
			'buku' => Buku::all(),
			'filter' => $filter,
			'data' => collect($laporan->getLaporanPenerimaan($filter))->groupBy('kode_kec'),
		];
		return view('admin.lap_penerimaan.pdf', $pack);
	}
}
