<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Tarif;

class TarifController extends Controller
{
	
	public function index()
	{
		return view('admin.tarif.index');
	}
	
	public function datatable()
	{
		$data = Tarif::all()->toArray();
		return response()->json(['data'=>$data]);		
	}
		
}
