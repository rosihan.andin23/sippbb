<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Buku;
use App\Kecamatan;
use App\Laporan;
use App\Config;

class LapHimbauanController extends Controller
{
	
	public function index()
	{
		$pack = [
			'buku' => Buku::all(),
			'kecamatan' => Kecamatan::all(),
		];
		return view('admin.lap_himbauan.index', $pack);
	}
	
	public function filter(Request $request)
	{
		$filter = $request->validate([
			'thn_start' => 'required',
			'thn_end' => 'required',
			'buku' => 'nullable',
			'kecamatan' => 'required',
			'kelurahan' => 'nullable',
			'keyword' => 'nullable',
    ]);
		
		$laporan = new Laporan;
		$pack = [
			'denda' => Config::getValue('DENDA_ALL'),
			'buku' => Buku::all(),
			'filter' => $filter,
			'kecamatan' => Kecamatan::all(),
			'data' => collect($laporan->getLaporanTunggakan($filter))->groupBy('kode_op'),
		];
		return view('admin.lap_himbauan.index', $pack);
	}
	
	public function print(Request $request)
	{
		//dd($request->all());
		$kodeOp = $request->get('check-item');
		$filter = $request->validate([
			'thn_start' => 'required',
			'thn_end' => 'required',
			'buku' => 'nullable',
			'kecamatan' => 'required',
			'kelurahan' => 'nullable',
			'print-tgl' => 'required',
			'print-periode' => 'required',
			'print-due-tagihan' => 'required',
			'print-hal-tagihan' => 'required',
    ]);
		
		$laporanConfigs = Config::getValueByGroup('SURAT');
		$laporan = new Laporan;
		
		$pack = [
			'filter' => $filter,
			'configs' => $laporanConfigs,
			'isPrintHimbauan' => $request->get('print-himbauan', false),
			'isPrintTagihan' => $request->get('print-tagihan', false),
			'kantor' => $laporan->getKantor($laporanConfigs),
			'data' => collect($laporan->getLaporanTunggakan($filter, $kodeOp, $laporanConfigs))->groupBy('kode_op'),
		];
		//dd($pack);
		return view('admin.lap_himbauan.print', $pack);
	}
}
