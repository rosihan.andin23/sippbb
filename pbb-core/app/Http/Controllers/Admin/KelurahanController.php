<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Kelurahan;

class KelurahanController extends Controller
{
	
	public function index()
	{
		return view('admin.kelurahan.index');
	}
	
	public function datatable()
	{
		$data = Kelurahan::all()->toArray();
		return response()->json(['data'=>$data]);		
	}
		
}
