<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class UserStamp extends Model
{
	
	public static function boot() 
	{
		parent::boot();

		static::updating(function($table)  {
			$table->updated_by = Auth::user()->username;
		});
	
		static::creating(function($table)  {
			$table->created_by = Auth::user()->username;
		});
	}

}
