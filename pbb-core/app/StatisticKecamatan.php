<?php

namespace App;

use Illuminate\Support\Facades\DB;

use App\Kecamatan;
use App\StatisticDati2;

class StatisticKecamatan extends StatisticDati2
{
	public $kecamatan = false;
	public $kodeKec = false;
	
	function __construct($kodeKec=false)
	{
		// get kecamatan
		$this->kodeKec = $kodeKec;
		$this->kecamatan = Kecamatan::myKecamatan($this->kodeKec)->first();
		
		parent::__construct();
		if(!$this->kecamatan) return $this;
	}
	
	public function getGroupByTahun()
	{
		$data = [];
		if(!$this->kecamatan) return $data;
		
		for($i=$this->configs['START_TAHUN']; $i<=date('Y'); $i++)
		{
			$pokok = $this->statPokokTahunan
				->where('tahun', $i)
				->where('kd_kecamatan', $this->kecamatan->kd_kecamatan);
				
			$realisasi = $this->statRealisasiTahunan
				->where('tahun', $i)
				->where('kd_kecamatan', $this->kecamatan->kd_kecamatan);
				
			$row = [
				'tahun' => (string)$i,
				'pokok_rp' => $pokok ? $pokok->sum('pokok_pbb') : 0,
				'pokok_sppt' => $pokok ? $pokok->sum('jml_sppt') : 0,
				'realisasi_rp' => $realisasi ? $realisasi->sum('realisasi_pbb') : 0,
				'realisasi_sppt' => $realisasi ? $realisasi->sum('jml_sppt') : 0,
				'sisa_rp' => 0,
				'sisa_sppt' => 0,
			];
			$row['sisa_rp'] = $row['pokok_rp'] - $row['realisasi_rp'];
			$row['sisa_sppt'] = $row['pokok_sppt'] - $row['realisasi_sppt'];
			
			$data[] = $row;
		}
		return $data;
	}
	
	public function getGroupByKelurahan($tahun=false)
	{
		$data = [];
		if(!$this->kecamatan) return $data;
		if(!$tahun) $tahun = date('Y');
		
		foreach($this->kecamatan->kelurahan as $kel)
		{
			$pokok = $this->statPokokTahunan
				->where('tahun', $tahun)
				->where('kd_kecamatan', $this->kecamatan->kd_kecamatan)
				->where('kd_kelurahan', $kel->kd_kelurahan)
				->first();
				
			$realisasi = $this->statRealisasiTahunan
				->where('tahun', $tahun)
				->where('kd_kecamatan', $this->kecamatan->kd_kecamatan)
				->where('kd_kelurahan', $kel->kd_kelurahan)
				->first();
				
			$row = [
				'kode_wilayah' => (string)$kel->kd_kelurahan,
				'nama_wilayah' => $kel->nm_kelurahan,
				'pokok_rp' => $pokok ? $pokok->pokok_pbb : 0,
				'pokok_sppt' => $pokok ? $pokok->jml_sppt : 0,
				'realisasi_rp' => $realisasi ? $realisasi->realisasi_pbb : 0,
				'realisasi_sppt' => $realisasi ? $realisasi->jml_sppt : 0,
				'sisa_rp' => 0,
				'sisa_sppt' => 0,
			];
			$row['sisa_rp'] = $row['pokok_rp'] - $row['realisasi_rp'];
			$row['sisa_sppt'] = $row['pokok_sppt'] - $row['realisasi_sppt'];
			
			$data[] = $row;
		}
		return $data;
	}

	public function getGroupByBuku($tahun=false)
	{
		$data = [];
		if(!$this->kecamatan) return $data;
		if(!$tahun) $tahun = date('Y');
		
		$data = $this->statBukuPokokTahunan
			->where('tahun', $tahun)
			->where('kd_kecamatan', $this->kecamatan->kd_kecamatan)
			->values()
			->all();

		return $data;
	}
	
}
