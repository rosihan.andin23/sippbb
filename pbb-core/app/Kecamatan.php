<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Kecamatan extends Model
{
	protected $connection = 'oracle';

	protected $table = 'ref_kecamatan';
	protected $primaryKey = 'kd_kecamatan';
	protected $keyType = 'string';
	public $incrementing = false;
	
	protected $appends = array('kode');
	
	public function scopeKodeIn($query, $kode=false)
	{
		return $query->whereIn(DB::raw('KD_PROPINSI||KD_DATI2||KD_KECAMATAN'),  $kode);
	}
	
	public function scopeKodeEqual($query, $kode=false)
	{
		return $query->where(DB::raw('KD_PROPINSI||KD_DATI2||KD_KECAMATAN'),  $kode);
	}
	
	public function scopeKodeWithSeparatorEqual($query, $kode=false)
	{
		return $query->where(DB::raw('KD_PROPINSI||\'.\'||KD_DATI2||\'.\'||KD_KECAMATAN'),  $kode);
	}
	
	public function scopeMyKecamatan($query, $kodeKec=false)
	{
		if(Auth::user()->role == 'admin') return $kodeKec ? $query->kodeEqual($kodeKec) : $query;		
		$myKodeKec = Auth::user()->userKecamatan->pluck('kd_kecamatan');
		if($kodeKec) $query->kodeEqual($kodeKec);
		return $query->kodeIn($myKodeKec);
	}
	
	public function scopeIsMyKecamatan($query, $kodeKec)
	{
		if(Auth::user()->role == 'admin') return true;		
		$kodeKec = Auth::user()->userKecamatan->pluck('kd_kecamatan');
		return $kodeKec->contains($kodeKec);
	}
	
	
	
	public function getKodeAttribute($value)
	{
		return $this->kd_propinsi.$this->kd_dati2.$this->kd_kecamatan;
	}
	
	public function getKodeWithSeparatorAttribute($value)
	{
		return $this->kd_propinsi.'.'.$this->kd_dati2.'.'.$this->kd_kecamatan;
	}
	
	
	
	public function kelurahan()
	{
		return $this
			->hasMany('App\Kelurahan', 'kd_kecamatan', 'kd_kecamatan')
			->where('kd_propinsi', $this->kd_propinsi)
			->where('kd_dati2', $this->kd_dati2);
	}
	
}
