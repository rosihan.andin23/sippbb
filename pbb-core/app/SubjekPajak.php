<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Config;

class SubjekPajak extends Model
{
	protected $connection = 'oracle';
	protected $table = 'DAT_SUBJEK_PAJAK';
	protected $primaryKey = 'SUBJEK_PAJAK_ID';
	protected $keyType = 'string';
	
	public $incrementing = false;
	
	public function getDetail($idSubjekPajak)
	{
		$configs = Config::getValueByGroup('ALL');
		$return = (object)[
			'subjekPajak' => false,
			'objekPajak' => false,
			'sppt' => false,
			'tunggakan' => false,
		];
		$param = [
			'idSubjekPajak' => $idSubjekPajak,
		];
		
		$selNop = 'OP.KD_PROPINSI||OP.KD_DATI2||OP.KD_KECAMATAN||OP.KD_KELURAHAN||OP.KD_BLOK||OP.NO_URUT||OP.KD_JNS_OP';
		
		//get detail WP
		$query = "SELECT
			SUBJEK_PAJAK_ID, NM_WP,
			JALAN_WP||' '||BLOK_KAV_NO_WP||' RT.'||RT_WP||' RW.'||RW_WP||' KEL.'||KELURAHAN_WP||' '||KOTA_WP as ALAMAT_WP,
			KD_POS_WP,
			TELP_WP,
			NPWP
		FROM
			DAT_SUBJEK_PAJAK
		WHERE
			TRIM(SUBJEK_PAJAK_ID) = :idSubjekPajak
			AND ROWNUM = 1";
		$return->subjekPajak = DB::connection($this->connection)->select($query, $param);
		if($return->subjekPajak) $return->subjekPajak = $return->subjekPajak[0];
		
		//get data OP
		$query = "SELECT
			$selNop AS NOP,
			KC.NM_KECAMATAN,
			KL.NM_KELURAHAN,
			OP.JALAN_OP||' '||OP.BLOK_KAV_NO_OP||' RT.'||OP.RT_OP||' RW.'||OP.RW_OP AS ALAMAT_OP,
			TOTAL_LUAS_BNG,
			TOTAL_LUAS_BUMI
		FROM
			DAT_OBJEK_PAJAK OP
		JOIN
			REF_KECAMATAN KC ON KC.KD_PROPINSI=OP.KD_PROPINSI AND KC.KD_DATI2=OP.KD_DATI2 AND KC.KD_KECAMATAN=OP.KD_KECAMATAN
		JOIN
			REF_KELURAHAN KL ON KL.KD_PROPINSI=OP.KD_PROPINSI AND KL.KD_DATI2=OP.KD_DATI2 AND KL.KD_KECAMATAN=OP.KD_KECAMATAN AND KL.KD_KELURAHAN=OP.KD_KELURAHAN
		WHERE
			TRIM(SUBJEK_PAJAK_ID) = :idSubjekPajak";
		$return->objekPajak = DB::connection($this->connection)->select($query, $param);
		
		//get data SPPT
		$param['startTahun'] = $configs['START_TAHUN'];
		$param['kdPropinsi'] = $configs['KD_PROPINSI'];
		$param['kdDati2'] = $configs['KD_DATI2'];
		$query = "SELECT
			MAX($selNop) AS NOP,
			MAX(S.KODE_SPPT) AS KODE_SPPT,
			MAX(S.THN_PAJAK_SPPT) AS THN_PAJAK_SPPT,
			MAX(S.PBB_YG_HARUS_DIBAYAR_SPPT) AS PBB_YG_HARUS_DIBAYAR_SPPT,
			MAX(S.TGL_JATUH_TEMPO_SPPT) AS TGL_JATUH_TEMPO_SPPT,
			MAX(S.STATUS_PEMBAYARAN_SPPT) AS STATUS_PEMBAYARAN_SPPT,
			MAX(S.TGL_TERBIT_SPPT) AS TGL_TERBIT_SPPT,
			COUNT(B.KD_TP) AS JML_BAYAR,
			SUM(JML_SPPT_YG_DIBAYAR) AS JML_SPPT_YG_DIBAYAR,
			SUM(DENDA_SPPT) AS DENDA_SPPT_BAYAR,
			MAX(PEMBAYARAN_SPPT_KE) AS PEMBAYARAN_SPPT_KE,
			FLOOR(MONTHS_BETWEEN(SYSDATE, MAX(s.TGL_JATUH_TEMPO_SPPT))) AS BULAN_TELAT,
			0 AS DENDA
		FROM
			DAT_OBJEK_PAJAK OP
		JOIN
			SPPT S ON S.KD_PROPINSI=OP.KD_PROPINSI AND S.KD_DATI2=OP.KD_DATI2 AND S.KD_KECAMATAN=OP.KD_KECAMATAN AND S.KD_KELURAHAN=OP.KD_KELURAHAN AND S.KD_BLOK=OP.KD_BLOK AND S.NO_URUT=OP.NO_URUT AND S.KD_JNS_OP=OP.KD_JNS_OP
		LEFT JOIN
			PEMBAYARAN_SPPT B ON B.KD_PROPINSI=S.KD_PROPINSI AND B.KD_DATI2=S.KD_DATI2 AND B.KD_KECAMATAN=S.KD_KECAMATAN AND B.KD_KELURAHAN=S.KD_KELURAHAN AND B.KD_BLOK=S.KD_BLOK AND B.NO_URUT=S.NO_URUT AND B.KD_JNS_OP=S.KD_JNS_OP AND B.THN_PAJAK_SPPT=S.THN_PAJAK_SPPT
		WHERE
			TRIM(SUBJEK_PAJAK_ID) = :idSubjekPajak
			AND OP.KD_PROPINSI = :kdPropinsi
			AND OP.KD_DATI2 = :kdDati2
			AND S.THN_PAJAK_SPPT >= :startTahun
		GROUP BY
			S.KD_PROPINSI, S.KD_DATI2, S.KD_KECAMATAN, S.KD_KELURAHAN, S.KD_BLOK, S.NO_URUT, S.KD_JNS_OP, S.THN_PAJAK_SPPT
		ORDER BY
			THN_PAJAK_SPPT ASC";
		$return->sppt = collect(DB::connection($this->connection)->select($query, $param))->groupBy('nop');
		
		return $return;
	}
	
	public function getData($filter=false)
	{
		$configs = Config::getValueByGroup('ALL');
		$where = [];
		$param = [
			'kd_propinsi' => $configs['KD_PROPINSI'],
			'kd_dati2' => $configs['KD_DATI2'],
		];
		
		if(Auth::user()->role != 'admin') 
		{
			$kodeKec = Auth::user()->userKecamatan->pluck('kd_kecamatan')->toArray();
			$kodeKec = '\''.implode('\',\'', $kodeKec).'\'';
			$where[] = 'OP.KD_PROPINSI||OP.KD_DATI2||OP.KD_KECAMATAN IN ('.$kodeKec.')';			
		}
		if(isset($filter['keyword']) && $filter['keyword'])
		{
			$param['keyword'] = '%'.strtolower($filter['keyword']).'%';
			$where[] = "(
				LOWER(WP.SUBJEK_PAJAK_ID) LIKE :keyword
				OR LOWER(WP.NM_WP) LIKE :keyword
				OR LOWER(TELP_WP) LIKE :keyword
				OR LOWER(JALAN_WP||' '||BLOK_KAV_NO_WP||' '||KELURAHAN_WP||' '||KOTA_WP) LIKE :keyword
				OR LOWER(OP.JALAN_OP||' '||OP.BLOK_KAV_NO_OP) LIKE :keyword
			)";
		}
		if(isset($filter['kecamatan']) && $filter['kecamatan'])
		{
			$param['kd_kecamatan'] = $filter['kecamatan'];
			$where[] = 'OP.KD_KECAMATAN = :kd_kecamatan';
		}
		
		if(isset($filter['kelurahan']) && $filter['kelurahan'])
		{
			$param['kd_kelurahan'] = $filter['kelurahan'];
			$where[] = 'OP.KD_KELURAHAN = :kd_kelurahan';
		}
		
		$where = $where ? ' AND '.implode(' AND ', $where) : '';
		
		$query = "
		SELECT
			WP.SUBJEK_PAJAK_ID, MAX(NM_WP) AS NM_WP, 
			MAX(JALAN_WP||' '||BLOK_KAV_NO_WP||' RT.'||RT_WP||' RW.'||RW_WP||' KEL.'||KELURAHAN_WP||' '||KOTA_WP) as ALAMAT_WP,
			MAX(OP.JALAN_OP||' '||OP.BLOK_KAV_NO_OP||' RT.'||OP.RT_OP||' RW.'||OP.RW_OP) as ALAMAT_OP,
			MAX(TELP_WP) AS TELP_WP,
			COUNT(OP.SUBJEK_PAJAK_ID) as JML_OP
		FROM
			DAT_SUBJEK_PAJAK WP
		JOIN
			DAT_OBJEK_PAJAK OP ON OP.SUBJEK_PAJAK_ID=WP.SUBJEK_PAJAK_ID
		WHERE
			OP.KD_PROPINSI = :kd_propinsi
			AND OP.KD_DATI2 = :kd_dati2
			$where
		GROUP BY
			WP.SUBJEK_PAJAK_ID
		ORDER BY
			NM_WP ASC";

		$results = DB::connection($this->connection)->select($query, $param);
		return $results;		
	}
}