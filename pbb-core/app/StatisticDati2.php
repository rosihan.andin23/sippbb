<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use App\Config;
use App\Buku;
use App\Kecamatan;

class StatisticDati2
{
	public $statPokokTahunan = [];
	public $statRealisasiTahunan = [];
	public $statBukuPokokTahunan = [];
	public $publishDate = null;
	
	protected $connection = 'oracle';
	protected $configs = false;
	
	protected $cacheStorage = null;
	protected $cacheName = 'statAll';
	protected $cacheStorageName = 'cache_stats';
	protected $isCached = false;
	protected $cachedDate = false;
	protected $cacheExpiredDays = 30; // day
	protected $cachedItems = ['statPokokTahunan', 'statRealisasiTahunan', 'statBukuPokokTahunan'];
	protected $cacheFormat = [
		'dateCreated' => null,
		'dateExpired' => null,
	];
	
	
	function __construct()
	{
		$this->configs = Config::getValueByGroup('ALL');
		$this->cacheStorage = Storage::disk($this->cacheStorageName);
		$this->publishDate = date('Y-m-d');
	}
	
	public function getCollections()
	{
		return $this->loadCache()
			->getPokok()
			->getRealisasi()
			->getSpptBuku()
			->saveCache();		
	}
	
	protected function getPokok()
	{
		if($this->statPokokTahunan) return $this;
		
		$select = "MAX(THN_PAJAK_SPPT) AS TAHUN, 
		MAX(KD_KECAMATAN) AS KD_KECAMATAN, 
		MAX(KD_KELURAHAN) AS KD_KELURAHAN, 
		SUM(PBB_YG_HARUS_DIBAYAR_SPPT) AS POKOK_PBB, 
		COUNT(THN_PAJAK_SPPT) AS JML_SPPT";
		
		$this->statPokokTahunan = DB::connection($this->connection)
			->table('SPPT')
			->select(DB::raw($select))
			->where('KD_PROPINSI', $this->configs['KD_PROPINSI'])
			->where('KD_DATI2', $this->configs['KD_DATI2'])
			->where('THN_PAJAK_SPPT', '>=',$this->configs['START_TAHUN'])
			->groupBy('THN_PAJAK_SPPT', 'KD_KELURAHAN', 'KD_KECAMATAN')
			->get();

		return $this;
	}
	
	protected function getRealisasi()
	{
		if($this->statRealisasiTahunan) return $this;
		
		$select = "MAX(THN_PAJAK_SPPT) AS TAHUN, 
		MAX(KD_KECAMATAN) AS KD_KECAMATAN, 
		MAX(KD_KELURAHAN) AS KD_KELURAHAN, 
		SUM(JML_SPPT_YG_DIBAYAR) AS REALISASI_PBB, 
		SUM(DENDA_SPPT) as DENDA_DIBAYAR,
		COUNT(THN_PAJAK_SPPT) AS JML_SPPT";
		
		$this->statRealisasiTahunan = DB::connection($this->connection)
			->table('PEMBAYARAN_SPPT')
			->select(DB::raw($select))
			->where('KD_PROPINSI', $this->configs['KD_PROPINSI'])
			->where('KD_DATI2', $this->configs['KD_DATI2'])
			->where('THN_PAJAK_SPPT', '>=',$this->configs['START_TAHUN'])
			->groupBy('THN_PAJAK_SPPT', 'KD_KELURAHAN', 'KD_KECAMATAN')
			->get();
			
		return $this;
	}
	
	protected function getSpptBuku()
	{
		if($this->statBukuPokokTahunan) return $this;
		
		$this->statBukuPokokTahunan = collect([]);
		$buku = Buku::all();
		if($buku) foreach($buku as $row)
		{
			$select = "
			'Buku ".$row->kd_buku."' AS KODE_BUKU,
			MAX(KD_KECAMATAN) AS KD_KECAMATAN, 
			MAX(THN_PAJAK_SPPT) AS TAHUN, 
			COUNT(THN_PAJAK_SPPT) AS POKOK_SPPT";
			
			$this->statBukuPokokTahunan = $this->statBukuPokokTahunan->merge(
				DB::connection($this->connection)
				->table('SPPT')
				->select(DB::raw($select))
				->where('KD_PROPINSI', $this->configs['KD_PROPINSI'])
				->where('KD_DATI2', $this->configs['KD_DATI2'])
				->where('THN_PAJAK_SPPT', '>=',$this->configs['START_TAHUN'])
				->whereBetween('THN_PAJAK_SPPT', [$row->thn_awal, $row->thn_akhir])
				->whereBetween('PBB_YG_HARUS_DIBAYAR_SPPT', [$row->nilai_min_buku, $row->nilai_max_buku])
				->groupBy('THN_PAJAK_SPPT', 'KD_KECAMATAN')
				->get()
			);
		}
		return $this;
	}
	
	
	
	protected function loadCache()
	{
		if(!$this->cacheStorage->exists($this->cacheName)) return $this;
		
		$cache = $this->cacheStorage->get($this->cacheName);
		if(!$cache) return $this;
		
		$cache = json_decode($cache);
		
		//if expired
		if(strtotime($cache->dateExpired) < strtotime('now'))
		{
			$this->cacheStorage->delete($this->cacheName);
			return $this;
		}
		
		foreach($this->cachedItems as $item) if(isset($cache->$item)) $this->$item = collect($cache->$item);
		
		$this->isCached = true;
		$this->cachedDate = strtotime($cache->dateCreated);
		$this->publishDate = $cache->dateCreated;
		
		return $this;
	}

	protected function saveCache()
	{
		// no need to create cache if curr data already from cache
		if($this->isCached) return $this;
		
		$cache = $this->cacheFormat;
		$cache['kodeKec'] = $this->kecamatan->kode;
		$cache['dateExpired'] = date('Y-m-d', strtotime('+'.$this->cacheExpiredDays.' days'));
		$cache['dateCreated'] = date('Y-m-d H:i:s');
		
		foreach($this->cachedItems as $item) if(isset($this->$item)) $cache[$item] = $this->$item;
		
		$this->cacheStorage->put($this->cacheName, json_encode($cache));
		return $this;
	}
	
	public function clearCache()
	{
		if($this->cacheStorage->exists($this->cacheName)) $this->cacheStorage->delete($this->cacheName);
		return !$this->cacheStorage->exists($this->cacheName);
	}

	
	
	public function getGroupByTahun()
	{
		$data = [];		
		for($i=$this->configs['START_TAHUN']; $i<=date('Y'); $i++)
		{
			$pokok = $this->statPokokTahunan->where('tahun', $i);
			$realisasi = $this->statRealisasiTahunan->where('tahun', $i);

			$row = [
				'tahun' => (string)$i,
				'pokok_rp' => $pokok ? $pokok->sum('pokok_pbb') : 0,
				'pokok_sppt' => $pokok ? $pokok->sum('jml_sppt') : 0,
				'realisasi_rp' => $realisasi ? $realisasi->sum('realisasi_pbb') : 0,
				'realisasi_sppt' => $realisasi ? $realisasi->sum('jml_sppt') : 0,
				'sisa_rp' => 0,
				'sisa_sppt' => 0,
			];
			$row['sisa_rp'] = $row['pokok_rp'] - $row['realisasi_rp'];
			$row['sisa_sppt'] = $row['pokok_sppt'] - $row['realisasi_sppt'];
			
			$data[] = $row;
		}
		return $data;
	}
	
	public function getGroupByKecamatan($tahun=false)
	{
		$data = [];
		if(!$tahun) $tahun = date('Y');
		$kecamatan = Kecamatan::myKecamatan()->get();
		
		foreach($kecamatan as $kec)
		{
			$pokok = $this->statPokokTahunan
				->where('tahun', $tahun)
				->where('kd_kecamatan', $kec->kd_kecamatan);
				
			$realisasi = $this->statRealisasiTahunan
				->where('tahun', $tahun)
				->where('kd_kecamatan', $kec->kd_kecamatan);
				
			$row = [
				'kode_wilayah' => (string)$kec->kd_kecamatan,
				'nama_wilayah' => $kec->nm_kecamatan,
				'pokok_rp' => $pokok ? $pokok->sum('pokok_pbb') : 0,
				'pokok_sppt' => $pokok ? $pokok->sum('jml_sppt') : 0,
				'realisasi_rp' => $realisasi ? $realisasi->sum('realisasi_pbb') : 0,
				'realisasi_sppt' => $realisasi ? $realisasi->sum('jml_sppt') : 0,
				'sisa_rp' => 0,
				'sisa_sppt' => 0,
			];
			$row['sisa_rp'] = $row['pokok_rp'] - $row['realisasi_rp'];
			$row['sisa_sppt'] = $row['pokok_sppt'] - $row['realisasi_sppt'];
			
			$data[] = $row;
		}
		return $data;
	}

	public function getGroupByBuku($tahun=false)
	{
		$data = [];
		if(!$tahun) $tahun = date('Y');

		$data = $this->statBukuPokokTahunan
			->where('tahun', $tahun);
		
		$dataOrg = [];
		$buku = Buku::all();
		if($buku) foreach($buku as $row)
		{
			$kodeBuku = 'Buku '.$row->kd_buku;
			$dataBuku = $data->where('kode_buku', $kodeBuku);
			$dataOrg[] = [
				'kode_buku' => $kodeBuku,
				'tahun' => $tahun,
				'pokok_sppt' => $dataBuku->sum('pokok_sppt'),
			];
		}
		
		return $dataOrg;
	}
	
	
}
