<?php
namespace App\Helpers;

class NumberHelper
{
	static function numberToCurrId($number)
	{
		return number_format($number,0,',','.');
	}

	static function calcFinePerMonths($total, $percentage, $months)
	{
		if($months <= 0) return 0;
		return ($total * ($percentage/100)) * $months;
	}
	
}