<?php
namespace App\Helpers;

class DateHelper
{
	static $monthId = ['', 'Januari','Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
	
	static function shortIdToLongId($shortId)
	{
		$arr = explode('/', $shortId);
		return $arr[0].' '.static::$monthId[(int)$arr[1]].' '.$arr[2];
	}

	static function shortIdToshortEn($shortId)
	{
		return date('Y-m-d', strtotime(str_replace('/','-',$shortId)));
	}
	
	static function shortEnToLongId($shortEn)
	{
		$shortId = date('d/m/Y', strtotime($shortEn));
		return static::shortIdToLongId($shortId);
	}
	
}