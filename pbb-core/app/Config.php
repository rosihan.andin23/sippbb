<?php

namespace App;

use App\Model\UserStamp;

class Config extends UserStamp
{
	
	protected $table = 'configs';
	protected $primaryKey = 'conf_code';
	protected $keyType = 'string';
	
	public $incrementing = false;
	
	protected $fillable = ['conf_value', 'updated_by'];


	public function scopeGetValue($query, $code, $default='')
	{
		$data = $query->find($code);
		return $data ? $data->conf_value : $default;
	}
	
	public function scopeGetValueByGroup($query, $groupName, $isValueOnly=true)
	{
		$data = $query
			->where('conf_groups', 'like', '%['.$groupName.']%')
			->orWhere('conf_groups', 'like', '%[ALL]%')
			->get()
				->groupBy('conf_code')
				->map(function($item, $key) use ($isValueOnly) {
					return $isValueOnly ? $item[0]->conf_value : $item[0];
				});
		return $data;
	}
	
}
