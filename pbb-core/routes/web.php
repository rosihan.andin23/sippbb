<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AuthController@view')->name('auth');
Route::post('/auth/login', 'AuthController@login')->name('auth.login');
Route::get('/logout', 'AuthController@logout')->name('auth.logout');

Route::get('/oracle', 'TestController@oracle')->name('test.oracle');

Route::middleware('auth:kecamatan,admin')->group(function() 
{
	Route::get('dashboard_kec', 'Kecamatan\DashboardController@view')->name('kecamatan.dashboard');
	Route::get('dashboard_kec/load/{kodeKecamatan?}/{tahun?}', 'Kecamatan\DashboardController@load')->name('kecamatan.dashboard.load');
	Route::get('dashboard_kec/reload', 'Kecamatan\DashboardController@reload')->name('kecamatan.dashboard.reload');
	
	Route::get('profile', 'Admin\UserController@profile')->name('profile');
	Route::post('profile', 'Admin\UserController@profileSubmit')->name('profile.submit');
	
	Route::get('subjek_pajak', 'Admin\SubjekPajakController@index')->name('wp');
	Route::post('subjek_pajak', 'Admin\SubjekPajakController@index')->name('wp.filter');
	Route::get('subjek_pajak/datatable/{encodedFilter}', 'Admin\SubjekPajakController@datatable')->name('wp.datatable');
	Route::get('pencarian', 'Admin\SubjekPajakController@search')->name('pencarian');
	Route::get('subjek_pajak/detail/{id?}', 'Admin\SubjekPajakController@detail')->name('wp.detail');
		
	Route::get('objek_pajak', 'Admin\ObjekPajakController@index')->name('op');
	Route::post('objek_pajak', 'Admin\ObjekPajakController@index')->name('op.filter');
	Route::get('objek_pajak/datatable/{encodedFilter}', 'Admin\ObjekPajakController@datatable')->name('op.datatable');
	Route::get('objek_pajak/detail/{id?}', 'Admin\ObjekPajakController@detail')->name('op.detail');
	
	Route::get('lap_penerimaan', 'Admin\LapPenerimaanController@index')->name('lap.penerimaan');
	Route::post('lap_penerimaan', 'Admin\LapPenerimaanController@filter')->name('lap.penerimaan.filter');
	Route::post('lap_penerimaan/pdf', 'Admin\LapPenerimaanController@exportPdf')->name('lap.penerimaan.pdf');
	
	Route::get('lap_tunggakan', 'Admin\LapTunggakanController@index')->name('lap.tunggakan');
	Route::post('lap_tunggakan', 'Admin\LapTunggakanController@filter')->name('lap.tunggakan.filter');
	Route::post('lap_tunggakan/pdf', 'Admin\LapTunggakanController@exportPdf')->name('lap.tunggakan.pdf');
});

Route::middleware('auth:admin')->group(function() 
{
	Route::get('dashboard_adm/{kodeKecamatan?}', 'Admin\DashboardController@view')->name('dashboard');
	Route::get('dashboard_adm/load/{kodeKecamatan?}/{tahun?}', 'Admin\DashboardController@load')->name('dashboard.load');
	Route::get('dashboard_adm/reload', 'Admin\DashboardController@reload')->name('dashboard.reload');
	
	Route::get('user', 'Admin\UserController@index')->name('user');
	Route::get('user/datatable', 'Admin\UserController@datatable')->name('user.datatable');
	Route::get('user/form/{id?}', 'Admin\UserController@form')->name('user.form');
	Route::post('user/form/{id?}', 'Admin\UserController@store')->name('user.store');
	Route::get('user/activate/{id?}/{state?}', 'Admin\UserController@activate')->name('user.activate');
	Route::get('user/delete/{id?}', 'Admin\UserController@delete')->name('user.delete');
	
	Route::get('buku', 'Admin\BukuController@index')->name('buku');
	Route::get('buku/datatable', 'Admin\BukuController@datatable')->name('buku.datatable');
	
	Route::get('njoptkp', 'Admin\NjoptkpController@index')->name('njoptkp');
	Route::get('njoptkp/datatable', 'Admin\NjoptkpController@datatable')->name('njoptkp.datatable');
	
	Route::get('tarif', 'Admin\TarifController@index')->name('tarif');
	Route::get('tarif/datatable', 'Admin\TarifController@datatable')->name('tarif.datatable');
	
	Route::get('kelurahan', 'Admin\KelurahanController@index')->name('kelurahan');
	Route::get('kelurahan/datatable', 'Admin\KelurahanController@datatable')->name('kelurahan.datatable');
	
	
	Route::get('lap_himbauan', 'Admin\LapHimbauanController@index')->name('lap.himbauan');
	Route::post('lap_himbauan', 'Admin\LapHimbauanController@filter')->name('lap.himbauan.filter');
	Route::post('lap_himbauan/print', 'Admin\LapHimbauanController@print')->name('lap.himbauan.print');
	Route::post('lap_himbauan/download', 'Admin\LapHimbauanController@download')->name('lap.himbauan.download');
	
	
	Route::get('config', 'Admin\ConfigController@index')->name('config');
	Route::get('config/datatable', 'Admin\ConfigController@datatable')->name('config.datatable');
	Route::get('config/form/{id?}', 'Admin\ConfigController@form')->name('config.form');
	Route::post('config/form/{id?}', 'Admin\ConfigController@store')->name('config.store');	
});