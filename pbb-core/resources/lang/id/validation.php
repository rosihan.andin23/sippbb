<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Bagian :attribute harus disetujui.',
    'active_url' => 'Bagian :attribute bukan URL yang valid.',
    'after' => 'Bagian :attribute harus setelah tanggal :date.',
    'after_or_equal' => 'Bagian :attribute harus setelah atau sama dengan tanggal :date.',
    'alpha' => 'Bagian :attribute hanya boleh berisi huruf.',
    'alpha_dash' => 'Bagian :attribute hanya boleh berisi huruf, angka, strip dan garis bawah.',
    'alpha_num' => 'Bagian :attribute hanya boleh berisi huruf dan angka.',
    'array' => 'Bagian :attribute harus berupa array.',
    'before' => 'Bagian :attribute harus sebelum tanggal :date.',
    'before_or_equal' => 'Bagian :attribute harus sebelum atau sama dengan tanggal :date.',
    'between' => [
        'numeric' => 'Bagian :attribute harus diantara :min dan :max.',
        'file' => 'Bagian :attribute harus diantara :min dan :max kilobytes.',
        'string' => 'Bagian :attribute harus diantara :min dan :max characters.',
        'array' => 'Bagian :attribute must have diantara :min dan :max items.',
    ],
    'boolean' => 'Bagian :attribute field harus true atau false.',
    'confirmed' => 'Konfirmasi bagian :attribute tidak cocok.',
    'date' => 'Bagian :attribute bukan tipe tanggal yang benar.',
    'date_equals' => 'Bagian :attribute harus sama dengan tanggal :date.',
    'date_format' => 'Bagian :attribute tidak cocok dengan format :format.',
    'different' => 'Bagian :attribute dan :other harus berbeda.',
    'digits' => 'Bagian :attribute harus :digits digit.',
    'digits_between' => 'Bagian :attribute harus diantara :min dan :max digits.',
    'dimensions' => 'Bagian :attribute tidak sesuai dengan image dimensions yang dijinkan.',
    'distinct' => 'Bagian :attribute memiliki isi yang sama.',
    'email' => 'Bagian :attribute harus berupa alamat email yang benar.',
    'ends_with' => 'Bagian :attribute harus dakhiri dengan: :values.',
    'exists' => ':attribute yang dipilih tidak valid.',
    'file' => 'Bagian :attribute harus berupa file.',
    'filled' => 'Bagian :attribute harus diisi.',
    'gt' => [
        'numeric' => 'Bagian :attribute harus lebih besar dari :value.',
        'file' => 'Bagian :attribute harus lebih besar dari :value kilobytes.',
        'string' => 'Bagian :attribute harus lebih besar dari :value karakter.',
        'array' => 'Bagian :attribute harus lebih besar dari :value items.',
    ],
    'gte' => [
        'numeric' => 'Bagian :attribute harus lebih besar dari atau sama dengan :value.',
        'file' => 'Bagian :attribute harus lebih besar dari atau sama dengan :value kilobytes.',
        'string' => 'Bagian :attribute harus lebih besar dari atau sama dengan :value karakter.',
        'array' => 'Bagian :attribute must have :value items or more.',
    ],
    'image' => 'Bagian :attribute harus berupa gambar.',
    'in' => ':attribute yang dipilih tidak valid.',
    'in_array' => 'Bagian :attribute tidak ada di :other.',
    'integer' => 'Bagian :attribute harus berupa bilangan.',
    'ip' => 'Bagian :attribute harus berupa IP address.',
    'ipv4' => 'Bagian :attribute harus berupa IPv4 address.',
    'ipv6' => 'Bagian :attribute harus berupa IPv6 address.',
    'json' => 'Bagian :attribute harus berupa JSON string.',
    'lt' => [
        'numeric' => 'Bagian :attribute harus kurang dari :value.',
        'file' => 'Bagian :attribute harus kurang dari :value kilobytes.',
        'string' => 'Bagian :attribute harus kurang dari :value karakter.',
        'array' => 'Bagian :attribute harus kurang dari :value items.',
    ],
    'lte' => [
        'numeric' => 'Bagian :attribute harus kurang dari atau sama dengan :value.',
        'file' => 'Bagian :attribute harus kurang dari atau sama dengan :value kilobytes.',
        'string' => 'Bagian :attribute harus kurang dari atau sama dengan :value karakter.',
        'array' => 'Bagian :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'Bagian :attribute tidak boleh lebih besar dari :max.',
        'file' => 'Bagian :attribute tidak boleh lebih besar dari :max kilobytes.',
        'string' => 'Bagian :attribute tidak boleh lebih besar dari :max characters.',
        'array' => 'Bagian :attribute may not have more than :max items.',
    ],
    'mimes' => 'Bagian :attribute harus berupa file dengan tipe: :values.',
    'mimetypes' => 'Bagian :attribute harus berupa file dengan tipe: :values.',
    'min' => [
        'numeric' => 'Bagian :attribute minimal :min.',
        'file' => 'Bagian :attribute minimal :min kilobytes.',
        'string' => 'Bagian :attribute minimal :min karakter.',
        'array' => 'Bagian :attribute must have at least :min items.',
    ],
    'not_in' => ':attribute yang dipilih tidak valid.',
    'not_regex' => 'Format bagian :attribute tidak valid.',
    'numeric' => 'Bagian :attribute harus angka.',
    'password' => 'Password tidak benar.',
    'present' => 'Bagian :attribute harus ada.',
    'regex' => 'Format bagian :attribute tidak valid.',
    'required' => 'Bagian :attribute harus diisi.',
    'required_if' => 'Bagian :attribute harus diisi jika :other berisi :value.',
    'required_unless' => 'Bagian :attribute harus diisi jika :other tidak berisi :values.',
    'required_with' => 'Bagian :attribute harus diisi jika :values ada.',
    'required_with_all' => 'Bagian :attribute harus diisi jika :values ada.',
    'required_without' => 'Bagian :attribute harus diisi jika :values tidak ada.',
    'required_without_all' => 'Bagian :attribute Harus diisi jika :values kosong.',
    'same' => 'Bagian :attribute dan :other harus sama persis.',
    'size' => [
        'numeric' => 'Bagian :attribute harus :size.',
        'file' => 'Bagian :attribute harus :size kilobytes.',
        'string' => 'Bagian :attribute harus :size characters.',
        'array' => 'Bagian :attribute must contain :size items.',
    ],
    'starts_with' => 'Bagian :attribute harus diawali dengan: :values.',
    'string' => 'Bagian :attribute harus berupa string.',
    'timezone' => 'Bagian :attribute harus berisi zona yang valid.',
    'unique' => 'Bagian :attribute sudah terpakai, harap pilih lainnya.',
    'uploaded' => 'Bagian :attribute gagal diupload.',
    'url' => 'Format bagian :attribute tidak valid.',
    'uuid' => 'Bagian :attribute harus berupa UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
