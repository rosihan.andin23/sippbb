@extends('template.template')

@section('sub-header')
<div class="kt-subheader__main">
	<h3 class="kt-subheader__title">Dasbor</h3>
	<span class="kt-subheader__separator kt-subheader__separator--v"></span>
	<span class="kt-subheader__desc">Ringkasan dan Statistik</span>
</div>
<div class="kt-subheader__toolbar">
	<div class="kt-subheader__wrapper">
		<a href="#" class="btn kt-subheader__btn-daterange kt-subheader__btn-secondary"><i class="flaticon-calendar"></i> Tgl Terbit : {{ date('d/m/Y') }}</a>
	</div>
</div>
@endsection

@section('content')
<div id="pbb-dasbor">

<!--begin:: Widgets/Stats-->
<div class="kt-portlet">
	<div class="kt-portlet__body  kt-portlet__body--fit">
		<div class="row row-no-padding row-col-separator-xl">
			
			<div class="col-md-12 col-lg-6 col-xl-3">
				<div class="kt-widget24">
					<div class="kt-widget24__details">
						<div class="kt-widget24__info">
							<h4 class="kt-widget24__title">
								Realisasi
							</h4>
							<span class="kt-widget24__desc">
								Tahun 2018
							</span>
						</div>
						<span class="kt-widget24__stats kt-font-success">
							748 Jt
						</span>
					</div>
					<div class="progress progress--sm">
						<div class="progress-bar kt-bg-success" role="progressbar" style="width: 90%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
					<div class="kt-widget24__action">
						<span class="kt-widget24__change">
							33.389 SPT
						</span>
						<span class="kt-widget24__number">
							90%
						</span>
					</div>
				</div>
			</div>
			
			<div class="col-md-12 col-lg-6 col-xl-3">
				<div class="kt-widget24">
					<div class="kt-widget24__details">
						<div class="kt-widget24__info">
							<h4 class="kt-widget24__title">
								Pokok Ketetapan
							</h4>
							<span class="kt-widget24__desc">
								Tahun 2019
							</span>
						</div>
						<span class="kt-widget24__stats kt-font-brand">
							932 Jt
						</span>
					</div>
					<div class="progress progress--sm">
						<div class="progress-bar kt-bg-brand" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
					<div class="kt-widget24__action">
						<span class="kt-widget24__change">
							35.346 SPT
						</span>
						<span class="kt-widget24__number">
							100%
						</span>
					</div>
				</div>
			</div>
			
			<div class="col-md-12 col-lg-6 col-xl-3">
				<div class="kt-widget24">
					<div class="kt-widget24__details">
						<div class="kt-widget24__info">
							<h4 class="kt-widget24__title">
								Realisasi 
							</h4>
							<span class="kt-widget24__desc">
								Tahun 2019
							</span>
						</div>
						<span class="kt-widget24__stats kt-font-warning">
							883 Jt
						</span>
					</div>
					<div class="progress progress--sm">
						<div class="progress-bar kt-bg-warning" role="progressbar" style="width: 95%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
					<div class="kt-widget24__action">
						<span class="kt-widget24__change">
							33.389 SPT
						</span>
						<span class="kt-widget24__number">
							95%
						</span>
					</div>
				</div>
			</div>
			
			<div class="col-md-12 col-lg-6 col-xl-3">
				<div class="kt-widget24">
					<div class="kt-widget24__details">
						<div class="kt-widget24__info">
							<h4 class="kt-widget24__title">
								Sisa
							</h4>
							<span class="kt-widget24__desc">
								Tahun 2019
							</span>
						</div>
						<span class="kt-widget24__stats kt-font-danger">
							49 Jt
						</span>
					</div>
					<div class="progress progress--sm">
						<div class="progress-bar kt-bg-danger" role="progressbar" style="width: 5%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
					<div class="kt-widget24__action">
						<span class="kt-widget24__change">
							1.957 SPT
						</span>
						<span class="kt-widget24__number">
							5%
						</span>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
<!--end:: Widgets/Stats-->

<div class="row">
	<!--begin:: Chart PBB Tahunan-->
	<div class="col-md-8">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head kt-portlet__head--noborder">
				<div class="kt-portlet__head-label"><h3 class="kt-portlet__head-title">Grafik PBB Tahunan</h3></div>
			</div>
			<div id="chart-periode" style="min-height:400px;"></div>
		</div>
	</div>
	<!--end:: Chart PBB Tahunan-->
	
	<!--begin:: Pie SPT per Buku-->
	<div class="col-md-4">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head kt-portlet__head--noborder">
				<div class="kt-portlet__head-label"><h3 class="kt-portlet__head-title">Grafik SPT Pokok per Buku Tahun 2019</h3></div>
			</div>
			<div class="p-2">
				<div id="pie-buku" style="min-height:400px;"></div>
			</div>
		</div>
	</div>
	<!--end:: Pie SPT per Buku-->
</div>


<div class="row">
	
</div>


<div class="row">
	<!--begin:: Pie PBB Kecamatan-->
	<div class="col-md-6">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head kt-portlet__head--noborder">
				<div class="kt-portlet__head-label"><h3 class="kt-portlet__head-title">Persentase PBB per Kecamatan 2019</h3></div>
			</div>
			<div class="pb-2">
				<div id="pie-kecamatan" style="min-height:600px;"></div>
			</div>
		</div>
	</div>
	<!--end:: Pie PBB Kecamatan-->
	
	<!--begin:: Chart PBB Kecamatan-->
	<div class="col-md-6">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head kt-portlet__head--noborder">
				<div class="kt-portlet__head-label"><h3 class="kt-portlet__head-title">Grafik PBB per Kecamatan 2019</h3></div>
			</div>
			<div class="pb-2">
				<div id="chart-kecamatan" style="min-height:900px;"></div>
			</div>
		</div>
	</div>
	<!--end:: Chart PBB Kecamatan-->
</div>

</div>
@endsection


@section('js-plugin')
<script type="text/javascript" src="{{ asset('plugins/amcharts4/core.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/amcharts4/charts.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/amcharts4/themes/animated.js') }}"></script>
<!--script type="text/javascript" src="{{ asset('plugins/amcharts4/themes/material.js') }}"></script-->
<script type="text/javascript" src="{{ asset('plugins/amcharts4/lang/id_ID.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app/dasbor.js') }}"></script>
@endsection

@section('js-script')
<script>
	$('#pbb-dasbor').pbbDasbor({
		idChartPeriode: 'chart-periode',
		idChartPieBuku: 'pie-buku',
		idChartKecamatan: 'chart-kecamatan',
		idChartPieKecamatan: 'pie-kecamatan',
	})
</script>
@endsection