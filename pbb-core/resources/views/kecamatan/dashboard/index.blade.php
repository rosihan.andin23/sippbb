@extends('template.template')

@section('title', 'Dasbor')
@section('subheader-title', 'Dasbor')
@section('subheader-subtitle', 'Ringkasan dan Statistik')
@section('subheader-toolbar')
	<div class="dropdown d-inline-block">
		<button class="btn kt-subheader__btn-daterange kt-subheader__btn-secondary dropdown-toggle" type="button" id="dropdownKecamatanButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="flaticon-placeholder"></i> Kecamatan :&nbsp;<span data="kecamatan-name"></span>
		</button>
		<div class="dropdown-menu" aria-labelledby="dropdownKecamatanButton">
			@foreach($listKecamatan as $kec)
			<a class="dropdown-item py-1" href="javascript:;" data-toggle="kt-tooltip" data-kecamatan-toggle="{{$kec->kode}}" title="Tampikan data Kec {{$kec->nm_kecamatan}}" data-placement="right" data-skin="dark" data-container="body">{{$kec->nm_kecamatan}}</a>
			@endforeach
		</div>
	</div>
	
	<div class="dropdown d-inline-block">
		<button class="btn kt-subheader__btn-daterange kt-subheader__btn-secondary dropdown-toggle" type="button" id="dropdownTahunButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="flaticon-calendar"></i> Tahun :&nbsp;<span data="curr-year"></span>
		</button>
		<div class="dropdown-menu" aria-labelledby="dropdownTahunButton">
			@for($i = $minTahun; $i<=date('Y'); $i++)
			<a class="dropdown-item py-1" href="javascript:;" data-toggle="kt-tooltip" data-tahun-toggle="{{$i}}" title="Tampikan data tahun {{$i}}" data-placement="right" data-skin="dark" data-container="body">{{$i}}</a>
			@endfor
		</div>
	</div>
	
	<a href="javascript:;" class="btn kt-subheader__btn-daterange kt-subheader__btn-secondary">Data Tgl :&nbsp;<span data="publish-date"></span></a>
	
	<a href="{{route('kecamatan.dashboard.reload')}}" class="btn kt-subheader__btn-primary" data-toggle="link-confirm" data-message-text="Anda akan mengupdate data grafik ke yang terbaru, proses ini memakan waktu kurang lebih 3 menit">Update Data</a>
@endsection

@section('content')
<div id="pbb-dasbor">

<!--begin:: Widgets/Stats-->
<div class="kt-portlet">
	<div class="kt-portlet__body  kt-portlet__body--fit">
		<div class="row row-no-padding row-col-separator-xl" id="summary-periode">
			
			<div class="col-md-12 col-lg-3 col-xl-3">
				<div class="kt-widget24" data-summary-year="prev">
					<div class="kt-widget24__details">
						<div class="kt-widget24__info">
							<h4 class="kt-widget24__title">
								Realisasi
							</h4>
							<span class="kt-widget24__desc">
								Tahun <span data="prev-year"></span>
							</span>
						</div>
						<span class="kt-widget24__stats kt-font-success">
							<span data-summary-field="realisasi_rp" data-summary-format="suffix"></span>
						</span>
					</div>
					<div class="progress progress--sm">
						<div class="progress-bar kt-bg-success" role="progressbar" data-summary-percent="realisasi_rp" style="width: 0%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
					<div class="kt-widget24__action">
						<span class="kt-widget24__change">
							<span data-summary-field="realisasi_sppt" data-summary-format="number"></span>&nbsp;SPPT
						</span>
						<span class="kt-widget24__number">
							<span data-summary-percent="realisasi_rp"></span>%
						</span>
					</div>
				</div>
			</div>
			
			<div class="col-md-12 col-lg-3 col-xl-3">
				<div class="kt-widget24" data-summary-year="curr">
					<div class="kt-widget24__details">
						<div class="kt-widget24__info">
							<h4 class="kt-widget24__title">
								Pokok Ketetapan
							</h4>
							<span class="kt-widget24__desc">
								Tahun <span data="curr-year"></span>
							</span>
						</div>
						<span class="kt-widget24__stats kt-font-brand">
							<span data-summary-field="pokok_rp" data-summary-format="suffix"></span>
						</span>
					</div>
					<div class="progress progress--sm">
						<div class="progress-bar kt-bg-brand" role="progressbar" data-summary-percent="pokok_rp" style="width: 0%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
					<div class="kt-widget24__action">
						<span class="kt-widget24__change">
							<span data-summary-field="pokok_sppt" data-summary-format="number"></span>&nbsp;SPPT
						</span>
						<span class="kt-widget24__number">
							<span data-summary-percent="pokok_rp"></span>%
						</span>
					</div>
				</div>
			</div>
			
			<div class="col-md-12 col-lg-3 col-xl-3">
				<div class="kt-widget24" data-summary-year="curr">
					<div class="kt-widget24__details">
						<div class="kt-widget24__info">
							<h4 class="kt-widget24__title">
								Realisasi 
							</h4>
							<span class="kt-widget24__desc">
								Tahun <span data="curr-year"></span>
							</span>
						</div>
						<span class="kt-widget24__stats kt-font-warning">
							<span data-summary-field="realisasi_rp" data-summary-format="suffix"></span>
						</span>
					</div>
					<div class="progress progress--sm">
						<div class="progress-bar kt-bg-warning" role="progressbar" data-summary-percent="realisasi_rp" style="width: 0%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
					<div class="kt-widget24__action">
						<span class="kt-widget24__change">
							<span data-summary-field="realisasi_sppt" data-summary-format="number"></span>&nbsp;SPPT
						</span>
						<span class="kt-widget24__number">
							<span data-summary-percent="realisasi_rp"></span>%
						</span>
					</div>
				</div>
			</div>
			
			<div class="col-md-12 col-lg-3 col-xl-3">
				<div class="kt-widget24" data-summary-year="curr">
					<div class="kt-widget24__details">
						<div class="kt-widget24__info">
							<h4 class="kt-widget24__title">
								Sisa
							</h4>
							<span class="kt-widget24__desc">
								Tahun <span data="curr-year"></span>
							</span>
						</div>
						<span class="kt-widget24__stats kt-font-danger">
							<span data-summary-field="sisa_rp" data-summary-format="suffix"></span>
						</span>
					</div>
					<div class="progress progress--sm">
						<div class="progress-bar kt-bg-danger" role="progressbar" data-summary-percent="sisa_rp" style="width: 0%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
					<div class="kt-widget24__action">
						<span class="kt-widget24__change">
							<span data-summary-field="sisa_sppt" data-summary-format="number"></span>&nbsp;SPPT
						</span>
						<span class="kt-widget24__number">
							<span data-summary-percent="sisa_rp"></span>%
						</span>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
<!--end:: Widgets/Stats-->

<div class="row">
	<!--begin:: Chart PBB Tahunan-->
	<div class="col-md-8">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head kt-portlet__head--noborder">
				<div class="kt-portlet__head-label"><h3 class="kt-portlet__head-title">Grafik PBB Tahunan</h3></div>
			</div>
			<div id="chart-periode" style="min-height:400px;"></div>
		</div>
	</div>
	<!--end:: Chart PBB Tahunan-->
	
	<!--begin:: Pie SPT per Buku-->
	<div class="col-md-4">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head kt-portlet__head--noborder">
				<div class="kt-portlet__head-label"><h3 class="kt-portlet__head-title">Grafik Pokok SPPT per Buku <span data="curr-year"></span></h3></div>
			</div>
			<div class="p-2">
				<div id="pie-buku" data-series="pokok_sppt" style="min-height:400px;"></div>
			</div>
		</div>
	</div>
	<!--end:: Pie SPT per Buku-->
</div>


<div class="row">
	
</div>


<div class="row">
	<!--begin:: Pie PBB Kecamatan-->
	<div class="col-md-6">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head kt-portlet__head--noborder">
				<div class="kt-portlet__head-label"><h3 class="kt-portlet__head-title">Persentase SPPT per Kelurahan <span data="curr-year"></span></h3></div>
			</div>
			<div class="p-2">
				<div class="text-right" data-toggle="change-series" data-chart-target="pieWilayah" data-dom-target="#pie-kelurahan">
					<button type="button" data-series="pokok_sppt" class="btn btn-outline-brand btn-sm active">Pokok</button>
					<button type="button" data-series="realisasi_sppt" class="btn btn-outline-brand btn-sm">Realisasi</button>
					<button type="button" data-series="sisa_sppt" class="btn btn-outline-brand btn-sm">Sisa</button>
				</div>
				<div id="pie-kelurahan" data-series="pokok_sppt" style="min-height:600px;"></div>
			</div>
		</div>
	</div>
	<!--end:: Pie PBB Kecamatan-->
	
	<!--begin:: Chart PBB Kecamatan-->
	<div class="col-md-6">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head kt-portlet__head--noborder">
				<div class="kt-portlet__head-label"><h3 class="kt-portlet__head-title">Grafik PBB per Kelurahan <span data="curr-year"></span></h3></div>
			</div>
			<div class="pb-2">
				<div class="text-right px-2" data-toggle="change-series" data-chart-target="wilayah" data-dom-target="#chart-kelurahan">
					<button type="button" data-series="pokok_rp" class="btn btn-outline-brand btn-sm active">Pokok</button>
					<button type="button" data-series="realisasi_rp" class="btn btn-outline-brand btn-sm">Realisasi</button>
					<button type="button" data-series="sisa_rp" class="btn btn-outline-brand btn-sm">Sisa</button>
				</div>
				<div id="chart-kelurahan" data-series="pokok_rp" style="min-height:600px;"></div>
			</div>
		</div>
	</div>
	<!--end:: Chart PBB Kecamatan-->
</div>

</div>
@endsection


@push('js-plugin')
<script type="text/javascript" src="{{ asset('plugins/amcharts4/core.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/amcharts4/charts.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/amcharts4/themes/animated.js') }}"></script>
<!--script type="text/javascript" src="{{ asset('plugins/amcharts4/themes/material.js') }}"></script-->
<script type="text/javascript" src="{{ asset('plugins/amcharts4/lang/id_ID.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app/dasbor.js') }}"></script>
@endpush

@push('js-script')
<script>
	var stat = $('body').pbbDasbor({
		url: '{{route('kecamatan.dashboard.load')}}',
		tahun: '{{$tahun}}',
		kodeKecamatan: '{{$kodeKecamatan ?? 0}}',
		
		loaderLocation: '#kt_wrapper',
		idChartPeriode: 'chart-periode',
		idChartPieBuku: 'pie-buku',
		idChartWilayah: 'chart-kelurahan',
		idChartPieWilayah: 'pie-kelurahan',
		idSummary: 'summary-periode',
	})
</script>
@endpush