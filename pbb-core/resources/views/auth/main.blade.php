<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Penagihan PBB-P2 | Kab Ngawi</title>
	<meta name="description" content="Login Page">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Montserrat:300,400,500,600,700"]
			},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<link href="{{ asset('plugins/line-awesome/css/line-awesome.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('plugins/flaticon/flaticon.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('plugins/flaticon2/flaticon.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('plugins/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('css/login.css') }}" rel="stylesheet" type="text/css" />
	
	<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

	<!-- begin:: Page -->
	<div class="kt-grid kt-grid--ver kt-grid--root">
		<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile kt-login-wrapper">
				
				<div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
					<div class="kt-login__logo mb-5">
						<a href="#">
							<img src="{{ asset('img/logo.png') }}" height="80" class="float-left mr-3">
							<h2 class="ft-thin pt-2">Kabupaten Ngawi</h2>
							<h2 class="ft-xbold">Badan Keuangan</h2>
						</a>
					</div>
					
					<div class="login-container">
						<h2 class="ft-xbold">Selamat Datang</h2>
						
						<form class="kt-form mt-4 mb-4" method="POST" action="{{route('auth.login')}}">
							@csrf
							
							@if($errors->any())
								<div class="alert alert-danger">
									{{ implode('<br>', $errors->all()) }}
								</div>
							@endif
							<div class="form-group">
								<label>Username</label>
								<input class="form-control" type="text" placeholder="" name="username" autocomplete="off" value="{{ old('username') }}" required>
							</div>
							<div class="form-group mb-3">
								<label>Password</label>
								<input class="form-control form-control-last" type="password" placeholder="" name="password" required>
							</div>
							<div class="kt-login__extra mb-4">
								<label class="kt-checkbox ft-normal">
									<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Ingat Saya
									<span></span>
								</label>
								<a href="javascript:;" id="kt_login_forgot" class="ft-normal ft-bold float-right">Lupa Password ?</a>
							</div>
							<div class="kt-login__actions">
								<button id="kt_login_signin_submit" class="btn btn-brand btn-pill btn-elevate btn-block mt-2 ft-normal ft-bold">Login</button>
							</div>
						</form>
						<h4 class="pt-3">Helpdesk Ngawi | <b>EASN Ngawi</b></h4>
						<h6>Copyright &copy;{{date('Y')}} | <b>BK - PBB P2 Kabupaten Ngawi</b></h6>
					</div>
				</div>
				
					
				<div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content">
					<div class="kt-login__section pl-3">
						<div class="kt-login__block">
							<div class="kt-login__logo clearfix pb-5">
								<a href="#">
									<img src="{{ asset('img/logo.png') }}" height="80" class="float-left mr-3">
									<h2 class="ft-thin pt-2">Kabupaten Ngawi</h2>
									<h2 class="ft-xbold">Badan Keuangan</h2>
								</a>
							</div>
							<h3 class="kt-login__title ft-thin sticky">e-Government</h3>
							<h3 class="kt-login__title ft-xbold mb-0 pb-0">Penagihan PBB-P2</h3>
							<h3 class="kt-login__title">Kabupaten Ngawi</h3>
							<h4 class="kt-login__subtitle">Sekilas</h4>
							<div class="kt-login__desc">Sistem Informasi PBB-P2 adalah media olah data dalam melakukan proses penagihan Pajak Bumi &amp; Bangunan lingkup Kabupaten Ngawi.</div>
							<h3 class="kt-login__subtitle mt-3">Fitur</h3>
							<div class="kt-login__desc">
								<ul>
									<li>Aplikasi Penagihan Bidang Pendapatan</li>
									<li>Realisasi Penerimaan Tahun Berjalan</li>
									<li>Realisasi Tunggakan</li>
									<li>Penerbitan Himbauan</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<script src="{{ asset('plugins/jquery/dist/jquery.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/block-ui/jquery.blockUI.js') }}" type="text/javascript"></script>
</body>
</html>