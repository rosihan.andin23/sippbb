<?php 

	// var
	$defCellHeight = 5.5;
	$defFontSize = 12;
	$tahun = $filter['tahun'];
	
	// header
	$pdf->AliasNbPages();
	$pdf->AddPage('P','A4');
	$pdf->SetMargins(3, 5);
	$pdf->SetFont('Times','B', 16);
	$pdf->Cell(0, $defCellHeight, 'LAPORAN PENERIMAAN PBB', 0, 1, 'C');
	$pdf->SetFont('Times','B', 12);
	$pdf->Cell(0, $defCellHeight, 'PERIODE '.$tahun, 0, 1, 'C');
	$pdf->SetFont('Times','', 10);
	$pdf->Cell(0, $defCellHeight, 'BUKU '.implode(', ',$filter['buku']), 0, 1, 'C');
	$pdf->Ln();
	

	// table header
	$pdf->SetFont('Times', 'B', 8);
	$pdf->SetColsWidth([40, 31, 66, 66]);
	$pdf->SetColsAlign(['', 'C', 'C', 'C']);
	$tableHeader = ['Kecamatan / Kelurahan', 'Pokok Ketetapan '.$tahun, 'Realisasi Pokok Ketetapan'.$tahun, 'Sisa Pokok Ketetapan'.$tahun];
	$pdf->Row($tableHeader, 3.5);
	
	$pdf->SetColsWidth([40, 11,20, 11,19,17,19, 11,19,17,19]);
	$pdf->SetColsAlign(['', 'R','R', 'R','R','R','R', 'R','R','R','R']);
	/* $tableHeader = ['', 
		'SPPT', 'Jumlah (Rp)',
		'SPPT', 'Pokok PBB (Rp)', 'Denda (Rp)', 'Total (Rp)',
		'SPPT', 'Pokok PBB (Rp)', 'Denda (Rp)', 'Total (Rp)',
	]; 
	$pdf->Row($tableHeader, 3.5);
	*/
	
	foreach($data as $rowKec)
	{
		$pdf->SetFont('Times', 'B');
		$kec = $rowKec->first();
		$pdf->Row([
			'KEC. '.$kec->nm_kecamatan, 
			'SPPT', 'Jumlah (Rp)',
			'SPPT', 'Pokok PBB (Rp)', 'Denda (Rp)', 'Total (Rp)',
			'SPPT', 'Pokok PBB (Rp)', 'Denda (Rp)', 'Total (Rp)',
		]);
		
		/* $pdf->Row([
			'KEC. '.$kec->nm_kecamatan,
			NumberHelper::numberToCurrId($rowKec->sum('sppt_tagih')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_tagih')),

			NumberHelper::numberToCurrId($rowKec->sum('sppt_bayar')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_bayar')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_denda_bayar')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_bayar') + $rowKec->sum('tot_denda_bayar')),
			
			NumberHelper::numberToCurrId($rowKec->sum('sppt_tagih') - $rowKec->sum('sppt_bayar')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_tagih') - $rowKec->sum('tot_bayar')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_denda_tagih')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_tagih') - $rowKec->sum('tot_bayar') + $rowKec->sum('tot_denda_tagih')),
		]); */
		
		

		$pdf->SetFont('Times', '');
		foreach($rowKec as $row)
		{
			$pdf->Row([
				$row->kode_kel.' '.$row->nm_kelurahan,
				NumberHelper::numberToCurrId($row->sppt_tagih),
				NumberHelper::numberToCurrId($row->tot_tagih),
				
				NumberHelper::numberToCurrId($row->sppt_bayar),
				NumberHelper::numberToCurrId($row->tot_bayar),
				NumberHelper::numberToCurrId($row->tot_denda_bayar),
				NumberHelper::numberToCurrId($row->tot_bayar + $row->tot_denda_bayar),
				
				NumberHelper::numberToCurrId($row->sppt_tagih - $row->sppt_bayar),
				NumberHelper::numberToCurrId($row->tot_tagih - $row->tot_bayar),
				NumberHelper::numberToCurrId($row->tot_denda_tagih),
				NumberHelper::numberToCurrId($row->tot_tagih - $row->tot_bayar + $row->tot_denda_tagih),
			]);
		}
		
		$pdf->SetFont('Times', 'B');
		$pdf->Row([
			'Total',
			NumberHelper::numberToCurrId($rowKec->sum('sppt_tagih')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_tagih')),
			
			NumberHelper::numberToCurrId($rowKec->sum('sppt_bayar')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_bayar')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_denda_bayar')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_bayar') + $rowKec->sum('tot_denda_bayar')),
			
			NumberHelper::numberToCurrId($rowKec->sum('sppt_tagih') - $rowKec->sum('sppt_bayar')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_tagih') - $rowKec->sum('tot_bayar')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_denda_tagih')),
			NumberHelper::numberToCurrId($rowKec->sum('tot_tagih') - $rowKec->sum('tot_bayar') + $rowKec->sum('tot_denda_tagih')),
		]);
		$pdf->Ln();
	}
	
	
	$pdf->Output('I', 'Laporan Penerimaan PBB Periode '.$tahun.'.pdf');
	exit;