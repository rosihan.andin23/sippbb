@extends('template.template')

@section('title', 'Realisasi Penerimaan')
@section('subheader-title', 'Laporan')
@section('subheader-subtitle', 'Realisasi Penerimaan')

@section('content')

	@component('template.component.portlet')
		@slot('icon', 'kt-font-brand fa fa-copy ')
		@slot('title', 'Pilih Periode Laporan')
		
		@slot('toolbar')
		@endslot
		
		<form method="POST" name="form-filter" action="">
			@csrf
			<div class="row kt-margin-b-20">
				<div class="col-lg-3 kt-margin-b-15-tablet-and-mobile">
					<label>Tanggal Bayar:</label>
					<div class="input-daterange input-group input-group-sm" id="kt_datepicker">
						<input type="text" class="form-control kt-input" name="tgl_awal" placeholder="Mulai tanggal" data-col-index="1" required value="{{$filter['tgl_awal'] ?? date('01/01/Y')}}"/>
						<div class="input-group-append">
							<span class="input-group-text">s/d</span>
						</div>
						<input type="text" class="form-control kt-input" name="tgl_akhir" placeholder="Sampai tanggal" data-col-index="1" required value="{{$filter['tgl_akhir'] ?? date('d/m/Y')}}"/>
					</div>
				</div>
				<div class="col-lg-1 col-6 kt-margin-b-15-tablet-and-mobile">
					<label>Tahun:</label>
					<input name="tahun" type="number" min="1990" max="{{date('Y')}}" class="form-control form-control-sm kt-input" data-col-index="2" required value="{{$filter['tahun'] ?? date('Y')}}">
				</div>
				<div class="col-lg-3 col-6 kt-margin-b-15-tablet-and-mobile">
					<label>Buku:</label>
					<select class="form-control form-control-sm apply-select2" name="buku[]" data-col-index="3" multiple  required>
						@foreach($buku as $row)
							<option value="{{$row->kd_buku}}" {{!isset($filter) || in_array($row->kd_buku, $filter['buku']) ? 'selected' : '' }} >{{$row->kd_buku}}</option>
						@endforeach
					</select>
				</div>
				<div class="col-lg-5 col-6 kt-margin-b-15-tablet-and-mobile pt-3">
					<button class="btn btn-brand btn-sm btn-brand--icon mt-3" id="kt_search" data-url="{{route('lap.penerimaan.filter')}}"><i class="la la-search"></i> Tampilkan</button>
					
					<button class="btn btn-success btn-sm btn-success--icon mt-3" id="kt_download" data-url="{{route('lap.penerimaan.pdf')}}" data-target="_blank"><i class="la la-download"></i> Download PDF</button>
				</div>
			</div>
			
			
		</form>
		
		<hr>
		@if(isset($data))
		<table class="table table-striped table-bordered table-hover table-checkable font-courier" id="table-main" data-url="{{route('kelurahan.datatable')}}">
			<thead>
				<tr>
					<th>Kecamatan / Kelurahan</th>
					<th colspan="2" class="text-center">Pokok Ketetapan<br>Tahun {{$filter['tahun']}}</th>
					<th colspan="4" class="text-center">Realisasi Pokok Ketetapan<br>Tahun {{$filter['tahun']}}</th>
					<th colspan="4" class="text-center">Sisa Pokok Ketetapan<br>Tahun {{$filter['tahun']}}</th>
				</tr>
				<tr>
					<th class="text-center"><input data-toggle="search-table" type="text" class="form-control form-control-small kt-input" placeholder="Cari disini..."></th>
					<th class="text-center">SPPT</th>
					<th class="text-right">Jumlah (Rp)</th>
					
					<th class="text-right">SPPT</th>
					<th class="text-right">Pokok PBB (Rp)</th>
					<th class="text-right">Denda (Rp)</th>
					<th class="text-right">Total (Rp)</th>
					
					<th class="text-right">SPPT</th>
					<th class="text-right">Pokok PBB (Rp)</th>
					<th class="text-right">Denda (Rp)</th>
					<th class="text-right">Total (Rp)</th>
				</tr>
			</thead>
			<tbody>
			@php
				$i = 0;
			@endphp
			@foreach($data as $rowKec)
				@php
					$kec = $rowKec->first();
				@endphp
				<tr class="tr-group-head {{$i++%2 == 0 ? 'even' : 'odd'}}" data-kec="{{$kec->kode_kec}}">
					<th data-searchable="true">
						KEC. {{$kec->nm_kecamatan}} 
						<a href="javascript:;" class="btn btn-brand btn-xs float-right" data-toggle="expand-kec" data-kec="{{$kec->kode_kec}}" ><i class="fa fa-plus p-0"></i></a>
					</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('sppt_tagih'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_tagih'))}}</th>
					
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('sppt_bayar'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_bayar'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_denda_bayar'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_bayar') + $rowKec->sum('tot_denda_bayar'))}}</th>
					
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('sppt_tagih') - $rowKec->sum('sppt_bayar'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_tagih') - $rowKec->sum('tot_bayar'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_denda_tagih'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_tagih') - $rowKec->sum('tot_bayar') + $rowKec->sum('tot_denda_tagih'))}}</th>
					<th></td>
				</tr>
				
				@foreach($rowKec as $row)
				<tr class="kt-hidden tr-group-child" data-kec="{{$row->kode_kec}}">
					<td data-searchable="true">{{$row->kode_kel.' '.$row->nm_kelurahan}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($row->sppt_tagih)}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($row->tot_tagih)}}</td>
					
					<td class="text-right">{{NumberHelper::numberToCurrId($row->sppt_bayar)}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($row->tot_bayar)}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($row->tot_denda_bayar)}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($row->tot_bayar + $row->tot_denda_bayar)}}</td>
					
					<td class="text-right">{{NumberHelper::numberToCurrId($row->sppt_tagih - $row->sppt_bayar)}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($row->tot_tagih - $row->tot_bayar)}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($row->tot_denda_tagih)}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($row->tot_tagih - $row->tot_bayar + $row->tot_denda_tagih)}}</td>
					<td></td>
				</tr>
				@endforeach
				
				<tr class="kt-hidden tr-group-child" data-kec="{{$row->kode_kec}}">
					<th>Total</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('sppt_tagih'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_tagih'))}}</th>
					
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('sppt_bayar'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_bayar'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_denda_bayar'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_bayar') + $rowKec->sum('tot_denda_bayar'))}}</th>
					
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('sppt_tagih') - $rowKec->sum('sppt_bayar'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_tagih') - $rowKec->sum('tot_bayar'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_denda_tagih'))}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($rowKec->sum('tot_tagih') - $rowKec->sum('tot_bayar') + $rowKec->sum('tot_denda_tagih'))}}</th>
					<th></td>
				</tr>
				<tr class="kt-hidden tr-group-child" data-kec="{{$row->kode_kec}}">
					<td colspan="11"></td>
				</tr>
			@endforeach
			</tbody>
		</table>
		@endif
		
		
	@endcomponent
	
@endsection

@push('css-plugin')
	<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endpush
@push('js-plugin')
	<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/bootstrap-datepicker/locales/bootstrap-datepicker.id.min.js') }}" type="text/javascript"></script>
	
	<script src="{{ asset('js/app/lap_penerimaan.js') }}" type="text/javascript"></script>
@endpush

@push('js-script')
<script>
	$('#table-main').tablePenerimaan({
		domFormFilter: '[name="form-filter"]',
		domSearch: '[data-toggle="search-table"]'
	});
</script>
@endpush