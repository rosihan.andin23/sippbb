@extends('template.template')

@section('title', 'Subjek Pajak')
@section('subheader-title', 'Subjek Pajak')
@section('subheader-subtitle', 'Subjek Pajak')

@section('content')

	@component('template.component.portlet')
		@slot('icon', 'kt-font-brand fa fa-users')
		@slot('title', 'Daftar Subjek Pajak')
		
		@slot('toolbar')
		@endslot
		
		<form method="POST" name="form-filter" action="">
			@csrf
			<div class="row kt-margin-b-20">
				<div class="col-lg-3 kt-margin-b-15-tablet-and-mobile">
					<label>Kecamatan Obyek Pajak:</label>
					<select class="form-control form-control-sm" name="kecamatan" option-required>
						<option value="">Pilih Kecamatan</option>
						@foreach($kecamatan as $row)
							<option value="{{$row->kd_kecamatan}}" {{isset($filter['kecamatan']) && $row->kd_kecamatan == $filter['kecamatan'] ? 'selected' : '' }} >{{$row->nm_kecamatan}}</option>
						@endforeach
					</select>
				</div>
				<div class="col-lg-3 kt-margin-b-15-tablet-and-mobile">
					<label>Kelurahan Obyek Pajak:</label>
					<select class="form-control form-control-sm" name="kelurahan" option-required>
						<option value="">Semua Kelurahan</option>
						@foreach($kecamatan as $kec) @foreach($kec->kelurahan as $row)
							<option value="{{$row->kd_kelurahan}}" data-kec="{{$kec->kd_kecamatan}}" {{isset($filter['kelurahan']) && $row->kd_kelurahan == $filter['kelurahan'] ? 'selected' : '' }} >{{$row->nm_kelurahan}}</option>
						@endforeach @endforeach
					</select>
				</div>
				
				<div class="col-lg-3 kt-margin-b-15-tablet-and-mobile">
					<label>Kata Kunci:</label>
					<input type="text" class="form-control form-control-sm kt-input" name="keyword" value="{{$filter['keyword'] ?? ''}}" option-required>
				</div>
				
				<div class="col-lg-3 col-6 kt-margin-b-15-tablet-and-mobile pt-3">
					<button class="btn btn-brand btn-sm btn-brand--icon mt-3" id="kt_search" data-url="{{route('wp.filter')}}"><i class="la la-search"></i> Tampilkan</button>
				</div>
				
			</div>
		</form>
		
		@if($filter)
		<table class="table table-striped table-bordered table-hover table-checkable" id="datatable-main" data-url="{{route('wp.datatable', ['encodedFilter'=>$encodedFilter])}}">
			<thead>
				<tr>
					<th data-name="subjek_pajak_id" width="100">Subjek Pajak ID</th>
					<th data-name="nm_wp">Nama</th>
					<th data-name="alamat_wp">Alamat Subjek Pajak</th>
					<th data-name="telp_wp" width="100">Telp</th>
					<th data-name="jml_op" data-class="text-center" width="50">Jml Objek Pajak</th>
					<th data-type='options' data-id="subjek_pajak_id" width="50">Detail</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		@endif

	@endcomponent
	
@endsection

@push('js-plugin')
	<script src="{{ asset('js/app/filter.js') }}" type="text/javascript"></script>
@endpush

@component('template.component.datatable')
<script>
	$('#datatable-main').filterTable({
		domFormFilter: '[name="form-filter"]',
	});

	@if($filter)
	
	var panel = $('#datatable-main').parents('.kt-portlet');
	KTApp.block(panel, {
		overlayColor: '#000000',
		type: 'v2',
		state: 'primary',
		message: 'Memproses data...',
	});
	
	$('#datatable-main').customDataTable(
	{
		order: [[1,'asc']],
		//searching: false,
		optionsRenderer : function (id, type, row, meta)
		{
			return '<a class="btn btn-xs btn-brand" href="{{route('wp.detail')}}/'+id+'" data-toggle="modal" data-target="#main-modal-lg"><i class="la la-clipboard p-0"></i></a>';
		},
		initComplete: function( settings ) {
      KTApp.unblock(panel);
    }

	});
	@endif
</script>
@endcomponent