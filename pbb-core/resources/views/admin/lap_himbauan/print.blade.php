<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Cetak Himbauan | PBB-P2</title>
	<meta name="description" content="Dashboard">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="{{ asset('css/global.custom.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('css/print.custom.css') }}" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" />
</head>
<body>

@php
	// Olah Template
	$replace = [
		'{PERIODE}' => $filter['print-periode'],
		'{TAHUN}' => date('Y', strtotime(DateHelper::shortIdToShortEn($filter['print-tgl']))),
		'{KOSONG}' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
		'{JATUH_TEMPO_SURAT}' => DateHelper::shortIdToLongId($filter['print-due-tagihan']),
		'{DENDA_ALL}' => $configs['DENDA_ALL'],
	];
	if($isPrintHimbauan)
	{
		$replace['{TAHUN}'] = $filter['print-periode'];
		$noSuratHimbauan = str_replace(array_keys($replace), array_values($replace), $configs['SURAT_HIMBAUAN_NO']);
		$templaHimbauan = str_replace(array_keys($replace), array_values($replace), $configs['SURAT_HIMBAUAN_ISI']);
		$templaHimbauan = explode('{DATA}', $templaHimbauan);
	}
	if($isPrintTagihan)
	{
		$replace['{TAHUN}'] = date('Y', strtotime(DateHelper::shortIdToShortEn($filter['print-tgl'])));
		$noSuratTagihan = str_replace(array_keys($replace), array_values($replace), $configs['SURAT_TAGIHAN_NO']);
		$templaTagihan = str_replace(array_keys($replace), array_values($replace), $configs['SURAT_TAGIHAN_ISI']);
		$templaTagihan = explode('{DATA}', $templaTagihan);
	}
	


@endphp

@foreach($data as $item)
@php
	$firstItem = $item[0];
	$currHimbauanItem = false;
	foreach($item as $row) if($row->thn_pajak_sppt == $filter['print-periode']) 
	{
		$currHimbauanItem = $row;
	}
	
@endphp
@if($isPrintHimbauan && $currHimbauanItem)
<div class="page">
	<div class="header clearfix">
		<img alt="Logo" src="{{ asset('img/logo.png') }}" class="logo"/>
		<div>
			<div class="text-center"><b>PEMERINTAH KABUPATEN NGAWI</b></div>
			<div class="f-24 text-center"><b>BADAN KEUANGAN</b></div>
			<div class="f-12 text-center">
				{{ucwords(strtolower($kantor->al_kppbb))}} Ngawi {{$kantor->kode_pos}}<br>
				Telp/Fax {{$kantor->no_telpon}}, e-mail : {{$kantor->email}}<br>
				Website: {{$kantor->website}}
			</div>
		</div>
		
		<hr class="double-line"/>
	</div>
	
	<div class="body">

		<div class="text-center"><b><u>SURAT HIMBAUAN</u></b></div>
		<div class="text-center">Nomor : {!!$noSuratHimbauan!!}</div>
		<br>
		{!! $templaHimbauan[0] !!}
		
		<table>
			<tr>
				<td width="230">Nomor Objek Pajak SPPT</td><td width="15">:</td><td>{{$currHimbauanItem->nop}}</td>
			</tr>
			<tr>
				<td>Nama Wajib Pajak</td><td>:</td><td>{{$currHimbauanItem->nm_wp_sppt}}</td>
			</tr>
			<tr>
				<td>Alamat Wajib Pajak</td><td>:</td><td>{{$currHimbauanItem->jln_wp_sppt}} {{$currHimbauanItem->blok_kav_no_wp_sppt}} {{$currHimbauanItem->kelurahan_wp_sppt}} {{$currHimbauanItem->kota_wp_sppt}}</td>
			</tr>
			<tr>
				<td>Alamat Objek Pajak</td><td>:</td><td>{{$currHimbauanItem->jalan_op}}  {{$currHimbauanItem->blok_kav_no_op}} {{$currHimbauanItem->nm_kelurahan}} {{$currHimbauanItem->nm_kecamatan}}</td>
			</tr>
		</table>
		<table width="100%" cellspacing="0" class="bordered">
			<tr>
				<th>No</th>
				<th>Tahun</th>
				<th>PBB Terhutang</th>
				<th>Kekurangan</th>
				<th>Jatuh Tempo</th>
			</tr>
			@foreach($item as $i=>$row) @if($row->thn_pajak_sppt == $filter['print-periode'])
			<tr>
				<td class="text-center">{{$i+1}}</td>
				<td class="text-center">{{$row->thn_pajak_sppt}}</td>
				<td class="text-right">{{NumberHelper::numberToCurrId($row->pbb_terhutang_sppt)}}</td>
				<td class="text-right">{{NumberHelper::numberToCurrId($row->pbb_yg_harus_dibayar_sppt)}}</td>
				<td class="text-center">{{DateHelper::shortEnToLongId($row->tgl_jatuh_tempo_sppt)}}</td>
			</tr>
			@endif @endforeach
		</table>
		
		{!! $templaHimbauan[1] !!}
		
		<table width="100%">
			<tr>
				<td ></td>
				<td class="text-center f-12 ttd" width="430"> 
					{!!$configs['SURAT_HIMBAUAN_TTD']!!}
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="page-break"></div>
@endif


@if($isPrintTagihan)
<div class="page">
	<div class="header clearfix">
		<img alt="Logo" src="{{ asset('img/logo.png') }}" class="logo"/>
		<div>
			<div class="text-center"><b>PEMERINTAH KABUPATEN NGAWI</b></div>
			<div class="f-24 text-center"><b>BADAN KEUANGAN</b></div>
			<div class="f-12 text-center">
				{{ucwords(strtolower($kantor->al_kppbb))}} Ngawi {{$kantor->kode_pos}}<br>
				Telp/Fax {{$kantor->no_telpon}}, e-mail : {{$kantor->email}}<br>
				Website: {{$kantor->website}}
			</div>
		</div>
		
		<hr class="double-line"/>
	</div>
	
	<div class="body f-12">
		<div class="text-right">Ngawi, {{DateHelper::shortIdToLongId($filter['print-tgl'])}}</div>
		<table width="100%">
			<tr>
				<td width="100">
					Nomor<br>
					Sifat<br>
					Lampiran<br>
					Perihal
				</td>
				<td width="15">
					:<br>
					:<br>
					:<br>
					:
				</td>
				<td width="220" >
					{!!$noSuratTagihan!!}<br>
					Penting<br>
					1 ( Satu ) Bendel<br>
					<b>{!!nl2br($filter['print-hal-tagihan'])!!}</b>
					<hr>
				</td>
				<td rowspan="4"></td>
				<td width="220">
					Kepada : <br/>
					Yth. <b><i>{{$firstItem->nm_wp_sppt}}</i></b><br/>					
					di-<br/>
					{{$firstItem->jln_wp_sppt}} {{$firstItem->blok_kav_no_wp_sppt}} {{$firstItem->kelurahan_wp_sppt}}<br/>
					<u>{{$firstItem->kota_wp_sppt}}</u>
				</td>
			</tr>
		</table>
		
		{!! $templaTagihan[0] !!}
		
		<table>
			<tr>
				<td width="210">Nomor Objek Pajak SPPT</td><td width="15">:</td>
				<td><b>{{$firstItem->nop}}</b></td>
			</tr>
			<tr>
				<td>Nama Wajib Pajak</td><td>:</td>
				<td>{{$firstItem->nm_wp_sppt}}</td>
			</tr>
			<tr>
				<td>Alamat Wajib Pajak</td><td>:</td>
				<td>{{$firstItem->jln_wp_sppt}}  {{$firstItem->blok_kav_no_wp_sppt}} {{$firstItem->kelurahan_wp_sppt}} {{$firstItem->kota_wp_sppt}}</td>
			</tr>
			<tr>
				<td>Alamat Objek Pajak</td><td>:</td>
				<td>{{$firstItem->jalan_op}} {{$firstItem->blok_kav_no_op}} {{$firstItem->nm_kelurahan}} {{$firstItem->nm_kecamatan}}</td>
			</tr>
		</table>

		<table width="100%" cellspacing="0" class="bordered">
			<tr>
				<th>No</th>
				<th>Tahun</th>
				<th>PBB Terhutang</th>
				<th>Kekurangan</th>
				<th>Denda</th>
				<th>Denda + Kekurangan</th>
				<th>Jatuh Tempo</th>
			</tr>
			@foreach($item as $i=>$sppt)
			@php
				$denda = NumberHelper::calcFinePerMonths($sppt->pbb_yg_harus_dibayar_sppt, $configs['DENDA_ALL'], $sppt->bulan_telat);
			@endphp 
			<tr>
				<td class="text-center">{{$i+1}}</td>
				<td class="text-center">{{$sppt->thn_pajak_sppt}}</td>
				<td class="text-right">{{NumberHelper::numberToCurrId($sppt->pbb_terhutang_sppt)}}</td>
				<td class="text-right">{{NumberHelper::numberToCurrId($sppt->pbb_yg_harus_dibayar_sppt)}}</td>
				<td class="text-right">{{NumberHelper::numberToCurrId($denda)}}</td>
				<td class="text-right">{{NumberHelper::numberToCurrId($sppt->pbb_yg_harus_dibayar_sppt + $denda)}}</td>
				<td class="text-center">{{DateHelper::shortEnToLongId($sppt->tgl_jatuh_tempo_sppt)}}</td>
			</tr>
			@endforeach
		</table>
		
		{!! $templaTagihan[1] !!}
		
		<table width="100%">
			<tr>
				<td ></td>
				<td class="text-center f-12 ttd" width="430"> 
					{!!$configs['SURAT_TAGIHAN_TTD']!!}
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="page-break"></div>
@endif

@endforeach

<script>
	window.print();
</script>
</body>

</html>