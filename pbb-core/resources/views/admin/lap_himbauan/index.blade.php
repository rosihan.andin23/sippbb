@extends('template.template')

@section('title', 'Penerbitan Surat Himbauan')
@section('subheader-title', 'Penerbitan Surat Himbauan')
@section('subheader-subtitle', 'SPPT')

@section('content')

	@component('template.component.portlet')
		@slot('icon', 'kt-font-brand flaticon-technology ')
		@slot('title', 'Pilih Periode Laporan')
		
		@slot('toolbar')
		@endslot
		
		<form method="POST" name="form-filter" action="">
			@csrf
			<div class="row kt-margin-b-20">
				<div class="col-lg-2 kt-margin-b-15-tablet-and-mobile">
					<label>Kecamatan:</label>
					<select class="form-control form-control-sm" name="kecamatan" required>
						<option value="">Pilih Kecamatan</option>
						@foreach($kecamatan as $row)
							<option value="{{$row->kode_with_separator}}" {{isset($filter) && $row->kode_with_separator == $filter['kecamatan'] ? 'selected' : '' }} >{{$row->nm_kecamatan}}</option>
						@endforeach
					</select>
				</div>
				<div class="col-lg-3 kt-margin-b-15-tablet-and-mobile">
					<label>Kelurahan:</label>
					<select class="form-control form-control-sm" name="kelurahan">
						<option value="">Semua Kelurahan</option>
						@foreach($kecamatan as $kec) @foreach($kec->kelurahan as $row)
							<option value="{{$row->kode_with_separator}}" data-kec="{{$kec->kode_with_separator}}" {{isset($filter) && $row->kode_with_separator == $filter['kelurahan'] ? 'selected' : '' }} >{{$row->nm_kelurahan}}</option>
						@endforeach @endforeach
					</select>
				</div>
				<div class="col-lg-3 kt-margin-b-15-tablet-and-mobile">
					<label>Tahun:</label>
					<div class="input-group input-group-sm">
						<input class="form-control" type="number" name="thn_start" value="{{$filter['thn_start'] ?? '2014'}}">
						<div class="input-group-prepend input-group-append">
								<div class="input-group-text">s/d</div>
						</div>
						<input class="form-control" type="number" name="thn_end" value="{{$filter['thn_end'] ?? date('Y')}}">
					</div>
				</div>
				<div class="col-lg-3 col-6 kt-margin-b-15-tablet-and-mobile">
					<label>Buku:</label>
					<select class="form-control form-control-sm apply-select2" name="buku[]" data-col-index="3" multiple  required>
						@foreach($buku as $row)
							<option value="{{$row->kd_buku}}" {{!isset($filter) || in_array($row->kd_buku, $filter['buku']) ? 'selected' : '' }} >{{$row->kd_buku}}</option>
						@endforeach
					</select>
				</div>
				
			</div>
			
			<div class="row kt-margin-b-20">
				
				<div class="col-lg-3 kt-margin-b-15-tablet-and-mobile">
					<label>Pencarian:</label>
					<input type="text" class="form-control form-control-sm kt-input" name="keyword" value="{{$filter['keyword'] ?? ''}}" >
				</div>
				
				<div class="col-lg-9 col-6 kt-margin-b-15-tablet-and-mobile pt-3">
					<button class="btn btn-brand btn-sm btn-brand--icon mt-3" id="kt_search" data-url="{{route('lap.himbauan.filter')}}"><i class="la la-search"></i> Tampilkan</button>
					<button class="btn btn-success btn-sm btn-success--icon mt-3" id="kt_download" data-url="{{route('lap.tunggakan.pdf')}}" data-target="_blank"><i class="la la-download"></i> Download Laporan</button>
					@if(isset($data) && $data)
					&nbsp;&nbsp;&nbsp;
					<button class="btn btn-primary btn-sm btn-primary--icon mt-3" type="button" data-url="{{route('lap.himbauan.print')}}" name="btn-print" id="kt_print"><i class="la la-print"></i> Cetak yang dicentang</button>
					<!--button class="btn btn-success btn-sm btn-success--icon mt-3" type="button" data-url="{{route('lap.himbauan.download')}}" name="btn-download" id="kt_download"><i class="la la-cloud-download"></i> Download yang dicentang</button-->
					@endif
				</div>
			</div>
			
		</form>
		
		<hr>
		
		@if(isset($data) && $data)
		<form method="POST" name="form-print" action="" target="_blank">
		@csrf
		<input type="hidden" name="thn_start" value="{{$filter['thn_start']}}">
		<input type="hidden" name="thn_end" value="{{$filter['thn_end']}}">
		<input type="hidden" name="kecamatan" value="{{$filter['kecamatan']}}">
		<input type="hidden" name="kelurahan" value="{{$filter['kelurahan']}}">
		
		<table class="table table-striped table-bordered table-hover table-checkable font-courier" id="table-main" data-url="{{route('kelurahan.datatable')}}">
			<thead>
				<tr>
					<th class="text-center">
						<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"><input type="checkbox" name="check-all"><span></span></label>
					</th>
					<th>NOP</th>
					<th>Nama WP</th>
					<th>Alamat WP</th>
					<th>Alamat OP</th>
					<th class="text-right">Pajak yang harus dibayar</th>
					<th class="text-right">Denda</th>
					<th class="text-center">Tahun Pajak</th>
				</tr>
			</thead>
			<tbody>

			@foreach($data as $rowWp)
				@php
					$rowspan = count($rowWp) > 1 ? 'rowspan="'.count($rowWp).'"' : ''
				@endphp
				@foreach($rowWp as $i=>$rowWpPeriod)
					@if($i == 0)
					<tr class="tr-group-head" >
						<td {!! $rowspan ?: '' !!} class="text-center">
							<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand"><input type="checkbox" name="check-item[]" value="{{$rowWpPeriod->kode_op}}"><span></span></label>
						</td>
						<td {!! $rowspan ?: '' !!}>
							<a href="{{route('op.detail', ['id'=>$rowWpPeriod->kode_op])}}" data-toggle="modal" data-target="#main-modal-lg">
								{{$rowWpPeriod->kode_op}}
							</a>
						</td>
						<td {!! $rowspan ?: '' !!}>
							<a href="{{route('wp.detail', ['id'=>trim($rowWpPeriod->subjek_pajak_id)])}}" data-toggle="modal" data-target="#main-modal-lg">
								{{$rowWpPeriod->nm_wp_sppt}}
							</a>
						</td>
						<td {!! $rowspan ?: '' !!}>{{$rowWpPeriod->jln_wp_sppt}} {{$rowWpPeriod->blok_kav_no_wp_sppt}}</td>
						<td {!! $rowspan ?: '' !!}>{{$rowWpPeriod->jalan_op}} {{$rowWpPeriod->blok_kav_no_op}} {{$rowWpPeriod->nm_kelurahan}} {{$rowWpPeriod->nm_kecamatan}}</td>
						<td class="text-right">{{NumberHelper::numberToCurrId($rowWpPeriod->pbb_yg_harus_dibayar_sppt)}}</td>
						<td class="text-right">{{NumberHelper::numberToCurrId(NumberHelper::calcFinePerMonths($rowWpPeriod->pbb_yg_harus_dibayar_sppt, $denda, $rowWpPeriod->bulan_telat)) }}</td>
						<td class="text-center">{{$rowWpPeriod->thn_pajak_sppt}}</td>
					</tr>
					@else
						<tr class="tr-group-head" >
							<td class="text-right">{{NumberHelper::numberToCurrId($rowWpPeriod->pbb_yg_harus_dibayar_sppt)}}</td>
							<td class="text-right">{{NumberHelper::numberToCurrId(NumberHelper::calcFinePerMonths($rowWpPeriod->pbb_yg_harus_dibayar_sppt, $denda, $rowWpPeriod->bulan_telat)) }}</td>
							<td class="text-center">{{$rowWpPeriod->thn_pajak_sppt}}</td>
						</tr>
					@endif
				
				@endforeach
			@endforeach
			
			</tbody>
		</table>
		
		<div id="doc-options" class="kt-hidden">
			
			<div class="row kt-margin-t-20">
				<div class="col">
					<div class="form-group  text-left">
						<div class="kt-checkbox-inline">
							<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
								<input type="checkbox" name="print-himbauan" value="1" checked="checked"> Surat Himbauan
								<span></span>
							</label>
						</div>
					</div>
					<div class="form-group text-left">
						<label>Periode Surat Himbauan</label>
						<input class="form-control" name="print-periode" type="number" value="{{date('Y')}}" >
					</div>
				</div>
				
				
				<div class="col">
					<div class="form-group  text-left">
						<div class="kt-checkbox-inline">
							<label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
								<input type="checkbox" name="print-tagihan" value="1" checked="checked">Surat Tagihan PBB-P2
								<span></span>
							</label>
						</div>
					</div>
					<div class="form-group text-left">
						<label>Tgl Surat Tagihan</label>
						<input class="form-control" name="print-tgl" datepicker type="text" value="" >
					</div>
					<div class="form-group text-left">
						<label>Perihal Surat Tagihan</label>
						<textarea class="form-control" name="print-hal-tagihan" rows="2">Tagihan ke I
Pembayaran PBB P2</textarea>
					</div>
					<div class="form-group text-left">
						<label>Jatuh Tempo Surat Tagihan</label>
						<input class="form-control" name="print-due-tagihan" datepicker type="text" value="" >
					</div>
				</div>
				
			</div>
		</div>
		</form>
		@endif
		
	@endcomponent
	
@endsection

@push('css-script')
	<style>
		th .kt-checkbox>span, td .kt-checkbox>span{
			left: 7px;
		}
		.datepicker.datepicker-orient-top.datepicker-dropdown{
			z-index: 1070!important;
		}
	</style>
@endpush

@push('css-plugin')
	<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endpush
@push('js-plugin')
	<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/bootstrap-datepicker/locales/bootstrap-datepicker.id.min.js') }}" type="text/javascript"></script>
	
	<script src="{{ asset('js/app/lap_himbauan.js') }}" type="text/javascript"></script>
@endpush

@push('js-script')
<script>
	$('#table-main').tableHimbauan({
		domFormFilter: '[name="form-filter"]',
		domFormPrint: '[name="form-print"]',
		domDocOptions: '#doc-options',
		domTable: '#table-main',
	});

</script>
@endpush