@extends('template.template')

@section('title', 'Referensi Buku')
@section('subheader-title', 'Referensi')
@section('subheader-subtitle', 'Buku')

@section('content')

	@component('template.component.portlet')
		@slot('icon', 'kt-font-brand fa fa-book')
		@slot('title', 'Daftar Buku')
		
		@slot('toolbar')
		@endslot

		<table class="table table-striped table-bordered table-hover table-checkable" id="datatable-main" data-url="{{route('buku.datatable')}}">
			<thead>
				<tr>
					<th data-name="thn_awal">Tahun</th>
					<th data-name="kd_buku">Kode Buku</th>
					<th data-name="nilai_min_buku">Nilai</th>
					<th data-type='options' data-id="kd_buku" width="50">Opsi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	@endcomponent
	
@endsection

@component('template.component.datatable')
<script>
$('#datatable-main').customDataTable(
{
	optionsRenderer : function (id, type, row, meta)
	{
		return ' <span class="dropdown">'+
			'<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">'+
				'<i class="la la-gear"></i>'+
			'</a>'+
			'<div class="dropdown-menu dropdown-menu-right">'+
				'<a class="dropdown-item" href="#" data-toggle="modal" data-target="#main-modal-md"><i class="la la-edit"></i> Ubah data</a>'+
			'</div>'+
		'</span>';
	},
	postConfig: function(configs)
	{
		configs.columnDefs.push({
			targets: 0, 
			render: function(data, type, row, meta){ return row.thn_awal+' s/d '+row.thn_akhir; },
		});
		configs.columnDefs.push({
			targets: 2, 
			render: function(data, type, row, meta){ 
				return row.nilai_min_buku.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+' s/d '+row.nilai_max_buku.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."); 
			},
		});
		return configs;
	}
});
</script>
@endcomponent