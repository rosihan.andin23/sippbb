@extends('template.modal')

@section('modal-title', $id ? 'Ubah Pengguna' : 'Tambah Pengguna')

@section('modal-content')
<form method="POST" action="{{route('user.store', ['id'=>$id])}}" class="kt-form" ajax>
	@csrf

	@component('template.component.form.input')
		@slot('name', 'nama_lengkap')
		@slot('class', 'form-control-sm')
		{{$data ? $data->nama_lengkap : old('nama_lengkap')}}
	@endcomponent
	
	<div class="row">
		<div class="col">
			@component('template.component.form.input')
				@slot('name', 'username')
				@slot('class', 'form-control-sm')
				{{$data ? $data->username : old('username')}}
			@endcomponent
		</div>
		<div class="col">
			@component('template.component.form.input')
				@slot('label', 'NIP')
				@slot('name', 'nip')
				@slot('class', 'form-control-sm')
				{{$data ? $data->nip : old('nip')}}
			@endcomponent
		</div>
	</div>
	
	<div class="row">
		<div class="col">
			@component('template.component.form.input')
				@slot('name', 'email')
				@slot('type', 'email')
				@slot('class', 'form-control-sm')
				{{$data ? $data->email : old('email')}}
			@endcomponent
		</div>
		<div class="col">
			@component('template.component.form.input')
				@slot('name', 'telepon')
				@slot('class', 'form-control-sm')
				{{$data ? $data->telepon : old('telepon')}}
			@endcomponent
		</div>
	</div>
	
	@component('template.component.form.select')
		@slot('label', 'Otoritas')
		@slot('name', 'role')
		@slot('class', 'form-control-sm')
		@slot('data', [(object)['value'=>'user', 'name'=>'Kecamatan'], (object)['value'=>'admin', 'name'=>'Administrator']])
		@slot('dataValue', 'value')
		@slot('dataText', 'name')
		@slot('value', $data ? $data->role : null)
	@endcomponent
	
	@component('template.component.form.select')
		@slot('label', 'Otoritas Kecamatan')
		@slot('class', 'apply-select2 form-control-sm')
		@slot('name', 'kecamatan[]')
		@slot('attr', 'multiple')
		@slot('data', $kecamatan)
		@slot('dataValue', 'kode')
		@slot('dataText', 'nm_kecamatan')
		@slot('value', $data ? $data->userKecamatan->pluck('kd_kecamatan')->all() : null)
		@slot('note', 'Pilih Kecamatan jika pengguna berotoritas sebagai staf Kecamatan')
	@endcomponent
	
	<br>
	
	@component('template.component.form.input')
		@slot('label', $id ? 'isi untuk ganti password' : 'Password')
		@slot('class', 'form-control-sm')
		@slot('name', 'password')
		@slot('type', 'password')
	@endcomponent

</form>	
@endsection

@section('modal-button')
	<button type="button" class="btn btn-primary" data-toggle="modal-submit" >Simpan</button>
@endsection