@extends('template.template')

@section('title', 'Pengguna Kecamatan')
@section('subheader-title', 'Pengguna')
@section('subheader-subtitle', 'Kecamatan')

@section('content')

	@component('template.component.portlet')
		@slot('icon', 'kt-font-brand fa fa-users')
		@slot('title', 'Daftar Pengguna Kecamatan')
		
		@slot('toolbar')
			<a href="{{route('user.form')}}" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal" data-target="#main-modal-md">
				<i class="la la-plus"></i> Tambah Baru
			</a>
		@endslot

		<table class="table table-striped table-bordered table-hover table-checkable" id="datatable-main" data-url="{{route('user.datatable')}}">
			<thead>
				<tr>
					<th data-type='#' width="10">No.</th>
					<th>Username</th>
					<th data-name="nama_lengkap">Nama Pengguna</th>
					<th>Email</th>
					<th>Telepon</th>
					<th data-name="role">Otoritas</th>
					<th data-name="kecamatan_str">Kecamatan</th>
					<th data-type='options' data-id="id" width="50">Opsi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	@endcomponent
	
@endsection

@component('template.component.datatable')
<script>
$('#datatable-main').customDataTable(
{
	optionsRenderer : function (id, type, row, meta)
	{
		var active = row.is_active == 'YES' ? 
		'<a class="dropdown-item" href="{{route('user.activate')}}/'+id+'/NO" data-toggle="ajax-confirm" data-message-text="Pengguna tersebut tidak akan bisa login ke dalam sistem" data-success-text="Pengguna berhasil dinonaktifkan"><i class="la la-ban"></i> Non aktifkan</a>' : 
		'<a class="dropdown-item" href="{{route('user.activate')}}/'+id+'/YES" data-toggle="ajax-confirm" data-message-text="Pengguna tersebut akan bisa login kembali ke dalam sistem" data-success-text="Pengguna berhasil diaktifkan"><i class="la la-check"></i> Aktifkan</a>';
		
		return ' <span class="dropdown">'+
			'<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">'+
				'<i class="la la-gear"></i>'+
			'</a>'+
			'<div class="dropdown-menu dropdown-menu-right">'+
				'<a class="dropdown-item" href="{{route('user.form')}}/'+id+'" data-toggle="modal" data-target="#main-modal-md"><i class="la la-edit"></i> Ubah data</a>'+
				//'<a class="dropdown-item" href="#"><i class="la la-dashboard"></i> Lihat aktifitas</a>'+
				//'<a class="dropdown-item" href="#"><i class="la la-sign-in"></i> Login sebagai</a>'+
				active+
				'<a class="dropdown-item" href="{{route('user.delete')}}/'+id+'" data-toggle="ajax-confirm"><i class="la la-trash"></i> Hapus</a>'+
			'</div>'+
		'</span>';
	},
	postConfig: function(configs)
	{
		configs.columnDefs.push({
			targets: 1, 
			render: function(data, type, row, meta)
			{
				return data+' '+(row.is_active == 'NO' ? '<span class="kt-badge kt-badge--inline kt-badge--danger ml-3">Tidak Aktif</span>' : '');
			}
		});
		
		configs.columnDefs.push({
			targets: 5, 
			render: function(data, type, row, meta)
			{
				return data=='admin' ? 'Administrator' : 'Kecamatan';
			}
		});
		
		configs.columnDefs.push({
			targets: 6, 
			render: function(data, type, row, meta)
			{
				return row.role=='admin' ? 'Semua Kecamatan' : data;
			}
		});
		return configs;
	}
});
</script>
@endcomponent