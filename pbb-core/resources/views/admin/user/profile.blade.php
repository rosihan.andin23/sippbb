@extends('template.modal')

@section('modal-title', 'Profile Saya')

@section('modal-content')
<form method="POST" action="{{route('profile')}}" class="kt-form" ajax>
	@csrf
	<div class="row">
		<div class="col">
			@component('template.component.form.text')
				@slot('label', 'Nama Lengkap :')
				{{$data->nama_lengkap}}
			@endcomponent
			
			@component('template.component.form.text')
				@slot('label', 'Username :')
				{{$data->username}}
			@endcomponent
			
			@component('template.component.form.input')
				@slot('name', 'telepon')
				@slot('class', 'form-control-sm')
				{{$data ? $data->telepon : old('telepon')}}
			@endcomponent
		</div>
		
		<div class="col">
			@component('template.component.form.text')
				@slot('label', 'NIP :')
				{{$data->nip}}
			@endcomponent
			
			@component('template.component.form.text')
				@slot('label', 'Otoritas :')
				{{$data->role == 'admin' ? 'Administrator' : 'Kecamatan : '.$data->kecamatanStr}}
			@endcomponent
			
			@component('template.component.form.input')
				@slot('name', 'email')
				@slot('class', 'form-control-sm')
				{{$data ? $data->email : old('email')}}
			@endcomponent
		</div>
	</div>
	
	<h5 class="mt-3 mb-3">Ubah Password</h5>
	
	@component('template.component.form.input')
		@slot('label','Password lama')
		@slot('name', 'password_lama')
		@slot('class', 'form-control-sm')
		@slot('type', 'password')
	@endcomponent
	
	<div class="row">
		<div class="col">
			@component('template.component.form.input')
				@slot('label','Password baru')
				@slot('name', 'password_baru')
				@slot('class', 'form-control-sm')
				@slot('type', 'password')
			@endcomponent
		</div>
		
		<div class="col">
			@component('template.component.form.input')
				@slot('label','Ulangi Password baru')
				@slot('name', 'ulangi_password')
				@slot('class', 'form-control-sm')
				@slot('type', 'password')
			@endcomponent
		</div>
	</div>

</form>	
@endsection

@section('modal-button')
	<button type="button" class="btn btn-primary" data-toggle="modal-submit" >Simpan</button>
@endsection