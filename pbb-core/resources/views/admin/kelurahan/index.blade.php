@extends('template.template')

@section('title', 'Referensi Wilayah')
@section('subheader-title', 'Referensi')
@section('subheader-subtitle', 'Wilayah')

@section('content')

	@component('template.component.portlet')
		@slot('icon', 'kt-font-brand fa fa-map-signs')
		@slot('title', 'Daftar Wilayah')
		
		@slot('toolbar')
		@endslot

		<table class="table table-striped- table-bordered table-hover table-checkable" id="datatable-main" data-url="{{route('kelurahan.datatable')}}">
			<thead>
				<tr>
					<th data-type="#" width="20">No.</th>
					<th data-name="kd_kelurahan" width="125">Kode Kelurahan</th>
					<th data-name="kd_sektor" data-class="text-center" width="30">Sektor </th>
					<th data-name="nm_kelurahan">Kelurahan</th>
					<th data-name="nm_kecamatan">Kecamatan</th>
					<th data-name="no_kelurahan" data-class="text-center" width="50">No. Kel</th>
					<th data-name="kd_pos_kelurahan" data-class="text-center" width="70">Kode Pos</th>
					<th data-type='options' data-id="kd_buku" width="50">Opsi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	@endcomponent
	
@endsection

@component('template.component.datatable')
<script>
$('#datatable-main').customDataTable(
{
	order: [[4, 'asc'],[1, 'asc']],
	drawCallback: function(settings) {
		var api = this.api();
		var rows = api.rows({page: 'current'}).nodes();
		var last = null;
		api.column(4, {page: 'current'}).data().each(function(group, i) {
			if (last !== group) {
				$(rows).eq(i).before(
					'<tr class="group btn-primary"><td colspan="10">Kecamatan ' + group + '</td></tr>',
				);
				last = group;
			}
		});
	},
	optionsRenderer : function (id, type, row, meta)
	{
		return ' <span class="dropdown">'+
			'<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">'+
				'<i class="la la-gear"></i>'+
			'</a>'+
			'<div class="dropdown-menu dropdown-menu-right">'+
				'<a class="dropdown-item" href="#" data-toggle="modal" data-target="#main-modal-md"><i class="la la-edit"></i> Ubah data</a>'+
			'</div>'+
		'</span>';
	},
	postConfig: function(configs)
	{
		configs.columnDefs.push({
			targets: 1, 
			render: function(data, type, row, meta){ return row.kd_propinsi.toString()+row.kd_dati2.toString()+row.kd_kecamatan.toString()+row.kd_kelurahan.toString(); },
		});

		return configs;
	}
});
</script>
@endcomponent