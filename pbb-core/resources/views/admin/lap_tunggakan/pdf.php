<?php 

	// var
	$defCellHeight = 5.5;
	$defFontSize = 12;
	$kelurahan = false;
	if($filter['kelurahan']){
		$kelurahan = $kecamatan->kelurahan()->kodeWithSeparatorEqual($filter['kelurahan'])->first();
	}
	
	// header
	$pdf->AliasNbPages();
	$pdf->AddPage('P','A4');
	$pdf->SetMargins(3, 5);
	$pdf->SetFont('Times','B', 16);
	$pdf->Cell(0, $defCellHeight, 'LAPORAN DATA BELUM BAYAR', 0, 1, 'C');
	$pdf->SetFont('Times','B', 12);
	$pdf->Cell(0, $defCellHeight, 'KEC. '.$kecamatan->nm_kecamatan.($kelurahan ? ' KEL.'.$kelurahan->nm_kelurahan : ''), 0, 1, 'C');
	$pdf->Cell(0, $defCellHeight, 'PERIODE '.$filter['thn_start'].' - '.$filter['thn_end'], 0, 1, 'C');
	$pdf->SetFont('Times','', 10);
	$pdf->Cell(0, $defCellHeight, 'BUKU '.implode(', ',$filter['buku']), 0, 1, 'C');
	$pdf->Ln();
	
	// body
	$tableWidth = [8, 38, 27, 40, 40, 18, 18, 14];
	$tableAlign = ['C', '', '' ,'', '', 'R', 'R', 'C'];
	$pdf->SetColsWidth($tableWidth);
	$pdf->SetColsAlign($tableAlign);
	
	// table header
	$pdf->SetFont('Times', 'B', 8);
	$tableHeader = ['No.', 'NOP', 'Nama WP' ,'Alamat WP', 'Alamat OP', 'Pajak yang harus dibayar', 'Denda', 'Tahun Pajak'];
	$pdf->Row($tableHeader, 3.5);

	// table body
	$j = 0;
	$totTagih = 0;
	$totDenda = 0;
	$pdf->SetFont('Times', '');
	foreach($data as $rowWp) 
	{
		$isStartRowSpan = count($rowWp) > 1;
		foreach($rowWp as $i=>$rowWpPeriod)
		{
			$isRowSpan = ($i > 0);
			$dendaCalc = NumberHelper::calcFinePerMonths($rowWpPeriod->pbb_yg_harus_dibayar_sppt, $denda, $rowWpPeriod->bulan_telat);
			$totTagih += $rowWpPeriod->pbb_yg_harus_dibayar_sppt;
			$totDenda += $dendaCalc;
			
			if($isStartRowSpan && $i==0) $pdf->StartRowSpan([0,1,2,3,4]);
			if(($isRowSpan && $i==count($rowWp)-1) || !$isStartRowSpan) $pdf->EndRowSpan([0,1,2,3,4]);

			$pdf->Row([
				!$isRowSpan ? ++$j : '',
				!$isRowSpan ? $rowWpPeriod->kode_op : '',
				!$isRowSpan ? $rowWpPeriod->nm_wp_sppt : '',
				!$isRowSpan ? $rowWpPeriod->jln_wp_sppt.' '.$rowWpPeriod->blok_kav_no_wp_sppt : '',
				!$isRowSpan ? $rowWpPeriod->jalan_op.' '.$rowWpPeriod->blok_kav_no_op.' '.$rowWpPeriod->nm_kelurahan.' '.$rowWpPeriod->nm_kecamatan : '',
				NumberHelper::numberToCurrId($rowWpPeriod->pbb_yg_harus_dibayar_sppt),
				NumberHelper::numberToCurrId($dendaCalc),
				$rowWpPeriod->thn_pajak_sppt,
			]);
		}
	}
	
	// table footer
	$pdf->SetFont('Times', 'B', 8);
	$pdf->Row([
		'',
		'',
		'',
		'',
		'Total :',
		NumberHelper::numberToCurrId($totTagih),
		NumberHelper::numberToCurrId($totDenda),
		'',
	]);
	
	$pdf->Output('I', 'Laporan Belum Bayar '.($filter['kelurahan'] ?? $filter['kecamatan']).' Periode '.$filter['thn_start'].' - '.$filter['thn_end'].'.pdf');
	exit;