@extends('template.template')

@section('title', 'Informasi Data Belum Bayar')
@section('subheader-title', 'Laporan')
@section('subheader-subtitle', 'Data Belum Bayar')

@section('content')

	@component('template.component.portlet')
		@slot('icon', 'kt-font-brand fa fa-exclamation-triangle ')
		@slot('title', 'Pilih Periode Laporan Data Belum Bayar')
		
		@slot('toolbar')
		@endslot
		
		<form method="POST" name="form-filter" action="">
			@csrf
			<div class="row kt-margin-b-20">
				<div class="col-lg-2 kt-margin-b-15-tablet-and-mobile">
					<label>Kecamatan:</label>
					<select class="form-control form-control-sm" name="kecamatan" required>
						<option value="">Pilih Kecamatan</option>
						@foreach($kecamatan as $row)
							<option value="{{$row->kode_with_separator}}" {{isset($filter) && $row->kode_with_separator == $filter['kecamatan'] ? 'selected' : '' }} >{{$row->nm_kecamatan}}</option>
						@endforeach
					</select>
				</div>
				<div class="col-lg-3 kt-margin-b-15-tablet-and-mobile">
					<label>Kelurahan:</label>
					<select class="form-control form-control-sm" name="kelurahan">
						<option value="">Semua Kelurahan</option>
						@foreach($kecamatan as $kec) @foreach($kec->kelurahan as $row)
							<option value="{{$row->kode_with_separator}}" data-kec="{{$kec->kode_with_separator}}" {{isset($filter) && $row->kode_with_separator == $filter['kelurahan'] ? 'selected' : '' }} >{{$row->nm_kelurahan}}</option>
						@endforeach @endforeach
					</select>
				</div>
				<div class="col-lg-3 kt-margin-b-15-tablet-and-mobile">
					<label>Tahun:</label>
					<div class="input-group input-group-sm">
						<input class="form-control" type="number" name="thn_start" value="{{$filter['thn_start'] ?? '2014'}}">
						<div class="input-group-prepend input-group-append">
								<div class="input-group-text">s/d</div>
						</div>
						<input class="form-control" type="number" name="thn_end" value="{{$filter['thn_end'] ?? date('Y')}}">
					</div>
				</div>
				<div class="col-lg-3 col-6 kt-margin-b-15-tablet-and-mobile">
					<label>Buku:</label>
					<select class="form-control form-control-sm apply-select2" name="buku[]" data-col-index="3" multiple  required>
						@foreach($buku as $row)
							<option value="{{$row->kd_buku}}" {{!isset($filter) || in_array($row->kd_buku, $filter['buku']) ? 'selected' : '' }} >{{$row->kd_buku}}</option>
						@endforeach
					</select>
				</div>
				
			</div>
			
			<div class="row kt-margin-b-20">
				
				<div class="col-lg-3 kt-margin-b-15-tablet-and-mobile">
					<label>Pencarian:</label>
					<input type="text" class="form-control form-control-sm kt-input" name="keyword" value="{{$filter['keyword'] ?? ''}}" >
				</div>
				
				<div class="col-lg-9 col-6 kt-margin-b-15-tablet-and-mobile pt-3">
					<button class="btn btn-brand btn-sm btn-brand--icon mt-3" id="kt_search" data-url="{{route('lap.tunggakan.filter')}}"><i class="la la-search"></i> Tampilkan</button>
					
					<button class="btn btn-success btn-sm btn-success--icon mt-3" id="kt_download" data-url="{{route('lap.tunggakan.pdf')}}" data-target="_blank"><i class="la la-download"></i> Download PDF</button>
				</div>
			</div>
			
		</form>
		
		<hr>
		
		<table class="table table-striped table-bordered table-hover table-checkable font-courier" id="table-main" data-url="{{route('kelurahan.datatable')}}">
			<thead>
				<tr>
					<th>NOP</th>
					<th>Nama WP</th>
					<th>Alamat WP</th>
					<th>Alamat OP</th>
					<th class="text-right">Pajak yang harus dibayar</th>
					<th class="text-right">Denda</th>
					<th class="text-center">Tahun Pajak</th>
				</tr>
			</thead>
			@if(isset($data) && $data)
			@php
				$tot_op = 0;
				$tot_tagih = 0;
				$tot_denda = 0;
			@endphp
			<tbody>
			@foreach($data as $rowWp)
				@php
					$tot_op += 1;
					$rowspan = count($rowWp) > 1 ? 'rowspan="'.count($rowWp).'"' : ''
				@endphp
				@foreach($rowWp as $i=>$rowWpPeriod)
					@php
						$dendaCalc = NumberHelper::calcFinePerMonths($rowWpPeriod->pbb_yg_harus_dibayar_sppt, $denda, $rowWpPeriod->bulan_telat);
						$tot_tagih += $rowWpPeriod->pbb_yg_harus_dibayar_sppt;
						$tot_denda += $dendaCalc;
					@endphp
					
					<tr class="tr-group-head" >
						@if($i == 0)
						<td {!! $rowspan ?: '' !!}>
							<a href="{{route('op.detail', ['id'=>$rowWpPeriod->kode_op])}}" data-toggle="modal" data-target="#main-modal-lg">
								{{$rowWpPeriod->kode_op}}
							</a>
						</td>
						<td {!! $rowspan ?: '' !!}>
							<a href="{{route('wp.detail', ['id'=>trim($rowWpPeriod->subjek_pajak_id)])}}" data-toggle="modal" data-target="#main-modal-lg">
								{{$rowWpPeriod->nm_wp_sppt}}
							</a>
						</td>
						<td {!! $rowspan ?: '' !!}>{{$rowWpPeriod->jln_wp_sppt}} {{$rowWpPeriod->blok_kav_no_wp_sppt}}</td>
						<td {!! $rowspan ?: '' !!}>{{$rowWpPeriod->jalan_op}} {{$rowWpPeriod->blok_kav_no_op}} {{$rowWpPeriod->nm_kelurahan}} {{$rowWpPeriod->nm_kecamatan}}</td>
						@endif
						<td class="text-right">{{NumberHelper::numberToCurrId($rowWpPeriod->pbb_yg_harus_dibayar_sppt)}}</td>
						<td class="text-right">{{NumberHelper::numberToCurrId($dendaCalc) }}</td>
						<td class="text-center">{{$rowWpPeriod->thn_pajak_sppt}}</td>
					</tr>
				@endforeach
			@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>{{$tot_op}} Obyek Pajak</th>
					<th></th>
					<th></th>
					<th>Total : </th>
					<th class="text-right">{{NumberHelper::numberToCurrId($tot_tagih) }}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($tot_denda) }}</th>
					<th class="text-center"></th>
				</tr>
			</tfoot>
			@endif
			
		</table>
		
		
		
	@endcomponent
	
@endsection

@push('css-plugin')
	<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endpush
@push('js-plugin')
	<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/bootstrap-datepicker/locales/bootstrap-datepicker.id.min.js') }}" type="text/javascript"></script>
	
	<script src="{{ asset('js/app/lap_tunggakan.js') }}" type="text/javascript"></script>
@endpush

@push('js-script')
<script>
	$('#table-main').tableTunggakan({
		domFormFilter: '[name="form-filter"]',
	});
</script>
@endpush