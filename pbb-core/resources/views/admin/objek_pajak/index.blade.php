@extends('template.template')

@section('title', 'Objek Pajak')
@section('subheader-title', 'Objek Pajak')
@section('subheader-subtitle', 'Objek Pajak')

@section('content')

	@component('template.component.portlet')
		@slot('icon', 'kt-font-brand fa fa-home')
		@slot('title', 'Daftar Objek Pajak')
		
		@slot('toolbar')
		@endslot
		
		<form method="POST" name="form-filter" action="">
			@csrf
			<div class="row kt-margin-b-20">
				<div class="col-lg-3 kt-margin-b-15-tablet-and-mobile">
					<label>Kecamatan Obyek Pajak:</label>
					<select class="form-control form-control-sm" name="kecamatan" option-required>
						<option value="">Pilih Kecamatan</option>
						@foreach($kecamatan as $row)
							<option value="{{$row->kd_kecamatan}}" {{isset($filter['kecamatan']) && $row->kd_kecamatan == $filter['kecamatan'] ? 'selected' : '' }} >{{$row->nm_kecamatan}}</option>
						@endforeach
					</select>
				</div>
				<div class="col-lg-3 kt-margin-b-15-tablet-and-mobile">
					<label>Kelurahan Obyek Pajak:</label>
					<select class="form-control form-control-sm" name="kelurahan" option-required>
						<option value="">Semua Kelurahan</option>
						@foreach($kecamatan as $kec) @foreach($kec->kelurahan as $row)
							<option value="{{$row->kd_kelurahan}}" data-kec="{{$kec->kd_kecamatan}}" {{isset($filter['kelurahan']) && $row->kd_kelurahan == $filter['kelurahan'] ? 'selected' : '' }} >{{$row->nm_kelurahan}}</option>
						@endforeach @endforeach
					</select>
				</div>
				
				<div class="col-lg-3 kt-margin-b-15-tablet-and-mobile">
					<label>Kata Kunci:</label>
					<input type="text" class="form-control form-control-sm kt-input" name="keyword" value="{{$filter['keyword'] ?? ''}}" option-required>
				</div>
				
				<div class="col-lg-3 col-6 kt-margin-b-15-tablet-and-mobile pt-3">
					<button class="btn btn-brand btn-sm btn-brand--icon mt-3" id="kt_search" data-url="{{route('op.filter')}}"><i class="la la-search"></i> Tampilkan</button>
				</div>
				
			</div>
		</form>
		
		@if($filter)
		<table class="table table-striped table-bordered table-hover table-checkable" id="datatable-main" data-url="{{route('op.datatable', ['encodedFilter'=>$encodedFilter])}}">
			<thead>
				<tr>
					<th width="100" data-name="nop" width="100">NOP</th>
					<th data-name="nm_kecamatan">Kecamatan</th>
					<th data-name="nm_kelurahan">Kelurahan</th>
					<th data-name="alamat_op">Alamat</th>
					<th data-name="nm_wp">Subjek Pajak</th>
					<th width="70" data-name="total_luas_bng" data-class="text-right">Luas Bangunan</th>
					<th width="70" data-name="total_luas_bumi" data-class="text-right">Luas Bumi</th>
					<th data-type='options' data-id="nop" width="50">Detail</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		@endif

	@endcomponent
	
@endsection

@push('js-plugin')
	<script src="{{ asset('js/app/filter.js') }}" type="text/javascript"></script>
@endpush

@component('template.component.datatable')
<script>
	$('#datatable-main').filterTable({
		domFormFilter: '[name="form-filter"]',
	});

	@if($filter)
	
	var panel = $('#datatable-main').parents('.kt-portlet');
	KTApp.block(panel, {
		overlayColor: '#000000',
		type: 'v2',
		state: 'primary',
		message: 'Memproses data...',
	});
	
	$('#datatable-main').customDataTable(
	{
		order: [[1,'asc']],
		//searching: false,
		optionsRenderer : function (id, type, row, meta)
		{
			return '<a class="btn btn-xs btn-brand" href="{{route('op.detail')}}/'+id+'" data-toggle="modal" data-target="#main-modal-lg"><i class="la la-clipboard p-0"></i></a>';
		},
		initComplete: function( settings ) {
      KTApp.unblock(panel);
    }

	});
	@endif
</script>
@endcomponent