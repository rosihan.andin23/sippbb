@extends('template.modal')

@section('modal-title')
	<i class="la la-clipboard"></i> Objek Pajak - {{$data->objekSubjekPajak->alamat_wp}} - {{$id}}
@endsection

@section('modal-content')

<table class="table table-striped">
	<tr>
		<th width="180">NOP</th>
		<td>{{$data->objekSubjekPajak->nop}}</td>
		<th width="150">Nama Pemilik</th>
		<td>{{$data->objekSubjekPajak->nm_wp}}</td>
	</tr>
	<tr>
		<th>Alamat Objek Pajak</th>
		<td>{{$data->objekSubjekPajak->alamat_op}}</td>
		<th>Alamat Pemilik</th>
		<td>{{$data->objekSubjekPajak->alamat_wp}}</td>
	</tr>
	<tr>
		<th>Kecamatan</th>
		<td>{{$data->objekSubjekPajak->nm_kecamatan}}</td>
		<th>Telp</th>
		<td>{{$data->objekSubjekPajak->telp_wp}}</td>
	</tr>
	<tr>
		<th>Kelurahan</th>
		<td>{{$data->objekSubjekPajak->nm_kelurahan}}</td>
		<th>NPWP</th>
		<td>{{$data->objekSubjekPajak->npwp}}</td>
	</tr>
	<tr>
		<th>Luas Bangunan</th>
		<td>{{number_format($data->objekSubjekPajak->total_luas_bng, 2, ',', '.')}} m<sup>3</sup></td>
		<th>Luas Bumi</th>
		<td>{{number_format($data->objekSubjekPajak->total_luas_bumi, 2, ',', '.')}} m<sup>3</sup></td>
	</tr>
</table>

<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="#op_tab_op">
			<i class="la la-home"></i> SPPT
		</a>
	</li>
	<li class="nav-item" id="wp-nav-tunggakan">
		<a class="nav-link kt-font-danger" data-toggle="tab" href="#op_tab_tunggakan">
			<i class="la la-exclamation-triangle kt-font-danger"></i> Tunggakan
		</a>
	</li>

</ul>
<div class="tab-content">

	<div class="tab-pane active" id="op_tab_op" role="tabpanel">
		<table class="table table-bordered table-striped table-small">
			<thead>
				<tr>
					<th width="10">#</th>
					<th width="70" class="text-center">Tahun</th>
					<th>Kode SPPT</th>
					<th width="110" class="text-right">Harus Bayar</th>
					<th width="110" class="text-right">Sudah Bayar</th>
					<th width="80" class="text-center">Denda</th>
					<th width="110" class="text-center">Total Kurang</th>
					<th width="110" class="text-center">Jatuh Tempo</th>
					<th width="120" class="text-center">Status Bayar</th>
					<th width="100" class="text-center">Tgl Terbit</th>
				</tr>
			</thead>
			<tbody>
			@php
				$statusBayar = [
					'0' => '<span class="kt-badge kt-badge--inline kt-badge--danger">Belum Lunas</span>',
					'1' => '<span class="kt-badge kt-badge--inline kt-badge--success">Lunas</span>',
				];
			@endphp
			@if($data->sppt) @foreach($data->sppt as $j=>$sppt)
				@php
					if($sppt->status_pembayaran_sppt == 0) $sppt->denda = NumberHelper::calcFinePerMonths($sppt->pbb_yg_harus_dibayar_sppt, $denda, $sppt->bulan_telat)
				@endphp
				<tr>
					<td class="text-center">{{$j+1}}</td>
					<td class="text-center">{{$sppt->thn_pajak_sppt}}</td>
					<td>{{$sppt->kode_sppt}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($sppt->pbb_yg_harus_dibayar_sppt)}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($sppt->jml_sppt_yg_dibayar)}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($sppt->denda)}}</td>
					<td class="text-right">{{ $sppt->status_pembayaran_sppt == 0 ? NumberHelper::numberToCurrId($sppt->denda + $sppt->pbb_yg_harus_dibayar_sppt - $sppt->jml_sppt_yg_dibayar) : '-'}}</td>
					<td class="text-center">{{date('d/m/Y', strtotime($sppt->tgl_jatuh_tempo_sppt))}}</td>
					<td class="text-center">{!!$statusBayar[$sppt->status_pembayaran_sppt] ?? ''!!}</td>
					<td class="text-center">{{date('d/m/Y', strtotime($sppt->tgl_terbit_sppt))}}</td>
				</tr>
			@endforeach @endif	
			</tbody>
		</table>
	
	</div>
	
	<div class="tab-pane" id="op_tab_tunggakan" role="tabpanel">
		<table class="table table-bordered table-striped" id="tb-tunggakan">
			<thead>
				<tr>
					<th width="10">#</th>
					<th>NOP</th>
					<th width="70" class="text-center">Tahun</th>
					<th width="110" class="text-right">Harus Bayar</th>
					<th width="110" class="text-right">Sudah Bayar</th>
					<th width="80" class="text-center">Denda</th>
					<th width="110" class="text-center">Total Kurang</th>
					<th width="110" class="text-center">Jatuh Tempo</th>
					<th width="100" class="text-center">Tgl Terbit</th>
				</tr>
			</thead>
			<tbody>
			@php
				$totalPokok = 0;
				$totalTunggakan = 0;
				$totalBayar = 0;
				$totalDenda = 0;
				$j=0;
			@endphp
			@if($data->sppt) @foreach($data->sppt as $sppt) @if($sppt->status_pembayaran_sppt == 0)
				@php
					$totalPokok += $sppt->pbb_yg_harus_dibayar_sppt;
					$totalDenda += $sppt->denda;
					$totalBayar += $sppt->jml_sppt_yg_dibayar;
					$totalTunggakan += $sppt->denda + $sppt->pbb_yg_harus_dibayar_sppt - $sppt->jml_sppt_yg_dibayar;
				@endphp
				<tr>
					<td class="text-center">{{++$j}}</td>
					<td>{{$sppt->nop}}</td>
					<td class="text-center">{{$sppt->thn_pajak_sppt}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($sppt->pbb_yg_harus_dibayar_sppt)}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($sppt->jml_sppt_yg_dibayar)}}</td>
					<td class="text-right">{{NumberHelper::numberToCurrId($sppt->denda)}}</td>
					<td class="text-right">{{ $sppt->status_pembayaran_sppt == 0 ? NumberHelper::numberToCurrId($sppt->denda + $sppt->pbb_yg_harus_dibayar_sppt - $sppt->jml_sppt_yg_dibayar) : '-'}}</td>
					<td class="text-center">{{date('d/m/Y', strtotime($sppt->tgl_jatuh_tempo_sppt))}}</td>
					<td class="text-center">{{date('d/m/Y', strtotime($sppt->tgl_terbit_sppt))}}</td>
				</tr>
			@endif @endforeach @endif	
			</tbody>
			<tfoot>
				<tr>
					<th class="text-right" colspan="3">Total</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($totalPokok)}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($totalBayar)}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($totalDenda)}}</th>
					<th class="text-right">{{NumberHelper::numberToCurrId($totalTunggakan)}}</th>
					<th colspan="2"></th>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
	
@endsection

@push('js')
<script>
	
	var parent = $('.modal-content');
	$('[target-child]', parent).click(function(){
		var target = $(this).attr('target-child');
		target = $('[child="'+target+'"]', parent);
		console.log(target)
		if(!target.length) return false;
		console.log(target.is(':visible'))
		if(target.is(':visible'))
		{
			target.hide();
		}
		else
		{
			target.show();
		}
	})
	if(!$('#tb-tunggakan tbody tr', parent).length) $('#wp-nav-tunggakan a, #wp-nav-tunggakan a i').removeClass('kt-font-danger').addClass('kt-font-success')
</script>
@endpush