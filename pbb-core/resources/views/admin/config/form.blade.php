@extends('template.modal')

@section('modal-title', 'Ubah Pengaturan')

@section('modal-content')
<form method="POST" action="{{route('config.store', ['id'=>$id])}}" class="config-form" ajax>
	@csrf
	
	@if($data->conf_type == 'number')
		@component('template.component.form.input')
			@slot('label', $data->conf_name)
			@slot('name', 'conf_value')
			@slot('type', 'number')
			{{$data ? $data->conf_value : old('conf_value')}}
		@endcomponent
		
	@elseif($data->conf_type == 'html')
		@component('template.component.form.textarea')
			@slot('label', $data->conf_name)
			@slot('name', 'conf_value')
			@slot('class', 'summernote')
			{{$data ? $data->conf_value : old('conf_value')}}
		@endcomponent
		
		
	@else
		@component('template.component.form.input')
			@slot('label', $data->conf_name)
			@slot('name', 'conf_value')
			{{$data ? $data->conf_value : old('conf_value')}}
		@endcomponent
	@endif
</form>	
@endsection

@section('modal-button')
	<button type="button" class="btn btn-primary" data-toggle="modal-submit" >Simpan</button>
@endsection

@push('js')
<script>
	$('.config-form .summernote').summernote({
		height: 200,
		lang: 'id-ID',
		toolbar: [
		['misc', ['codeview','fullscreen']],
    ['style', ['bold', 'italic', 'underline','strikethrough', 'clear']],
    ['font', ['superscript', 'subscript', 'color', 'fontsize']],
    ['para', ['ul', 'ol']],
		['insert', ['link','hr','table']],
		
  ]
	});
</script>
@endpush