@extends('template.template')

@section('title', 'Pengaturan')
@section('subheader-title', 'Pengaturan')
@section('subheader-subtitle', 'Pengaturan')

@section('content')

	@component('template.component.portlet')
		@slot('icon', 'kt-font-brand fa fa-cog')
		@slot('title', 'Daftar Item Pengaturan')

		<table class="table table-striped table-bordered table-hover table-checkable" id="datatable-main" data-url="{{route('config.datatable')}}">
			<thead>
				<tr>
					<th data-type='#' width="10">No.</th>
					<th data-name="conf_name" width="230">Nama Item</th>
					<th data-name="conf_value">Isian</th>
					<th data-type='options' data-id="conf_code" width="50">Ubah</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	@endcomponent
	
@endsection


@push('css-plugin')
<link href="{{ asset('plugins/summernote/dist/summernote.css') }}" rel="stylesheet" type="text/css" />
@endpush

@push('js-plugin')
<script src="{{ asset('plugins/summernote/dist/summernote.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/summernote/dist/lang/summernote-id-ID.min.js') }}" type="text/javascript"></script>
@endpush

@component('template.component.datatable')
<script>
$('#datatable-main').customDataTable(
{
	optionsRenderer : function (id, type, row, meta)
	{
		var modalSize = row.conf_type == 'html' ? 'main-modal-lg' : 'main-modal-md';
		return '<a class="btn btn-brand btn-xs" href="{{route('config.form')}}/'+id+'" data-toggle="modal" data-target="#'+modalSize+'"><i class="la la-edit kt-padding-r-0"></i></a>';
	},
	postConfig: function(configs)
	{
		configs.columnDefs.push({
			targets: 2, 
			render: function(data, type, row, meta)
			{
				var str = data.replace(/(<([^>]+)>)/ig,"");
				return str.length > 150 ? str.substr(0, 150)+'...' : str;
			}
		});
		return configs;
	}
});
</script>
@endcomponent