@extends('template.template')

@section('title', 'Referensi Tarif')
@section('subheader-title', 'Referensi')
@section('subheader-subtitle', 'Tarif')

@section('content')

	@component('template.component.portlet')
		@slot('icon', 'kt-font-brand fa fa-money-bill')
		@slot('title', 'Daftar Tarif')
		
		@slot('toolbar')
		@endslot

		<table class="table table-striped table-bordered table-hover table-checkable" id="datatable-main" data-url="{{route('tarif.datatable')}}">
			<thead>
				<tr>
					<th data-type="#" width="30">No.</th>
					<th data-name="kd_dati2">Kode Kota</th>
					<th data-name="thn_awal">Tahun</th>
					<th data-name="njop_min" data-class="text-center">Nilai NJOP</th>
					<th data-name="nilai_tarif">Nilai %</th>
					<th data-type='options' data-id="kd_buku" width="50">Opsi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	@endcomponent
	
@endsection

@component('template.component.datatable')
<script>
$('#datatable-main').customDataTable(
{
	optionsRenderer : function (id, type, row, meta)
	{
		return ' <span class="dropdown">'+
			'<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">'+
				'<i class="la la-gear"></i>'+
			'</a>'+
			'<div class="dropdown-menu dropdown-menu-right">'+
				'<a class="dropdown-item" href="#" data-toggle="modal" data-target="#main-modal-md"><i class="la la-edit"></i> Ubah data</a>'+
			'</div>'+
		'</span>';
	},
	postConfig: function(configs)
	{
		configs.columnDefs.push({
			targets: 1, 
			render: function(data, type, row, meta){ return row.kd_propinsi.toString()+row.kd_dati2.toString(); },
		});
		configs.columnDefs.push({
			targets: 2, 
			render: function(data, type, row, meta){ return row.thn_awal+' s/d '+row.thn_akhir; },
		});
		configs.columnDefs.push({
			targets: 3, 
			render: function(data, type, row, meta){ 
				return row.njop_min.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+' s/d '+row.njop_max.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");  
			},			
		});
		return configs;
	}
});
</script>
@endcomponent