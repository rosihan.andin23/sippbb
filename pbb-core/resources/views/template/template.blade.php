<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>@yield('title') | PBB-P2</title>
	<meta name="description" content="Dashboard">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
			},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
	
	<link href="{{ asset('plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" type="text/css" />
	
	<link href="{{ asset('plugins/animate.css/animate.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('plugins/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('plugins/line-awesome/css/line-awesome.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('plugins/flaticon/flaticon.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('plugins/flaticon2/flaticon.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('plugins/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
	@stack('css-plugin')
	
	<link href="{{ asset('css/template.bundle.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('css/global.custom.css') }}" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" />
	@stack('css-script')
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

	<!-- begin:: Page -->

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
		<div class="kt-header-mobile__logo">
			<a href="demo3/index.html">
				<img alt="Logo" src="{{ asset('img/logo.png') }}" height="40"/>
				PBB-P2 Kab Ngawi
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-toggler kt-hidden" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>

	<!-- end:: Header Mobile -->
	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

			<!-- begin:: Aside -->
			<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
			<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

				<!-- begin:: Aside -->
				<div class="kt-aside__brand kt-grid__item  " id="kt_aside_brand">
					<div class="kt-aside__brand-logo">
						<a href="#">
							<img alt="Logo" src="{{ asset('img/logo.png') }}" height="80" class="pt-3"/>
						</a>
					</div>
				</div>

				<!-- end:: Aside -->

				<!-- begin:: Aside Menu -->
				<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
					<div id="kt_aside_menu" class="kt-aside-menu  kt-aside-menu--dropdown " data-ktmenu-vertical="1" data-ktmenu-dropdown="1" data-ktmenu-scroll="0">
						@if(Auth::guard('admin')->check())
							@component('template.nav.admin')
							@endcomponent
						@else
							@component('template.nav.kecamatan')
							@endcomponent
						@endif
					</div>
				</div>

				<!-- end:: Aside Menu -->
			</div>

			<!-- end:: Aside -->
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

				<!-- begin:: Header -->
				<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

					<!-- begin: Header Menu -->
					<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
					<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
						<h2 class="pt-3 mb-0">PBB-P2</h2>
						<h4 class="pt-0 fw-100">BK Kabupaten Ngawi</h4>
					</div>

					<!-- end: Header Menu -->

					<!-- begin:: Header Topbar -->
					<div class="kt-header__topbar">

						<!--begin: Search -->
						<div class="kt-header__topbar-item kt-header__topbar-item--search dropdown" id="kt_quick_search_toggle">
							<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
								<span class="kt-header__topbar-icon"><i class="flaticon2-search-1"></i></span>
							</div>
							<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-top-unround dropdown-menu-anim dropdown-menu-lg">
								<div class="kt-quick-search kt-quick-search--inline" id="kt_quick_search_inlines">
									<form method="POST" class="kt-quick-search__form" action="{{route('wp.filter')}}">
										@csrf
										<div class="input-group">
											<div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
											<input type="text" name="keyword" class="form-control kt-quick-search__input" placeholder="Nama, alamat atau No Subjek Pajak">
											<div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
										</div>
									</form>
									<div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
									</div>
								</div>
							</div>
						</div>
						<!--end: Search -->

						<!--begin: User Bar -->
						<div class="kt-header__topbar-item kt-header__topbar-item--user">
							<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
								<div class="kt-header__topbar-user">
									<span class="kt-hidden kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
									<span class="kt-hidden kt-header__topbar-username kt-hidden-mobile">{{ Auth::user()->name }}</span>
									<img alt="Pic" src="{{ asset('img/user.jpg') }}" />
								</div>
							</div>
							<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

								<!--begin: Head -->
								<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url({{ asset('media/misc/bg-1.jpg') }})">
									<div class="kt-user-card__avatar">
										<img alt="Pic" src="{{ asset('img/user.jpg') }}" />
									</div>
									<div class="kt-user-card__name">
										{{ Auth::user()->name }}
										@if(Auth::guard('admin')->check())
											<div class="kt-label-font-color-2">Administrator</div>
										@else
											<div class="kt-label-font-color-2">Kecamatan</div>
										@endif
									</div>
								</div>

								<!--end: Head -->

								<!--begin: Navigation -->
								<div class="kt-notification">
									<a href="{{route('profile')}}" class="kt-notification__item" data-toggle="modal" data-target="#main-modal-md">
										<div class="kt-notification__item-icon">
											<i class="flaticon2-calendar-3 kt-font-success"></i>
										</div>
										<div class="kt-notification__item-details">
											<div class="kt-notification__item-title kt-font-bold">
												Profil Saya
											</div>
											<div class="kt-notification__item-time">
												Pengaturan Akun
											</div>
										</div>
									</a>
									<div class="kt-notification__custom kt-space-between">
										<a href="{{route('auth.logout')}}" class="btn btn-label btn-label-brand btn-sm btn-bold">Keluar</a>
									</div>
								</div>

								<!--end: Navigation -->
							</div>
						</div>

						<!--end: User Bar -->
					</div>

					<!-- end:: Header Topbar -->
				</div>

				<!-- end:: Header -->
				<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

					<!-- begin:: Content Head -->
					<div class="kt-subheader  kt-grid__item" id="kt_subheader">
						<div class="kt-container  kt-container--fluid ">
							
							<div class="kt-subheader__main">
								<h3 class="kt-subheader__title">@yield('subheader-title')</h3>
								<span class="kt-subheader__separator kt-subheader__separator--v"></span>
								<span class="kt-subheader__desc">@yield('subheader-subtitle')</span>
							</div>
							<div class="kt-subheader__toolbar">
								<div class="kt-subheader__wrapper">
									@yield('subheader-toolbar')
								</div>
							</div>
						</div>
					</div>

					<!-- end:: Content Head -->

					<!-- begin:: Content -->
					<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
					<!--Begin::Row-->
						
						@yield('content')
						
					<!--End::Row-->
					</div>

					<!-- end:: Content -->
				</div>

				<!-- begin:: Footer -->
				<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
					<div class="kt-container  kt-container--fluid ">
						<div class="kt-footer__copyright">
						{{ date('Y')}}&nbsp;&copy;&nbsp;<a href="#" target="_blank" class="kt-link">Badan Keuangan Kabupaten Ngawi</a>
						</div>
						<div class="kt-footer__menu">
							<a href="#" target="_blank" class="kt-footer__menu-link kt-link">Bantuan</a>
							<a href="#" target="_blank" class="kt-footer__menu-link kt-link">Kominfo Ngawi</a>
						</div>
					</div>
				</div>

				<!-- end:: Footer -->
			</div>
		</div>
	</div>

	<!-- end:: Page -->

	<!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->
	
	<!-- begin::Modal -->
	<div id="main-modal-lg" class="modal fade main-modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content"></div>
		</div>
	</div>
	<div id="main-modal-md" class="modal fade main-modal">
		<div class="modal-dialog">
			<div class="modal-content"></div>
		</div>
	</div>
	<!-- end::Modal -->

	@stack('content-plugin')

	<!-- begin::Global Config(global config for global JS sciprts) -->
	<script>
		var KTAppOptions = {
			"colors": {
				"state": {
					"brand": "#2c77f4",
					"light": "#ffffff",
					"dark": "#282a3c",
					"primary": "#5867dd",
					"success": "#34bfa3",
					"info": "#36a3f7",
					"warning": "#ffb822",
					"danger": "#fd3995"
				},
				"base": {
					"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
					"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
				}
			}
		};
	</script>


	<script src="{{ asset('plugins/jquery/dist/jquery.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/popper.js/dist/umd/popper.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/js-cookie/src/js.cookie.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/moment/min/moment.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/tooltip.js/dist/umd/tooltip.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/perfect-scrollbar/dist/perfect-scrollbar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/sticky-js/dist/sticky.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/wnumb/wNumb.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>
	
	<script src="{{ asset('plugins/jquery-form/dist/jquery.form.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/block-ui/jquery.blockUI.js') }}" type="text/javascript"></script>
	<script src="{{ asset('plugins/toastr/build/toastr.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/template.bundle.modified.js') }}" type="text/javascript"></script>
	@stack('js-plugin')
	
	<script src="{{ asset('js/global.custom.js') }}" type="text/javascript"></script>
	
	@stack('js-script')
</body>

<!-- end::Body -->
</html>