<ul class="kt-menu__nav ">
	<li class="kt-menu__item" aria-haspopup="true"><a href="{{route('dashboard')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-dashboard"></i><span class="kt-menu__link-text">Dasbor</span></a></li>
	<li class="kt-menu__item" aria-haspopup="true"><a href="{{route('wp')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-users"></i><span class="kt-menu__link-text text-center">Subjek Pajak</span></a></li>
	<li class="kt-menu__item" aria-haspopup="true"><a href="{{route('op')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-home"></i><span class="kt-menu__link-text text-center">Objek Pajak</span></a></li>
	
	<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-file-2"></i><span class="kt-menu__link-text">Laporan</span></a>
		<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
			<ul class="kt-menu__subnav">
				<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Laporan</span></span></li>
				<li class="kt-menu__item " aria-haspopup="true"><a href="{{route('lap.penerimaan')}}" class="kt-menu__link "><span class="kt-menu__link-text"><i class="fa fa-copy mr-2"></i> Realisasi Penerimaan</span></a></li>
				<li class="kt-menu__item " aria-haspopup="true"><a href="{{route('lap.tunggakan')}}" class="kt-menu__link "><span class="kt-menu__link-text"><i class="fa fa-exclamation-triangle mr-2"></i> Data Belum Bayar</span></a></li>
			</ul>
		</div>
	</li>
	
</ul>
