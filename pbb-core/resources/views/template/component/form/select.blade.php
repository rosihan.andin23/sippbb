<div class="form-group">
	<label>{{$label ?? (isset($name) ? ucwords(str_replace(['_','-'], ' ', $name)) : '') }}</label>
	<select name="{{$name ?? ''}}" class="form-control {{$class?? ''}}" {!!$attr ?? ''!!}>
		<option>-- Pilih {{$label ?? (isset($name) ? ucwords(str_replace(['_','-'], ' ', $name)) : '') }} --</option>
		@foreach($data as $row)
		<option value="{{$row->$dataValue}}" {{(is_array($value) && in_array($row->$dataValue,$value)) || $row->$dataValue == $value ? 'selected' : ''}}  >{{$row->$dataText}}</option>
		@endforeach
	</select>
	@if(isset($note))
	<span class="form-text text-muted">{{$note ?? ''}}</span>
	@endif
</div>