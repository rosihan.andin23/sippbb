<div class="form-group">
	<label>{{$label ?? (isset($name) ? ucwords(str_replace(['_','-'], ' ', $name)) : '') }}</label>
	<textarea name="{{$name ?? ''}}" class="form-control {{$class?? ''}}" rows="{{$rows ?? 5}}" placeholder="{{$placeholder?? ''}}" {!!$attr ?? ''!!}>{{$slot}}</textarea>
	@if(isset($note))
	<span class="form-text text-muted">{{$note ?? ''}}</span>
	@endif
</div>