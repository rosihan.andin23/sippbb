<div class="form-group">
	<label>{{$label ?? (isset($name) ? ucwords(str_replace(['_','-'], ' ', $name)) : '') }}</label>
	<input type="{{$type ?? ''}}" name="{{$name ?? ''}}" class="form-control {{$class?? ''}}" placeholder="{{$placeholder?? ''}}" {!!$attr ?? ''!!} value="{{$slot}}">
	@if(isset($note))
	<span class="form-text text-muted">{{$note ?? ''}}</span>
	@endif
</div>