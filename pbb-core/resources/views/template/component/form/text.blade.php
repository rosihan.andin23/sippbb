<div class="form-group">
	<label>{{$label ?? (isset($name) ? ucwords(str_replace(['_','-'], ' ', $name)) : '') }}</label>
	<div class="form-control-static">{{$slot}}</div>
	@if(isset($note))
	<span class="form-text text-muted">{{$note ?? ''}}</span>
	@endif
</div>