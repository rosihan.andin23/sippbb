@push('content-plugin')
<div id="{{$id}}" class="modal fade main-modal">
	<div class="modal-dialog {{$size}}">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" >{{$title}}</h4>
			</div>
			<div class="modal-body">
			{{$slot}}
			</div>
			<div class="modal-footer">
				{{$button}}
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>
@endpush