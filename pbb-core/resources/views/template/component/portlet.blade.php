<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="{{$icon ?? ''}}"></i>				
			</span>
			<h3 class="kt-portlet__head-title">{{$title ?? ''}}</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
				{{$toolbar ?? ''}}
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">
	{{$slot}}
	</div>
</div>