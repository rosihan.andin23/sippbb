@push('css-plugin')
<link href="{{ asset('plugins/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endpush

@push('js-plugin')
<script src="{{ asset('plugins/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/datatables.custom.js') }}" type="text/javascript"></script>
@endpush

@push('js-script')
{{$slot}}
@endpush