@if(request()->ajax())
	
	<div class="modal-header">
		<h4 class="modal-title" >@yield('modal-title')</h4>
		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	</div>

	<div class="modal-body">
	@yield('modal-content')
	</div>

	<div class="modal-footer">
		@yield('modal-button')
		<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	</div>

	@stack('js')
	
@else
	
	@extends(!request()->ajax() ? 'template.template' : 'template.blank')
	
	@section('subheader-title')
		@yield('modal-title')
	@endsection
		
	@section('content')

		@component('template.component.portlet')
			
			@slot('title')
				@yield('modal-title')
			@endslot
			
			@yield('modal-content')
			
			<div class="modal-options mt-2">
				@yield('modal-button')
			</div>
		@endcomponent
		
	@endsection
	
	@push('css-script')
		@stack('css')
	@endpush
	
	@push('js-script')
		@stack('js')
	@endpush

	@stack('css')

@endif