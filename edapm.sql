-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 18, 2021 at 04:17 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edapm`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE `access` (
  `id` int(11) NOT NULL,
  `access_control_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_control`
--

CREATE TABLE `access_control` (
  `id` int(11) NOT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `active_pinjaman_kelompok`
--

CREATE TABLE `active_pinjaman_kelompok` (
  `loan_id` int(20) NOT NULL,
  `tgl_aktif` date NOT NULL,
  `alokasi_dana` varchar(50) NOT NULL,
  `prosentase_jasa_active` int(100) NOT NULL,
  `jenis_jasa_active` int(50) NOT NULL,
  `jangka_bulan_active` varchar(50) NOT NULL,
  `sistem_angsuran_active` varchar(100) NOT NULL,
  `lunas` smallint(100) NOT NULL,
  `spk_kelompok` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `ip` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `get` longtext DEFAULT NULL,
  `post` longtext DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `angsuran`
--

CREATE TABLE `angsuran` (
  `id_angsuran` int(11) NOT NULL,
  `id_transaksi` int(25) NOT NULL,
  `loan_id` int(25) NOT NULL,
  `pokok` decimal(50,0) NOT NULL,
  `jasa` decimal(50,0) NOT NULL,
  `denda` decimal(50,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `desa`
--

CREATE TABLE `desa` (
  `id_desa` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `nama_desa` varchar(30) NOT NULL,
  `telepon_kades` varchar(20) NOT NULL,
  `nip_kades` varchar(30) NOT NULL,
  `nama_sekdes` varchar(30) NOT NULL,
  `telepon_sekdes` varchar(20) NOT NULL,
  `alamat_kantor_desa` varchar(250) NOT NULL,
  `telepon_kantor` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jaminan`
--

CREATE TABLE `jaminan` (
  `id_jaminan` int(25) NOT NULL,
  `jenis_jaminan` varchar(50) NOT NULL,
  `nomor_kepemilikan` int(100) NOT NULL,
  `merk_tipe` int(50) NOT NULL,
  `alamat_jaminan` varchar(50) NOT NULL,
  `nilai jaminan` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_transaksi`
--

CREATE TABLE `jurnal_transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `rek_debit` int(100) NOT NULL,
  `rek_kredit` int(100) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `tgl_transaksi` varchar(50) NOT NULL,
  `jumlah` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_kecamatan` int(11) NOT NULL,
  `nama_kecamatan` varchar(50) NOT NULL,
  `nama_camat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kelompok_peminjam`
--

CREATE TABLE `kelompok_peminjam` (
  `kd_kelompok` varchar(10) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `id_desa` int(30) NOT NULL,
  `nama_kelompok` varchar(50) NOT NULL,
  `alamat_kelompok` text NOT NULL,
  `telepon_kelompok` varchar(50) NOT NULL,
  `tanggal_berdiri` date NOT NULL,
  `nama_ketua_kel` varchar(50) NOT NULL,
  `nama_sekretaris_kel` varchar(50) NOT NULL,
  `nama_bendahara` varchar(50) NOT NULL,
  `jenis_kegiatan` varchar(50) NOT NULL,
  `tingkat_kelompok` varchar(50) NOT NULL,
  `fungsi_kelompok` varchar(50) NOT NULL,
  `jenis_usaha_kelompok` varchar(50) NOT NULL,
  `status_kelompok` varchar(50) NOT NULL,
  `produk_pinjaman` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kunjungan_kelompok`
--

CREATE TABLE `kunjungan_kelompok` (
  `id_kunjungan` int(30) NOT NULL,
  `kd_kelompok` varchar(10) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `tanggal_kunjungan` date NOT NULL,
  `hasil_kunjungan` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id_member` int(11) NOT NULL,
  `id_jaminan` int(11) NOT NULL,
  `nik_penjamin` int(16) NOT NULL,
  `nik_member` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `nama_member` varchar(100) NOT NULL,
  `jk_member` varchar(50) NOT NULL,
  `tempat_lahir_member` varchar(50) NOT NULL,
  `tanggal_lahir_member` varchar(100) NOT NULL,
  `alamat_ktp` varchar(100) NOT NULL,
  `no_kk` varchar(50) NOT NULL,
  `url_foto` varchar(50) NOT NULL,
  `no_hp_member` varchar(100) NOT NULL,
  `jenis_usaha_member` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pemanfaat`
--

CREATE TABLE `pemanfaat` (
  `kd_kelompok` varchar(10) NOT NULL,
  `id_member` int(25) NOT NULL,
  `alokasi_pemanfaat` decimal(50,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `penjamin`
--

CREATE TABLE `penjamin` (
  `nik_penjamin` int(25) NOT NULL,
  `nama_lengkap_penjamin` varchar(50) NOT NULL,
  `hubungan_penjamin` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `person_pegawai`
--

CREATE TABLE `person_pegawai` (
  `nip` varchar(30) NOT NULL,
  `USERNAME` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `unit` varchar(100) NOT NULL,
  `jabatan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pinjaman_kelompok`
--

CREATE TABLE `pinjaman_kelompok` (
  `loan_id` int(11) NOT NULL,
  `tgl_pengajuan` date NOT NULL,
  `nilai_pengajuan` int(100) NOT NULL,
  `prosentase_jasa_kelompok` int(50) NOT NULL,
  `jangka_bulan_kelompok` varchar(50) NOT NULL,
  `jenis_jasa` varchar(100) NOT NULL,
  `sistem_angsuran_pokok` varchar(50) NOT NULL,
  `note_pinjaman_kelompok` varchar(250) NOT NULL,
  `state_pinjaman_kelompok` varchar(100) NOT NULL,
  `jenis_produk_pinjaman_kelompok` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `rekening_akun`
--

CREATE TABLE `rekening_akun` (
  `id_rekening` int(11) NOT NULL,
  `parent_rekening` int(11) NOT NULL,
  `rek_debit` int(11) NOT NULL,
  `rek_kredit` int(11) NOT NULL,
  `nama_rekening` varchar(50) NOT NULL,
  `kode_rekening` int(100) NOT NULL,
  `level_rekening` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `saldo_bulan_berjalan`
--

CREATE TABLE `saldo_bulan_berjalan` (
  `id` int(11) NOT NULL,
  `id_rekening` int(25) NOT NULL,
  `bulan` int(11) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `total_debit` decimal(10,0) DEFAULT NULL,
  `total_kredit` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `verified_pinjaman_kelompok`
--

CREATE TABLE `verified_pinjaman_kelompok` (
  `loan_id` int(20) NOT NULL,
  `tgl_verified` date NOT NULL,
  `alokasi_dana_verified` varchar(50) NOT NULL,
  `prosentase_jasa_verified` int(100) NOT NULL,
  `jenis_jasa_kelompok_verified` int(50) NOT NULL,
  `jangka_kelompok_verified` varchar(50) NOT NULL,
  `sistem_angsuran_verified` varchar(100) NOT NULL,
  `jenis_pinjaman_verified` varchar(100) NOT NULL,
  `to_waiting_list` smallint(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `waiting_pinjaman_kelompok`
--

CREATE TABLE `waiting_pinjaman_kelompok` (
  `loan_id` int(20) NOT NULL,
  `tgl_waiting` date NOT NULL,
  `alokasi_dana_waiting` varchar(50) NOT NULL,
  `prosentase_jasa_waiting` int(100) NOT NULL,
  `jenis_jasa_kelompok_waiting` int(50) NOT NULL,
  `jangka_kelompok_waiting` varchar(50) NOT NULL,
  `sistem_angsuran_kelompok` varchar(100) NOT NULL,
  `activated` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `active_pinjaman_kelompok`
--
ALTER TABLE `active_pinjaman_kelompok`
  ADD PRIMARY KEY (`loan_id`),
  ADD KEY `loan_id` (`loan_id`);

--
-- Indexes for table `angsuran`
--
ALTER TABLE `angsuran`
  ADD PRIMARY KEY (`id_angsuran`),
  ADD KEY `id_transaksi` (`id_transaksi`),
  ADD KEY `loan_id` (`loan_id`);

--
-- Indexes for table `desa`
--
ALTER TABLE `desa`
  ADD PRIMARY KEY (`id_desa`),
  ADD KEY `username` (`username`,`id_kecamatan`),
  ADD KEY `id_kecamatan` (`id_kecamatan`);

--
-- Indexes for table `jaminan`
--
ALTER TABLE `jaminan`
  ADD PRIMARY KEY (`id_jaminan`),
  ADD KEY `id_jaminan` (`id_jaminan`);

--
-- Indexes for table `jurnal_transaksi`
--
ALTER TABLE `jurnal_transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `rek_debit` (`rek_debit`),
  ADD KEY `rek_kredit` (`rek_kredit`),
  ADD KEY `nip` (`nip`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `kelompok_peminjam`
--
ALTER TABLE `kelompok_peminjam`
  ADD PRIMARY KEY (`kd_kelompok`),
  ADD KEY `nip` (`nip`),
  ADD KEY `username` (`username`),
  ADD KEY `id_desa` (`id_desa`);

--
-- Indexes for table `kunjungan_kelompok`
--
ALTER TABLE `kunjungan_kelompok`
  ADD PRIMARY KEY (`id_kunjungan`),
  ADD KEY `nip` (`nip`),
  ADD KEY `kd_kelompok` (`kd_kelompok`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`),
  ADD KEY `id_jaminan` (`id_jaminan`,`nik_penjamin`),
  ADD KEY `nik_penjamin` (`nik_penjamin`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `pemanfaat`
--
ALTER TABLE `pemanfaat`
  ADD PRIMARY KEY (`kd_kelompok`,`id_member`),
  ADD KEY `kd_kelompok` (`kd_kelompok`,`id_member`),
  ADD KEY `id_member` (`id_member`);

--
-- Indexes for table `penjamin`
--
ALTER TABLE `penjamin`
  ADD PRIMARY KEY (`nik_penjamin`),
  ADD KEY `nik_penjamin` (`nik_penjamin`);

--
-- Indexes for table `person_pegawai`
--
ALTER TABLE `person_pegawai`
  ADD PRIMARY KEY (`nip`),
  ADD KEY `nip` (`nip`),
  ADD KEY `USERNAME` (`USERNAME`);

--
-- Indexes for table `pinjaman_kelompok`
--
ALTER TABLE `pinjaman_kelompok`
  ADD PRIMARY KEY (`loan_id`),
  ADD KEY `loan_id` (`loan_id`);

--
-- Indexes for table `rekening_akun`
--
ALTER TABLE `rekening_akun`
  ADD PRIMARY KEY (`id_rekening`),
  ADD KEY `parent_rekening` (`parent_rekening`);

--
-- Indexes for table `saldo_bulan_berjalan`
--
ALTER TABLE `saldo_bulan_berjalan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_rekening` (`id_rekening`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `verified_pinjaman_kelompok`
--
ALTER TABLE `verified_pinjaman_kelompok`
  ADD PRIMARY KEY (`loan_id`),
  ADD KEY `loan_id` (`loan_id`);

--
-- Indexes for table `waiting_pinjaman_kelompok`
--
ALTER TABLE `waiting_pinjaman_kelompok`
  ADD PRIMARY KEY (`loan_id`),
  ADD KEY `loan_id` (`loan_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `active_pinjaman_kelompok`
--
ALTER TABLE `active_pinjaman_kelompok`
  ADD CONSTRAINT `active_pinjaman_kelompok_ibfk_1` FOREIGN KEY (`loan_id`) REFERENCES `pinjaman_kelompok` (`loan_id`);

--
-- Constraints for table `angsuran`
--
ALTER TABLE `angsuran`
  ADD CONSTRAINT `angsuran_ibfk_1` FOREIGN KEY (`loan_id`) REFERENCES `pinjaman_kelompok` (`loan_id`),
  ADD CONSTRAINT `angsuran_ibfk_2` FOREIGN KEY (`id_transaksi`) REFERENCES `jurnal_transaksi` (`id_transaksi`);

--
-- Constraints for table `desa`
--
ALTER TABLE `desa`
  ADD CONSTRAINT `desa_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  ADD CONSTRAINT `desa_ibfk_2` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`);

--
-- Constraints for table `jurnal_transaksi`
--
ALTER TABLE `jurnal_transaksi`
  ADD CONSTRAINT `jurnal_transaksi_ibfk_1` FOREIGN KEY (`rek_debit`) REFERENCES `rekening_akun` (`parent_rekening`),
  ADD CONSTRAINT `jurnal_transaksi_ibfk_2` FOREIGN KEY (`rek_kredit`) REFERENCES `rekening_akun` (`parent_rekening`),
  ADD CONSTRAINT `jurnal_transaksi_ibfk_3` FOREIGN KEY (`nip`) REFERENCES `person_pegawai` (`nip`);

--
-- Constraints for table `kelompok_peminjam`
--
ALTER TABLE `kelompok_peminjam`
  ADD CONSTRAINT `kelompok_peminjam_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `person_pegawai` (`nip`),
  ADD CONSTRAINT `kelompok_peminjam_ibfk_2` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  ADD CONSTRAINT `kelompok_peminjam_ibfk_3` FOREIGN KEY (`id_desa`) REFERENCES `desa` (`id_desa`);

--
-- Constraints for table `kunjungan_kelompok`
--
ALTER TABLE `kunjungan_kelompok`
  ADD CONSTRAINT `kunjungan_kelompok_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `person_pegawai` (`nip`);

--
-- Constraints for table `member`
--
ALTER TABLE `member`
  ADD CONSTRAINT `member_ibfk_1` FOREIGN KEY (`nik_penjamin`) REFERENCES `penjamin` (`nik_penjamin`),
  ADD CONSTRAINT `member_ibfk_2` FOREIGN KEY (`id_jaminan`) REFERENCES `jaminan` (`id_jaminan`),
  ADD CONSTRAINT `member_ibfk_3` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Constraints for table `pemanfaat`
--
ALTER TABLE `pemanfaat`
  ADD CONSTRAINT `pemanfaat_ibfk_1` FOREIGN KEY (`kd_kelompok`) REFERENCES `kelompok_peminjam` (`kd_kelompok`),
  ADD CONSTRAINT `pemanfaat_ibfk_2` FOREIGN KEY (`id_member`) REFERENCES `member` (`id_member`);

--
-- Constraints for table `saldo_bulan_berjalan`
--
ALTER TABLE `saldo_bulan_berjalan`
  ADD CONSTRAINT `saldo_bulan_berjalan_ibfk_1` FOREIGN KEY (`id_rekening`) REFERENCES `rekening_akun` (`id_rekening`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Constraints for table `verified_pinjaman_kelompok`
--
ALTER TABLE `verified_pinjaman_kelompok`
  ADD CONSTRAINT `verified_pinjaman_kelompok_ibfk_1` FOREIGN KEY (`loan_id`) REFERENCES `pinjaman_kelompok` (`loan_id`);

--
-- Constraints for table `waiting_pinjaman_kelompok`
--
ALTER TABLE `waiting_pinjaman_kelompok`
  ADD CONSTRAINT `waiting_pinjaman_kelompok_ibfk_1` FOREIGN KEY (`loan_id`) REFERENCES `pinjaman_kelompok` (`loan_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
