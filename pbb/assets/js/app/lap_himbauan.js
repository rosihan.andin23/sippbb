$.fn.tableHimbauan= function(c) 
{
	var e = this;
	var c = $.extend({
		domFormFilter: '',
		domFormPrint: '',
		domDocOptions: '',
		domTable: '',
	}, c);


	/* Method */
	e.initialize = function()
	{
		$('[name="kecamatan"]', $(c.domFormFilter)).trigger('change');
	}

	
	/* Event */

	$(c.domFormFilter).submit(function()
	{
		var triggerButton = $(document.activeElement);
		var isNewTab = false;
		
		if(triggerButton.length)
		{
			if(triggerButton.attr('data-url')) $(this).attr('action', triggerButton.attr('data-url'))
			if(triggerButton.attr('data-target')) 
			{
				isNewTab = triggerButton.attr('data-target') == '_blank';
				$(this).attr('target', triggerButton.attr('data-target'))
			}
			else
				$(this).attr('target', '')
		}
		
		var parent = $(this).parents('.kt-portlet');
		var block = function()
		{		
			KTApp.block(parent, {
				overlayColor: '#000000',
				type: 'v2',
				state: 'primary',
				message: 'Memproses data...',
			});
		}
		
		if(!isNewTab)
		{
			if(!e.length) 
				block();
			else
				e.fadeOut(500, function(){ block(); })	
		}
	});
	
	$('[name="kecamatan"]', $(c.domFormFilter)).change(function()
	{
		var val = $(this).val();
		var targetOptions = $('[name="kelurahan"] option', $(c.domFormFilter));
		if(!val) targetOptions.show();
		else 
		{
			var kelValue = targetOptions.parent().val();
			var isKelValueExists = false;
			targetOptions.each(function(){
				if($(this).attr('data-kec') == val || !$(this).attr('data-kec'))
				{
					$(this).show();
					if($(this).attr('value') == kelValue) isKelValueExists = true;
				}
				else
					$(this).hide();
			})
			kelValue = isKelValueExists ? kelValue : '';
			targetOptions.parent().val(kelValue)
		}
		
	})

	$('[name="check-all"]', $(c.domTable)).click(function()
	{
		var isChecked = $(this).is(':checked');
		$('[name="check-item[]"]', $(c.domTable)).each(function(){
			if($(this).is(':checked') != isChecked) $(this).trigger('click');
		})
	})
	
	
	$('[name="btn-print"], [name="btn-download"]', $(c.domFormFilter)).click(function()
	{
		var targetUrl = $(this).data('url');
		var form = $(c.domFormPrint);
		if(!$('[name="check-item[]"]:checked', form).length)
		{
			swal.fire( 'Peringatan', 'Harap centang salah satu data terlebih dahulu', 'warning');
			return false;
		}
		
		swal.fire({
			type: 'question',
			title: 'Pilih jenis surat',
			html: $(c.domDocOptions).html(),
			cancelButtonText: 'Batal',
			showCancelButton: true,
			onOpen: function()
			{
				$('[datepicker]').datepicker({
					language: 'id',
					format: 'dd/mm/yyyy',
					todayHighlight: true,
					startView: 'year',
					templates: {
						leftArrow: '<i class="la la-angle-left"></i>',
						rightArrow: '<i class="la la-angle-right"></i>',
					},
				}).datepicker("setDate", new Date());;
			},
		}).then(function(result){
			if (result.value) {
				$('[type="checkbox"]', c.domDocOptions).each(function()
				{
					var isChecked = $(this).is(':checked');
					var target = $('#swal2-content [name="'+$(this).attr('name')+'"]')
					if(target.length && isChecked !== target.is(':checked')) $(this).trigger('click');
				})
				$('[type="text"],[type="number"]', c.domDocOptions).each(function()
				{
					var target = $('#swal2-content [name="'+$(this).attr('name')+'"]')
					if(target.length) $(this).val(target.val());
				})
				form.attr('action', targetUrl).trigger('submit');
			}
		})
	})
	
	
	e.initialize();
	
	return e;
}

