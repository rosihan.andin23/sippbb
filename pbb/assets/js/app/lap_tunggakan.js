$.fn.tableTunggakan= function(c) 
{
	var e = this;
	var c = $.extend({
		domFormFilter: '',
		domSearch: '',
	}, c);


	/* Method */
	e.initialize = function()
	{
		$('[name="kecamatan"]', $(c.domFormFilter)).trigger('change');
	}

	
	/* Event */

	$(c.domFormFilter).submit(function(event)
	{
		var triggerButton = $(document.activeElement);
		var isNewTab = false;
		
		if(triggerButton.length)
		{
			if(triggerButton.attr('data-url')) $(this).attr('action', triggerButton.attr('data-url'))
			if(triggerButton.attr('data-target')) 
			{
				isNewTab = triggerButton.attr('data-target') == '_blank';
				$(this).attr('target', triggerButton.attr('data-target'))
			}
			else
				$(this).attr('target', '')
		}
		
		var parent = $(this).parents('.kt-portlet');
		var block = function()
		{		
			KTApp.block(parent, {
				overlayColor: '#000000',
				type: 'v2',
				state: 'primary',
				message: 'Memproses data...',
			});
		}
		
		if(!isNewTab)
		{
			if(!e.length) 
				block();
			else
				e.fadeOut(500, function(){ block(); })	
		}
	});
	
	$('[name="kecamatan"]', $(c.domFormFilter)).change(function()
	{
		var val = $(this).val();
		var targetOptions = $('[name="kelurahan"] option', $(c.domFormFilter));
		if(!val) targetOptions.show();
		else 
		{
			var kelValue = targetOptions.parent().val();
			var isKelValueExists = false;
			targetOptions.each(function(){
				if($(this).attr('data-kec') == val || !$(this).attr('data-kec'))
				{
					$(this).show();
					if($(this).attr('value') == kelValue) isKelValueExists = true;
				}
				else
					$(this).hide();
			})
			kelValue = isKelValueExists ? kelValue : '';
			targetOptions.parent().val(kelValue)
		}
		
	})

	
	e.initialize();
	
	return e;
}

