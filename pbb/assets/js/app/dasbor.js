$.fn.pbbDasbor = function(c) 
{
	var e = this;
	var c = $.extend({
		url: '',
		loaderLocation: '',
		idSummary: '',
		dataSummary: 'dataPerTahun',
		idChartPeriode: '',
		dataChartPeriode: 'dataPerTahun',
		idChartPieBuku: '',
		dataChartPieBuku: 'dataPerBuku',
		idChartWilayah: '',		
		dataChartWilayah: 'dataPerWilayah',		
		idChartPieWilayah: '',
		dataChartPieWilayah: 'dataPerWilayah',
		
		kodeKecamatan: '0',
		tahun: '0',
	}, c);
	
	e.data = {};
	e.chartsFn = {};
	e.charts = {};
	e.currChart;
	e.currChartName;
	

	/* Method */
	e.initialize = function()
	{
		am4core.useTheme(am4themes_animated);
		//am4core.useTheme(am4themes_material);
		
		e.loadData();	
	}

	e.loadData = function()
	{
		var block = $(c.loaderLocation);
		KTApp.block(block, {overlayColor: '#000000', type: 'v2', state: 'primary', message: 'Harap tunggu...', centerY:false, baseZ:100});

		$.ajax({
			url: c.url+'/'+c.kodeKecamatan+'/'+c.tahun,
			method: 'GET',
			dataType: 'JSON',
			cache: true,
			success: function(data)
			{
				e.data = data;
				
				e.generateChart('periode');
				e.generateChart('wilayah');
				e.generateChart('pieWilayah');
				e.generateChart('pieBuku');
				e.generateChart('summary');
				
				$('[data="prev-year"]', e).text(parseInt(e.data.tahun) - 1);
				$('[data="curr-year"]', e).text(e.data.tahun);
				$('[data="kecamatan-name"]', e).text(e.data.namaKecamatan);
				$('[data="publish-date"]', e).text(e.data.publishDate);
				
				KTApp.unblock(block);
			},
			error: function(e)
			{
				KTApp.unblock(block);
				if(typeof(e.responseJSON) == 'undefined') msg = 'Data bukan bertipe JSON';
				else
				{
					var msg = (typeof(e.responseJSON.message) != 'undefined') ? e.responseJSON.message :  e.statusText;
					msg = (typeof(msg) == 'object') ? msg.join('<br>') :  msg;
				}
				defSwal.fire( 'Gagal memproses data ( Error : '+e.status+' )', msg, 'error');
			}
		})
	}
	
	e.generateChart = function(chartName, data)
	{
		e.chartsFn[chartName](chartName, data);
	}

	
	/*** Chart Name ***/
	e.chartsFn.summary = function(chartName, data)
	{
		var dom = $('#'+c.idSummary, e);
		var data = typeof(data) == 'undefined' ? e.data[c.dataSummary] : data;

		$.each(data, function(i, row)
		{
			var year = parseInt(row.tahun);
			year = year == parseInt(c.tahun) ? 'curr' : year;
			year = year == parseInt(c.tahun)-1 ? 'prev' : year;
			var iDom = $('[data-summary-year="'+year+'"]', dom)
			
			var pDom = $('[data-summary-percent]', iDom);
			if(pDom.length) pDom.each(function()
			{
				var field = $(this).attr('data-summary-percent')
				var percent = row[field] == 0 || row.pokok_rp == 0 ? 0 : (row[field] / row.pokok_rp)*100;
				
				if($(this).attr('role') == 'progressbar') $(this).css('width', percent.toFixed(1)+'%');
				else $(this).text(e.numberFormatter(percent));
			})
			
			
			if(iDom.length) $.each(row, function(key, val)
			{
				var jDom = $('[data-summary-field="'+key+'"]', iDom)
				if(!jDom.length) return;
				
				var format = jDom.attr('data-summary-format');
				if( format == 'suffix') jDom.text(e.suffixFormatter(val, 2));
				else if(format == 'number') jDom.text(e.numberFormatter(val,0));
				else jDom.text(val);
			})
		})
		
	
	}
	
	e.chartsFn.periode = function(chartName, data)
	{
		if(e.charts[chartName]) e.charts[chartName].dispose();

		e.charts[chartName] = am4core.create(c.idChartPeriode, am4charts.XYChart);
		e.charts[chartName].language.locale = am4lang_id_ID;
		e.charts[chartName].data = typeof(data) == 'undefined' ? e.data[c.dataChartPeriode] : data;
		//e.charts[chartName].scrollbarX = new am4core.Scrollbar();

		var xAxes = e.charts[chartName].xAxes.push(new am4charts.CategoryAxis());
		xAxes.dataFields.category = "tahun";
		xAxes.renderer.grid.template.location = 0;
		xAxes.renderer.minGridDistance = 30;
		xAxes.startLocation = 0.45;
		xAxes.endLocation = 0.55;
		xAxes.events.on("rangechangeended", function(ev) 
		{
			var axis = ev.target;
			var cellWidth = axis.axisFullLength / (axis.endIndex - axis.startIndex);
			axis.renderer.labels.template.maxWidth = cellWidth;
		});
		
		var label = xAxes.renderer.labels.template;
		label.truncate = true;
		label.tooltipText = "Tahun {tahun}";

		//var cursor = e.charts[chartName].cursor = new am4charts.XYCursor();
		var legend = e.charts[chartName].legend = new am4charts.Legend();
		legend.marginTop = 0;
		legend.itemContainers.template.events.on("over", function(event) {
			processOver(event.target.dataItem.dataContext);
		})
		legend.itemContainers.template.events.on("out", function(event) {
			processOut(event.target.dataItem.dataContext);
		})
		
		var yAxes_1 = e.charts[chartName].yAxes.push(new am4charts.ValueAxis());
		yAxes_1.numberFormatter.numberFormat = "#.##a";
		//yAxes_1.min = 0;
		
		var subCats = {
			pokok_rp:{title: 'Pokok', bg: '#2c77f4',},
			realisasi_rp:{ title: 'Realisasi', bg: '#ffb822',},
			sisa_rp:{ title: 'Sisa', bg: '#fd397a',}
		};
		
		$.each(subCats, function(subCat, row)
		{
			var series = e.charts[chartName].series.push(new am4charts.LineSeries());
			series.stroke = am4core.color(row.bg);
			series.yAxis = yAxes_1;
			series.dataFields.valueY = subCat;
			series.dataFields.categoryX = "tahun";
			series.name = row.title;
			series.fill = series.stroke;
			series.fillOpacity = 0.5;
			
			var bullet = series.bullets.push(new am4charts.CircleBullet());
			bullet.circle.fill = am4core.color("#fff");
			//bullet.circle.fillOpacity = .4;
			bullet.circle.strokeWidth = 2;
			bullet.circle.radius = 5;
			bullet.tooltipHTML = row.title+"<br>Tahun: <b>{tahun}</b><br>Nilai : <b>Rp. {"+subCat+".formatNumber('#,###.##')}</b>";
			bullet.cursorOverStyle = am4core.MouseCursorStyle.pointer;
			
			var segment = series.segments.template;
			segment.interactionsEnabled = true;

			var hoverState = segment.states.create("hover");
			hoverState.properties.strokeWidth = 3;

			var dimmed = segment.states.create("dimmed");
			dimmed.properties.stroke = am4core.color("#dadada");

			segment.events.on("over", function(event) {
				processOver(event.target.parent.parent.parent);
			});

			segment.events.on("out", function(event) {
				processOut(event.target.parent.parent.parent);
			});
		})
		
		
		function processOver(hoveredSeries) {
			hoveredSeries.toFront();

			hoveredSeries.segments.each(function(segment) {
				segment.setState("hover");
			})

			e.charts[chartName].series.each(function(series) {
				if (series != hoveredSeries) {
					series.segments.each(function(segment) {
						segment.setState("dimmed");
					})
					series.bulletsContainer.setState("dimmed");
				}
			});
		}

		function processOut(hoveredSeries) {
			e.charts[chartName].series.each(function(series) {
				series.segments.each(function(segment) {
					segment.setState("default");
				})
				series.bulletsContainer.setState("default");
			});
		}
		
		
		
		/* if(last_thn)
		{
			e.charts[chartName].events.on("beforedatavalidated", function(ev) 
			{
				e.charts[chartName].data.sort(function(a, b) {
					if ( a[last_thn] > b[last_thn] ) return -1;
					if ( a[last_thn] < b[last_thn] ) return 1;
					return 0;
				});
			});
		} */
	}
	
	e.chartsFn.pieBuku = function(chartName, data)
	{
		var seriesField = $('#'+c.idChartPieBuku).data('series');
		if(!seriesField) seriesField = "pokok_sppt";
		
		if(e.charts[chartName]) e.charts[chartName].dispose();
		e.charts[chartName] = am4core.create(c.idChartPieBuku, am4charts.PieChart);
		e.charts[chartName].language.locale = am4lang_id_ID;
		e.charts[chartName].hiddenState.properties.opacity = 0;
		//e.charts[chartName].legend = new am4charts.Legend();
		e.charts[chartName].data = typeof(data) == 'undefined' ? e.data[c.dataChartPieBuku] : data;
		
		
		var legend = e.charts[chartName].legend = new am4charts.Legend();
		legend.marginTop = 0;
		
		var series = e.charts[chartName].series.push(new am4charts.PieSeries());
		series.cursorOverStyle = am4core.MouseCursorStyle.pointer;
		series.dataFields.value = seriesField;
		series.dataFields.category = "kode_buku";
		//series.alignLabels = false;
		//series.labels.template.bent = true;
		//series.labels.template.radius = 3;
		//series.labels.template.padding(0,0,0,0);
		series.labels.template.disabled = true;
		series.ticks.template.disabled = true;

		series.slices.template.tooltipHTML = "<b>Buku {category}</b><br>"+
		"<b>{value.value.formatNumber('#,###.##	')}</b> SPPT<br>"+
		"<b>{value.percent.formatNumber('#.#')}%</b>";
	}

	e.chartsFn.wilayah = function(chartName, data)
	{
		var seriesField = $('#'+c.idChartWilayah).data('series');
		if(!seriesField) seriesField = "pokok_rp";
		
		data = typeof(data) == 'undefined' ? e.data[c.dataChartWilayah] : data;
		data.sort(function(a, b) {
			return b[seriesField] - a[seriesField];
		}) 
		
		if(e.charts[chartName]) e.charts[chartName].dispose();
		e.charts[chartName] = am4core.create(c.idChartWilayah, am4charts.XYChart);
		e.charts[chartName].language.locale = am4lang_id_ID;
		e.charts[chartName].data = data;

		var xAxes = e.charts[chartName].yAxes.push(new am4charts.CategoryAxis());
		xAxes.dataFields.category = "nama_wilayah";
		//xAxes.renderer.grid.template.location = 0;
		//xAxes.renderer.minGridDistance = 30;
		xAxes.renderer.inversed = true;

		
		var yAxes_1 = e.charts[chartName].xAxes.push(new am4charts.ValueAxis());
		yAxes_1.numberFormatter.numberFormat = "#.##a";
		
		
		var series = e.charts[chartName].series.push(new am4charts.ColumnSeries());
		//series.yAxis = yAxes_1;
		series.dataFields.valueX = seriesField;
		series.dataFields.categoryY = "nama_wilayah";
		series.name = "nama_wilayah";
		series.columns.template.tooltipHTML = "Wilayah : <b>{nama_wilayah}</b><br>PBB : <b>Rp. {valueX.formatNumber('#,###.##')}</b>";
		series.columns.template.fillOpacity = .7;
		series.columns.template.strokeWidth = 1;
		series.columns.template.strokeOpacity = 0.3;
		series.cursorOverStyle = am4core.MouseCursorStyle.pointer;
		
		series.columns.template.adapter.add("fill", function(fill, target) {
			return e.charts[chartName].colors.getIndex(target.dataItem.index);
		});
	}

	e.chartsFn.pieWilayah = function(chartName, data)
	{
		var seriesField = $('#'+c.idChartPieWilayah).data('series');
		if(!seriesField) seriesField = "pokok_sppt";
		
		if(e.charts[chartName]) e.charts[chartName].dispose();
		e.charts[chartName] = am4core.create(c.idChartPieWilayah, am4charts.PieChart);
		e.charts[chartName].language.locale = am4lang_id_ID;
		e.charts[chartName].hiddenState.properties.opacity = 0;
		//e.charts[chartName].legend = new am4charts.Legend();
		e.charts[chartName].data = typeof(data) == 'undefined' ? e.data[c.dataChartPieWilayah] : data;
		
		
		var legend = e.charts[chartName].legend = new am4charts.Legend();
		legend.marginTop = 0;
		
		var series = e.charts[chartName].series.push(new am4charts.PieSeries());
		series.cursorOverStyle = am4core.MouseCursorStyle.pointer;
		series.dataFields.value = seriesField;
		series.dataFields.category = "nama_wilayah";

		series.slices.template.tooltipHTML = "<b>{category}</b><br>"+
		"<b>{value.value.formatNumber('#,###.##')}</b> SPPT<br>"+
		"<b>{value.percent.formatNumber('#.#')}%</b>";
	}

	
	// event
	e.on('click', '[data-kecamatan-toggle]', function()
	{
		c.kodeKecamatan = $(this).attr('data-kecamatan-toggle');
		e.loadData();
	})
	
	e.on('click', '[data-tahun-toggle]', function()
	{
		c.tahun = $(this).attr('data-tahun-toggle');
		e.loadData();
	})
	
	e.on('click', '[data-toggle="change-series"] [data-series]', function() 
	{
		var parent = $(this).parent();
		var chartTarget = parent.data('chart-target')
		var domTarget = parent.data('dom-target')
		if(!chartTarget || !domTarget) return false;
		
		var series = $(this).data('series');
		$(domTarget).data('series', series);
		e.generateChart(chartTarget);
		$(this).addClass('active').siblings().removeClass('active');
	})
	
	e.suffixFormatter = function (num, digits=2) 
	{
		var isMinus = (num<0);
		if(isMinus) num = num * -1;
		var si = [
			{value:1, symbol:""},
			{value:1E3, symbol:" Rb"},
			{value:1E6, symbol:" Jt"},
			{value:1E9, symbol:" Mil"},
			{value:1E12, symbol:" Tr"},
			{value:1E15, symbol:" Bil"},
			{value:1E18, symbol:" E"}
		];
		var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
		var i;
		for (i = si.length - 1; i > 0; i--) {
			if (num >= si[i].value)  break;
		}
		return (isMinus ? '-' : '')+(num / si[i].value).toFixed(digits).replace(rx, "$1").replace('.', ',') + si[i].symbol;
	}
	
	e.numberFormatter = function numberWithCommas(num, digits=2) 
	{
    return num.toFixed(digits).toString().replace('.', ',').replace(/\B(?=(\d{3})+(?!\d))/g, '.');
	}
	
	e.initialize();
	
	return e;
}

