$.fn.tablePenerimaan = function(c) 
{
	var e = this;
	var c = $.extend({
		domFormFilter: '',
		domSearch: '',
	}, c);


	/* Method */
	e.initialize = function()
	{
		$('#kt_datepicker', $(c.domFormFilter)).datepicker({
			language: 'id',
			format: 'dd/mm/yyyy',
			todayHighlight: true,
			startView: 'year',
			templates: {
				leftArrow: '<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>',
			},
		});
	}

	
	/* Event */

	$(c.domFormFilter).submit(function()
	{
		var triggerButton = $(document.activeElement);
		var isNewTab = false;
		
		if(triggerButton.length)
		{
			if(triggerButton.attr('data-url')) $(this).attr('action', triggerButton.attr('data-url'))
			if(triggerButton.attr('data-target')) 
			{
				isNewTab = triggerButton.attr('data-target') == '_blank';
				$(this).attr('target', triggerButton.attr('data-target'))
			}
			else
				$(this).attr('target', '')
		}
		
		var parent = $(this).parents('.kt-portlet');
		var block = function()
		{		
			KTApp.block(parent, {
				overlayColor: '#000000',
				type: 'v2',
				state: 'primary',
				message: 'Memproses data...',
			});
		}
		
		if(!isNewTab)
		{
			if(!e.length) 
				block();
			else
				e.fadeOut(500, function(){ block(); })	
		}
	});

	$('[data-toggle="expand-kec"]', e).click(function()
	{
		var kec = $(this).data('kec');
		var target = $('tr.tr-group-child[data-kec="'+kec+'"]', e);
		if(target.hasClass('kt-hidden')) target.removeClass('kt-hidden').removeClass('search-not-match').removeClass('search-match')
		else target.addClass('kt-hidden')
	})
	
	$(c.domSearch).keyup(function()
	{
		var keyword = $(this).val().toLowerCase();
		$('[data-searchable="true"]', e).each(function()
		{
			$(this).parents('tr').removeClass('search-not-match').removeClass('search-match')
			if(keyword)
			{
				if ($(this).text().toLowerCase().indexOf(keyword) > -1) 
				{
					var tr = $(this).parents('tr').addClass('search-match')
					if(tr.is('.tr-group-child'))
					{
						$('.tr-group-head[data-kec="'+tr.data('kec')+'"]', e).addClass('search-match').removeClass('search-not-match')
					}
				}
				else 
					$(this).parents('tr').addClass('search-not-match')
			}
		})
	})

	
	e.initialize();
	
	return e;
}

