(function ($) {

$.fn.customDataTable = function(c) 
{
	var e = this;
	var c = $.extend({
		responsive: true,
		paging: true,
		pageLength: 50,
		processing : true,
		lengthMenu: [
			[2, 5, 10, 25, 50, 100, -1],
			[2, 5, 10, 25, 50, 100, "Semua"]
		],
		language: {
			search: "Pencarian",
			lengthMenu: "Data/halaman: &nbsp;_MENU_ ",
			zeroRecords: "Data tidak ditemukan",
			info: "Halaman _PAGE_ dari total _PAGES_ halaman",
			infoEmpty: "Data kosong",
			infoFiltered: "(_MAX_ data difilter)"
		}
	}, c);
	
	e.table;
	e.indexCols = [];
	e.firstNormalCols;
	
	/* Method */
	e.initConfig = function()
	{
		var cols = [];
		var colDefs = [];		
		
		// init ajax config
		if(e.data('url')) c.ajax = { url: e.data('url'), type: 'get'};
		
		// init columns & columnDefs config
		$('thead:first-child>tr:first-child>th', e).each(function(i, row)
		{
			var th = $(this);
			
			switch(th.data('type')) 
			{
				case '#':
					e.indexCols.push(i);
					cols.push({'class': 'text-center'});
					colDefs.push(
					{
						targets: i,
						orderable: false,
						searchable: false,
						render: function(data, type, full, meta) {
							return '#';
						}		
					});
				break;
				
				case 'options':
					if(typeof(th.data('id')) != 'undefined') cols.push({data: th.data('id'), 'class': 'text-center' });
					colDefs.push(
					{
						targets: i,
						orderable: false,
						searchable: false,
						render: function(data, type, full, meta) {
							if(typeof(c.optionsRenderer) != 'undefined') return c.optionsRenderer(data, type, full, meta);
							else return data;
						}
					});
				break;
				
				default:
					if(typeof(e.firstNormalCols) == 'undefined') e.firstNormalCols = i;
					cols.push({
						'class': th.data('class'),
						data: th.data('name') || th.text().toLowerCase()
					});
				break;
			}
		})
		
		if(cols && typeof(c.columns) == 'undefined') c.columns = cols;
		if(colDefs && typeof(c.columnDefs) == 'undefined') c.columnDefs = colDefs;
		if(typeof(e.firstNormalCols) != 'undefined' && typeof(c.order) == 'undefined') c.order = [[e.firstNormalCols, "asc"]];
		
		if(typeof(c.postConfig) == 'function') c = c.postConfig(c);
	}
	
	e.initDataTable = function()
	{
		e.table = e.DataTable(c);
		
		// add index columns
		if(e.indexCols)  
		{
			e.table.on( 'order.dt search.dt', function () 
			{
				$.each(e.indexCols, function(i, cols){
					e.table.column(cols, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
						cell.innerHTML = i+1;
					});
				})
			}).draw();
		}
		
		$('body').on('table.refresh', function(){
			e.table.ajax.reload(); 
		})
	}
	
	e.initConfig();
	e.initDataTable();
	
	
	return e;	
};

}(jQuery));