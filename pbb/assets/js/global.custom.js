
var CustomApp = function() {
	
	var handlePluginInit = function()
	{
		defSwal = swal.mixin({
			heightAuto: false,
			buttonsStyling: false,
			confirmButtonClass: 'btn btn-success',
			confirmButtonColor: null,
			cancelButtonClass: 'btn btn-secondary',
			cancelButtonColor: null,
			cancelButtonText: 'Batal',
		});
		
		$('select.apply-select2').select2()
	}

	var handleMainMenu = function()
	{
		var menu_con = $('.kt-aside-menu .kt-menu__nav ')
		
		$('a', menu_con).each(function(){
			var href = $(this).attr('href');
			if (window.location.href.indexOf(href) > -1)
			{
				var parent = $(this).parent('li').parents('li');
				$(this).parent('li').addClass('kt-menu__item--active');
				parent.addClass('kt-menu__item--active open').find('.arrow').addClass('open');
			}
		});
	}
	
	var handleAjaxModal = function()
	{
		$("body").on("shown.bs.modal", '.main-modal', function(e) 
		{
			var href = e.relatedTarget.href;
			var domContent = $(this).find('.modal-content');
			if(href)
			{
				domContent.html('<div class="modal-loading text-center p-2"><i class="la la-refresh la-spin"></i>&nbsp; Memuat Data</div>')
				$.ajax({
					url: href,
					method: 'GET',
					cache: true,
					success: function(data){
						domContent.fadeOut(400, function(){
							domContent.html(data).slideDown(400, function(){
								handlePluginInit();
							});
						})						
						$('.form-group', domContent).first().find('input').first().focus();
					},
					error: function(e)
					{
						var msg = 'Gagal memuat data<br>Error : '+e.status+' ('+(e.responseJSON.message ? e.responseJSON.message : e.statusText)+') ';
						domContent.html('<div class="alert alert-danger m-2">'+msg+'</div>')
					}
				})
			}
		});
		
		$('body').on('hidden.bs.modal', '.main-modal', function (e) 
		{
			$(this).find('.modal-content').html('')
		});
		
		$('body').on('click', '[data-toggle="modal-submit"]', function (e) 
		{
			var parent = $(this).parents('.modal-content')
			if(!parent.length) parent = $(this).parents('.kt-portlet__body');
			if(!parent.length) return false;
			
			var form = parent.find('form');
			if(form.length) form.trigger('submit');			
		});
		
	}
	
	var handleAjaxForm = function()
	{
		$('body').on('submit', 'form[ajax]', function()
		{
			var form = $(this);
			var parent = form.parents('.modal-content')
			if(!parent.length) parent = form.parents('.kt-portlet');
			if(!parent.length) parent = form;
			
			var isInModal = parent.hasClass('modal-content');
			
			$('.form-ajax-alert', form).remove();
			
			KTApp.block(parent, {
				overlayColor: '#000000',
				type: 'v2',
				state: 'primary',
				message: 'Memproses data...'
			});

			$.ajax({
				url: form.attr('action'),
				method: form.attr('method'),
				data: form.serialize(),
				cache: false,
				success: function(data)
				{
					KTApp.unblock(parent);
					if(isInModal)
					{
						defSwal.fire('Data berhasil diproses', typeof(data.message) != 'undefined' ? data.message : '', 'success');
						$('.modal').modal('hide');
						$('body').trigger('table.refresh');
					}
				},
				error: function(e)
				{
					KTApp.unblock(parent);
					var msg = (typeof(e.responseJSON.message) != 'undefined') ? e.responseJSON.message :  e.statusText;
					msg = (typeof(msg) == 'object') ? msg.join('<br>') :  msg;
					defSwal.fire( 'Gagal memproses data ( Error : '+e.status+' )', msg, 'error');
				}
			})
			return false;
		})
	}
	
	var handleAjaxConfirm = function()
	{
		$('body').on('click', '[data-toggle="ajax-confirm"]', function()
		{
			var elm = $(this);
			var parent = elm.parents('.modal-content')
			if(!parent.length) parent = elm.parents('.kt-portlet');
			if(!parent.length) parent = $('.dropdown.show [data-toggle="dropdown"][aria-expanded="true"]', 'body').parents('.kt-portlet')
			if(!parent.length) parent = elm;
			
			var isInModal = parent.hasClass('modal-content');
			var msgTitle = elm.data('message-title') ? elm.data('message-title') : 'Apakah anda yakin ?'
			var msgText = elm.data('message-text') ? elm.data('message-text') : 'Data ini akan terhapus dan tidak bisa dikembalikan lagi.'
			var msgBtnYes = elm.data('button-yes') ? elm.data('button-yes') : 'Ya!'
			
			var successTitle = elm.data('success-title') ? elm.data('success-title') : 'Data berhasil diproses'
			var successText = elm.data('success-text') ? elm.data('success-text') : 'Data berhasil dihapus.'

			defSwal.fire({
				title: msgTitle,
				text: msgText,
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: msgBtnYes
			}).then(function(result) 
			{
				if (result.value) 
				{
					KTApp.block(parent, {
						overlayColor: '#000000',
						type: 'v2',
						state: 'primary',
						message: 'Memproses data...'
					});
					
					$.ajax({
						url: elm.attr('href'),
						method: 'GET',
						cache: false,
						success: function(data)
						{
							KTApp.unblock(parent);
							defSwal.fire(successTitle, successText, 'success');
							$('body').trigger('table.refresh');
						},
						error: function(e)
						{
							KTApp.unblock(parent);
							var msg = (typeof(e.responseJSON.message) != 'undefined') ? e.responseJSON.message :  e.statusText;
							msg = (typeof(msg) == 'object') ? msg.join('<br>') :  msg;
							defSwal.fire( 'Gagal memproses data ( Error : '+e.status+' )', msg, 'error');
						}
					})
				}
			});

			return false
			
		})		
	}
	
	var handleLinkConfirm = function()
	{
		$('body').on('click', '[data-toggle="link-confirm"]', function()
		{
			var elm = $(this);
			
			var msgTitle = elm.data('message-title') ? elm.data('message-title') : 'Apakah anda yakin ?'
			var msgText = elm.data('message-text') ? elm.data('message-text') : 'Data ini akan terhapus dan tidak bisa dikembalikan lagi.'
			var msgBtnYes = elm.data('button-yes') ? elm.data('button-yes') : 'Ya!'
			
			var successTitle = elm.data('success-title') ? elm.data('success-title') : 'Data berhasil diproses'
			var successText = elm.data('success-text') ? elm.data('success-text') : 'Data berhasil dihapus.'

			defSwal.fire({
				title: msgTitle,
				text: msgText,
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: msgBtnYes
			}).then(function(result) 
			{
				if (result.value) window.location = elm.attr('href');
			});

			return false
			
		})		
	}
	
	return {
		init: function() {
			handlePluginInit();
			handleMainMenu();
			handleAjaxModal();
			handleAjaxForm();
			handleAjaxConfirm();
			handleLinkConfirm();
		},
	};

}();

jQuery(document).ready(function() {	
	CustomApp.init();
});